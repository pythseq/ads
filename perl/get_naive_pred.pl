#!/usr/bin/perl
use strict;
use warnings;

# creates Naive predictions for a set of gene\tgo\tscore data
#
# According to Clark & Radiovac (2013):
# The Naive classifier simply assignes the prior probability of an annotation term in a database.


my $usage= 	"USAGE: get_naive_pred.pl data_in naive_out go_counts go_th k\n\n".
		"data_in   :\t<gene\\tgo> annotation (only gene col is used)\n".
		"naive_out :\t<gene\\tgo\\tscore> naive FPS\n".
		"go_counts :\t<go\\tcount\\tfreq> sorted by the counts\n".
		"go_th     :\tGO-terms with frequency >go_th will be excluded from naive FPS, set to 1 for no thresholding\n".
		"k         :\tnum of GO-terms predicted for each gene\n";

if( scalar(@ARGV)< 5 ) { die $usage; }

my $fin= shift(@ARGV);
my $fout= shift(@ARGV);
my $fcounts= shift(@ARGV);
my $go_th= shift(@ARGV);
my $k= shift(@ARGV);

# debug
#print STDERR "fin: $fin\n";
#print STDERR "fout: $fout\n";
#print STDERR "fcounts: $fcounts\n";
#print STDERR "go_th: $go_th\n";
#print STDERR "k: $k\n";


# READING TOP FREQUENCY GO IDS
my @top_go_list;
my @top_score_list;
my $counter=0;
my @flt_go_list;

open(IN,"<$fcounts") or die "Can\'t open $fcounts: $!\n";
while(my $l=<IN>){
	if ( $l =~ m/^[\!#]/){ # comment line
		next;
	}
	chomp($l);
	#print "line:\'$l\'"; exit(0);
	
	my @fields = split(/\t/,$l);
	
	if( $fields[2]>$go_th){
		push(@flt_go_list,$fields[0]);
		next;
	}
	
	push(@top_go_list,$fields[0]);
	push(@top_score_list,$fields[2]);
	if( ($counter++)>$k){
		last;
	}
}		
close(IN);


# READING GENES FROM <GENE,GO> ANNOTATION
my %gene_hash;
open(IN,"<$fin") or die "Can\'t open $fin: $!\n";
while(my $l=<IN>){
	if ( $l =~ m/^[\!#]/){ # comment line
		next;
	}
	chomp($l);
	my @fields = split(/\t/,$l);
	$gene_hash{$fields[0]}= 1;
}		
close(IN);

# PRINTING NAIVE PREDICTIONS
open(OUT,">$fout") or die "Can\'t open $fout: $!\n";
my @gene_list= sort keys(%gene_hash);

foreach my $gene (@gene_list){
	for(my $i=0; $i<$k;$i++){
		print OUT "$gene\t$top_go_list[$i]\t$top_score_list[$i]\n";
	}
}
close(OUT);

# DEBUG
print STDERR "# excluded ",($#flt_go_list + 1)," GOs\n";
#foreach my $go(@flt_go_list){
#	print "$go\n";
#}

	
