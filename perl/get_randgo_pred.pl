#!/usr/bin/perl
use strict;
use warnings;

# Returns Decoy Prediction Set (DPS) by sampling k random GO-terms (without replacement) for each gene
# Predictions are scored either by GO-frequency or sampling Norm(mu,std)
#

my $usage= 	"USAGE: get_randgo_pred.pl data_in data_out go_counts k rseed [mu] [std]\n\n".

		"data_in     : <gene\\tgo> annotation (only gene col is used)\n".
		"data_out    : <gene\\tgo\\tscore> annotation\n".
		"go_counts   : <go\\tcount\\tfreq> sorted by the counts\n".
		"k           : num of pred GO-terms\n".
		"rseed       : seed for random number generator\n".
		"[mu]        : mu parameter for Normal score dist [optional]\n".
		"[std]       : std parameter for Normal score dist [optional]\n\n".
		
		"NOTE \t: by default GO-frequencies are used as scores\n".
		"     \t: alternatively scores are sampled from Normal(mu,std)\n\n";

if( scalar(@ARGV)< 5 ) { die $usage; }

my $fin		= shift(@ARGV);
my $fout	= shift(@ARGV);
my $fcounts	= shift(@ARGV);
my $k		= shift(@ARGV);
my $rseed	= shift(@ARGV);
my $mu		= 0;
my $std		= 0;
my $use_norm_scores	= 0;
if( scalar(@ARGV)>=2){
$use_norm_scores	= 1;
$mu		= shift(@ARGV);
$std		= shift(@ARGV);
}
srand($rseed);


# debug
my $DEBUG = 0;
if($DEBUG){
print STDERR "fin\t: $fin\n";
print STDERR "fout\t: $fout\n";
print STDERR "go_counts\t: $fcounts\n";
print STDERR "k\t: $k\n";
}


# READING GO IDS FOR SMALLES CLASSES
my %gofreq_hash;
my $counter=0;

open(IN,"<$fcounts") or die "Can\'t open $fcounts: $!\n";
while(my $l=<IN>){
	if ( $l =~ m/^[\!#]/){ # comment line
		next;
	}
	chomp($l);
	my @sp = split(/\t/,$l);
	
	# THRESHOLD counts < th
	#if($sp[1] < $th){
	#	next;}
		
	$gofreq_hash{$sp[0]}= $sp[2];
}		
close(IN);


# READING GENES FROM <GENE,GO> ANNOTATION
my %gene_hash;
open(IN,"<$fin") or die "Can\'t open $fin: $!\n";
while(my $l=<IN>){
	if ( $l =~ m/^[\!#]/){ # comment line
		next;
	}
	chomp($l);
	my @fields = split(/\t/,$l);
	$gene_hash{$fields[0]}= 1;
}		
close(IN);

# PRINTING RAND GO PREDICTIONS
open(OUT,">$fout") or die "Can\'t open $fout: $!\n";
my @gene_list	= sort keys(%gene_hash);
my @go_list	= sort keys %gofreq_hash;

#my %sampled_gos;

foreach my $gene (@gene_list){
	my %sampled_gos;
	my ($randi,$randgo);
	
	for(my $i=0; $i<$k;$i++){
		do{
			$randi	= int(rand(scalar(@go_list)));
			$randgo	= $go_list[$randi];
		}
		while( defined($sampled_gos{$randgo}) );
		
		$sampled_gos{$randgo}	= 1;
		
		if($use_norm_scores){
			my $norm_score = rand_norm_val($mu,$std);
			print OUT "$gene\t$randgo\t$norm_score\n";
		}
		else{
			print OUT "$gene\t$randgo\t$gofreq_hash{$randgo}\n";
		}
	}
}
close(OUT);
	
	
#################
# Sub-functions #
#################

sub rand_norm_val {

   # code copied from these places
   #
   # http://code.activestate.com/recipes/143083-normally-distributed-random-numbers/
   # http://users.wfu.edu/rollins/normal_random.html
   # http://mathworld.wolfram.com/Box-MullerTransformation.html

   my ($mu, $std) = @_;
   my $x = rand(1);
   my $pi = 3.14159;
   $x = sqrt( -2 * log(rand()) ) * cos( 2 * $pi * rand());
   $x = $std*$x + $mu;
   return $x;
}
