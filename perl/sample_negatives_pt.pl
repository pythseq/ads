#!/usr/bin/perl
use lib "/data/petrit/GOA_annotations/Ilja_GO_project/perl_modules/lib/perl5";
use strict;
use warnings;
use Set::Scalar;

# Samples negative (wrong) annotations for a given list of genes

my $usage= 	"USAGE: sample_negatives.pl pos_set neg_set go_parents k ID_col GO_col GO_sizes\n".
		"pos_set   \tannotation file with correct annotations, Gene_ID col and GO_ID col selected below\n".
		"          \tNOTE: this must be ordered by gene!!\n".
		"neg_set   \tthis is were we write neg annotations\n".
		"go_parents\tfile containing parent GOs for each child GO, <GO\tGO1[,GO2..]>\n".
		"k         \tnumber of negative terms sampled for each gene\n".
		"ID_col    \tColumn for gene ID in pos_set\n".
		"GO_col    \tColumn for GO ID in pos_set\n".
		"GO_sizes  \tTable with GO-class sizes. Format: GO-ID\tCount\tother data\n".
		"          \tThis table is used in fully random mode. Do NOT define it if you want to use\n".
		"          \tcertain negative classes. Defining it turns on the random mode\n" ;

if(scalar(@ARGV)<6) { die $usage; }

my $pos_setf=    shift(@ARGV);
my $neg_setf=    shift(@ARGV);
my $go_parentsf= shift(@ARGV);
my $k= shift(@ARGV);
my $ID_col = shift(@ARGV);
my $go_col = shift(@ARGV);
my $go_sizes_f = "";
if(scalar(@ARGV)==1) {
  $go_sizes_f = shift(@ARGV);
}
# Parameter I define here

my $UseGaussian = 1; # True - False values => 1 - 0
my $mu = -0.5;  #Two parameters for gaussian
my $std = 0.5;
my $MaxScoreVal = 0.4; # parameter for uniform distribution

print "reading positives from $pos_setf..\n";

my ($gene,$gene_prev,$go);
my @goa_list;
my %pos_set;
my @tmp_table;

open(IN,"<$pos_setf") or die "Can\'t open $pos_setf: $!\n";
while(my $l=<IN>){
	if ( $l =~ m/^[\!\#]/){ next;} # comment line
	chomp($l);
	#($gene_prev,$go)= split(/\t/,$l);
	@tmp_table= split(/\t/,$l);
	$gene_prev = $tmp_table[$ID_col];
	$go = $tmp_table[$go_col];
	$go =~ s/^GO://;
	push(@goa_list,$go);
	last;
}
while(my $l=<IN>){
	if ( $l =~ m/^[\!#]/){ next; } # comment line
	chomp($l);
	#($gene,$go)= split(/\t/,$l);
	@tmp_table= split(/\t/,$l);
	$gene = $tmp_table[$ID_col];
	$go = $tmp_table[$go_col];
	
	$go =~ s/^GO://;
	if($gene ne $gene_prev){
		my @temp= @goa_list;
		$pos_set{$gene}= \@temp;
		@goa_list=();
		$gene_prev= $gene;
	}
	push(@goa_list,$go);
}
if($#goa_list+1 > 0){
	my @temp= @goa_list;
	$pos_set{$gene}= \@temp;
}
close(IN);
# DEBUG
print "\tgene count: ",(scalar keys(%pos_set)),"\n";
#my @k= sort keys(%pos_set);
#for(my $i=0; $i<5; $i++){
#	print $k[$i]," > ",join(',',@{$pos_set{$k[$i]}}),"\n";
#}


print "reading GO parents from $go_parentsf..\n";
my %go_parents;
my ($child,$parent_list);

open(IN,"<$go_parentsf") or die "Can\'t open $go_parentsf: $!\n";
while(my $l=<IN>){
	if ( $l =~ m/^[\!#]/){ next;} # comment line
	chomp($l);
	($child,$parent_list)= split(/\t/,$l);
	$child =~ s/^\s+|\s+$//g;
	my @temp= split(/,/,$parent_list);
	push @temp, $child;			# PT modified this 
	$go_parents{$child}= \@temp;
}
close(IN);

# Manually add some parent - child definitions
# These are root nodes
# PT modified this

my @temp = ( "0003674" );
$go_parents{"0003674"}= \@temp;
@temp = ( "0008150" );
$go_parents{"0008150"}= \@temp;
@temp = ( "0005575" );
$go_parents{"0005575"}= \@temp;

# However, these classes should *NOT* be suggested 
# as potential false positive or true positive classes
# These should be filtered!!

my @GO_counts2 = ();
my $loop_count = 0;

if( $go_sizes_f) {

  print "reading class sizes\n";
  open(IN, "<$go_sizes_f") or die "File GO sizes did not open \n";
  my $GO_id = "";
  my $count = "";

  while(my $row = <IN>){
      if ( $row =~ m/^[\!\#]/){ next;} # comment line
      chomp $row;
      ($GO_id, $count) = split("\t", $row);
      $loop_count = $loop_count + $count;
      my @tmp = ($GO_id, $loop_count);
      push @GO_counts2, \@tmp;
  }
  close(IN);

}

# DEBUG
#print "DEBUG go_parents:\n";
#print "keys:",scalar(keys(%go_parents)),"\n";
#my @k= sort keys(%go_parents);
#for(my $i=0; $i<20; $i++){
#	print "\'",$k[$i],"\' > ",join(',',@{$go_parents{$k[$i]}}),"\n";
#}


# list of GOs to sample from
# NOTE: only include nodes with parents defined
my %go_hash;
foreach $gene(sort keys %pos_set){
	foreach $go(@{$pos_set{$gene}}){
		if(defined($go_parents{$go})){
			$go_hash{$go}= 1;
		}
	}
}
my @go_list= sort keys(%go_hash);



# sampling
print "sampling $k neg terms for each gene..\n";
open(OUT,">$neg_setf") or die "Can\'t open $neg_setf: $!\n";

my $go_list_size= scalar(@go_list);
my $Jacc_threshold= 0.10;
my $iteration_limit= 400;
my $counter=0;
my $reject_counter = 0;

foreach $gene(sort keys %pos_set){
	# DEBUG
	#print "DEBUG: $gene\n";

	my @gos_sampled;
	my $iterations= 0;
	my %tmp_go_hash = %go_hash;  # PT modified
	
	# BELOW:
	# remove positive classes and their parents from selection
	# store remaining GO classes to tmp_go_hash
	
	my $pos_set_sz = 0;	
	foreach my $term(@{$pos_set{$gene}}){ # PT modified
	   delete @tmp_go_hash{ @{$go_parents{$term} } };
	   $pos_set_sz++;  
	}
	my @tmp_go_list =  keys %tmp_go_hash ;
	my $tmp_size = scalar( @tmp_go_list );
	#print " $go_list_size AND $tmp_size AND $pos_set_sz\n";
	if( $go_sizes_f){
	   
	   my $tmp_ref = get_random_GO_class2($loop_count, \@GO_counts2, $k);
	   @gos_sampled = @$tmp_ref;
	}
	else{
	   while(scalar(@gos_sampled) < $k){
		my $goi= int(rand($tmp_size));
		my $go= $tmp_go_list[$goi];
		splice @tmp_go_list, $goi, 1;
		$tmp_size--;		
		my $neighbour_in_pos= 0;		
		my %tmp_parents = map {$_ => 1 } @{$go_parents{$go} }; #PT Modified
		
		foreach my $term(@{$pos_set{$gene}}){		
		
		        if( $tmp_parents{$term}){  # PT modified
				$neighbour_in_pos= 1;
				last;			
			}	
			my $Jacc= get_Jaccard($go,$term,\%go_parents);
			if($Jacc > $Jacc_threshold){
				$neighbour_in_pos= 1;
				last;
			}
		}		
		if( !$neighbour_in_pos ){
			push(@gos_sampled,$go);
		}		
		if( ($iterations++) > $iteration_limit){
		        # HERE SHOULD CHECK IF ARRAY @tmp_go_list GOES EMPTY. 
			# probably unlikely
			$reject_counter++;
			print "checked $iterations candidate negatives for gene $gene: skipping. Failure count: $reject_counter\n";
			last;
		}	
	   }
	}
	foreach $go (@gos_sampled){
	
		my $score;
		if($UseGaussian){
		   $score = rand_norm_val($mu, $std);
		}
		else{
		  $score = 0.0 + rand($MaxScoreVal);
		}
		
		print OUT "$gene\tGO:$go\t$score\n";
	}
	
	$counter++; 
	#if($counter>5){	last;}
}
close(OUT);


sub get_Jaccard{
	my $child1= shift(@_);
	my $child2= shift(@_);
	my $go_parents_ref= shift(@_);
	
	if(!defined($go_parents_ref->{$child1})){
		print "WARNING: get_Jaccard: no parents for ch1=\'$child1\'\n";
		return 0;
	}
	if(!defined($go_parents_ref->{$child2})){
		print "WARNING: get_Jaccard: no parents for ch2=\'$child2\'\n";
		return 0;
	}	
	my @parents1= @{$go_parents_ref->{$child1}};
	my @parents2= @{$go_parents_ref->{$child2}};
	
	my $set1= Set::Scalar->new(@parents1);
	my $set2= Set::Scalar->new(@parents2);
	
	my $un= $set1->union($set2);
	my $in= $set1->intersection($set2);
	
	return ($in->size())/($un->size());
}


sub rand_norm_val {

   # code copied from these places
   #
   # http://code.activestate.com/recipes/143083-normally-distributed-random-numbers/
   # http://users.wfu.edu/rollins/normal_random.html
   # http://mathworld.wolfram.com/Box-MullerTransformation.html

   my ($mu, $std) = @_;
   my $x = rand(1);
   my $pi = 3.14159;
   $x = sqrt( -2 * log(rand()) ) * cos( 2 * $pi * rand());
   $x = $std*$x + $mu;
   return $x;
}


sub get_random_GO_class{
   
  my $max_val = shift(@_);
  my $GO_counts_tab_ref = shift(@_);
  my $x = int(rand($max_val)) + 1;
  my $output = "";
  foreach my $tmp (@$GO_counts_tab_ref){
     my @inner_list = @$tmp;
     if( $inner_list[1] > $x ){
	$output = $inner_list[0];
	last;
     }
     
  }
  return $output;
}

sub get_random_GO_class2{
   
  my $max_val = shift(@_);
  my $GO_counts_tab_ref = shift(@_);
  my $how_many = shift(@_);
  my @x = map { int(rand($max_val)) + 1} ( 1..$how_many );
  @x = sort {$b <=> $a} @x;
  my $prev_ID = "";
  my @output = ();
  my $tmp_length = "";
  OUTER: foreach my $tmp (@$GO_counts_tab_ref){
     my @inner_list = @$tmp;

     INNER: for(my $i =0; $i < scalar(@x); $i++){
       if( scalar( @x ) <= $i ){
          last OUTER;
       }
       if( $x[$i] && $inner_list[1] > $x[$i] ){
          #$tmp_length = @inner_list;
          $tmp_length = @x;
	  my @tmp = splice @x, $i, $tmp_length - $i;
	  @tmp = ( $inner_list[0] ) x scalar( @tmp );
	  unshift @output, @tmp;  	  
	  if( scalar( @x ) == 0){
             last OUTER;
          }
	  last INNER;
       }
     }
     
  }
  @output = uniq(@output);
  return (\@output );
}

sub uniq {
    # http://stackoverflow.com/questions/7651/how-do-i-remove-duplicate-items-from-an-array-in-perl
    my %seen;
    grep !$seen{$_}++, @_;
}
