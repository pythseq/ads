#!/usr/bin/perl
use strict;
use warnings;

BEGIN{
   use Cwd;
   my $pwd = cwd();
   
   # PT comment: Did not get use lib to work
   # However, unshift @INC seems to work ... ?
   
   unshift @INC, "$pwd/PerlModules/";
   unshift @INC, "$pwd/perl/PerlModules/";
   unshift @INC, "$pwd";
}
use Set::Scalar;

my $usage=  "USAGE: perl get_Smin_InfCont3.pl go.obo go_parents.table gene_association.table data_out > debug_output\n".
            "go.obo			GO OBO file \n".
	    "go_parents.table		table with GO-parent list for each GO class\n".
	    "gene_association.table	GO annotations for genes\n".
	    "data_out			output table: GO-ID tab class_count tab parent_count tab ratio tab IC-value\n".
	    "EXAMPLE: \n".
            "perl get_Smin_InfCont3.pl ../data/go.obo ../data/goparents.tab ../data/gene_association.goa_ref_uniprot.flt Smin_IC_values > output\n";

unless( @ARGV == 4 ) { 
    die $usage; 
}
 
my $file1_in = shift(@ARGV);	# GO OBO
my $file2_in = shift(@ARGV);	# list of GO parents
my $file3_in = shift(@ARGV);	# direct annotations
my $file_out = shift(@ARGV);	# Output table

###############################################
# Here I create a hash of immediate parents
# for each GO class
###############################################

open(IN,"<$file1_in") or die("file 1 was not found\nget_Smin_InfCont.pl\n" );
my $GO_ID = "";
my $parent_ID ;
my $Read_ID = 0;
my $Read_data = 0;
my $write_data = 0;
my $ID = "";
my @ParentList = ();
my %ParentHash = ();
while(my $line_in = <IN>) {
    chomp $line_in;
    #print "$Read_ID\t";
    if($Read_ID){
       $ID = $line_in;
       $ID =~ s/(id\: )//; 
       if($1){
         $ID =~ s/GO\://;
         #print "ID: $ID \n"; 
       }
       $Read_ID = 0; 
       $Read_data = 1; 
       @ParentList = ();
       $write_data = 1;     
    }
    if($Read_data){  # consider is included because it maps old anno's to OBO
       if( ( $line_in =~ /(^is\_a\:)|(part\_of)|(^consider\:).+GO\:/ ) ){
          
          $line_in =~ /GO\:([\d]{7})/;
	  $parent_ID = $1;
          #print "$line_in\t$parent_ID \n";
	  if(length($parent_ID)){
	     push @ParentList, $parent_ID;
	     if($parent_ID eq "is_a"){
	        print "CHECK: $parent_ID\n";
	        print "$line_in\n";
             }
	  } 
       }
    }
    
    if( ($line_in =~ /\[Term\]/)){ # This starts GO-ID report
       $Read_ID = 1;   
       if( $write_data){  # Check we have data
         $Read_data = 0;
         $ParentHash{$ID} = [ @ParentList ];
         #print "ID: $ID ended\n";
         #print join(", ", @ParentList);
         #print "\n";
         $ID ="";
       }   
       $write_data = 0;
    }
}
close(IN);

#######################################################
# Here I create a hash of each go classes parent path
# This is the list of GO classes till the root node
#######################################################

open(IN,"<$file2_in") or die("file 2 was not found\nget_Smin_InfCont.pl\n" );

my %ParentHash2 = ();
my @tmp_arr;
my @tmp_arr2;
my %tmp_hash;
my $count = 0;
my $previous_ID;

while(my $line_in = <IN>) {
    #print $line_in;
    chomp $line_in;
    @tmp_arr = split("\t", $line_in);
    if( $tmp_arr[1]){
      @tmp_arr2 = split(",", $tmp_arr[1]);
      %tmp_hash = map { $_ => 1 } @tmp_arr2;
      $ParentHash2{$tmp_arr[0]} = { %tmp_hash } ;
      if( $count< 4){   # DEBUG control print. This can be removed
         print "$tmp_arr[0] ARRAY: ";
         print join(", ", @tmp_arr2);
         print "\n";
         print join(", ", keys %tmp_hash);
         print "\n";
         print join(", ", keys %{$ParentHash2{$tmp_arr[0]}});
         #print "\n$previous_ID = \n";
         #print join(", ", keys %{$ParentHash2{$previous_ID}});
         print "\n";
	 $count++;
      }
    }
    else{
      %tmp_hash = ();
      $ParentHash2{$tmp_arr[0]} = \%tmp_hash;
    }
    $previous_ID = $tmp_arr[0];
}

$count = 0;

#foreach my $go (keys %ParentHash2){
#
#  print "$go  => \t";
#  print "$ParentHash2{$go}\t";
#  print join(", ", keys %{$ParentHash2{$go}});
#  print "\n";
#  $count++;
#  
#}

# Now read in the DB annotations. 

open(IN,"<$file3_in") or die("file 3 was not found\nget_Smin_InfCont.pl\n" );

my $gene_ID;
my $earlier_gene;
#my $GO_ID;
my %Gene_Annos = ();
my %Old_Annos = ();
$count = 0;

while(my $line_in = <IN>) {
  if( !( $line_in =~ /^\!/)){
     #print $line_in;
     chomp $line_in;
     @tmp_arr = split("\t", $line_in);
     #print "$tmp_arr[1]\t$tmp_arr[4]\n";
     if($Gene_Annos{$tmp_arr[1]}){
        #print "OLD\t";
        %Old_Annos = %{$Gene_Annos{$tmp_arr[1]}};
     }
     else{
        #print "NEW\t";
        %Old_Annos = ();
     }
     $tmp_arr[4] =~ s/GO\://;
     $Old_Annos{$tmp_arr[4]} = 1;
     #print join(", ", keys %Old_Annos);
     #print "\n$tmp_arr[4]\tPARENT-PATH\t"; 
     #print join(", ", keys %{$ParentHash2{$tmp_arr[4]}});
     #print "\n";
     #print "\nMORE\t";
     if( $ParentHash2{$tmp_arr[4]}){
	%Old_Annos = ( %Old_Annos, %{$ParentHash2{$tmp_arr[4]}} );
     }
     $Gene_Annos{$tmp_arr[1]} = { %Old_Annos };
     
     #print "$tmp_arr[1]\t$tmp_arr[4]\tpop\t";
     #print join(", ", keys %{$Gene_Annos{$tmp_arr[1]}});
     #print "\nOLD_ANNO\t";
     #print join(", ", keys %Old_Annos);
     #print "\n";
     
  }

}

print "\n\n Change in output\n";

my %Anno_lists = ();
my @tmp_list = ();
my $anno_string = "";
foreach my $gene (keys %Gene_Annos){

  @tmp_list = sort( keys %{$Gene_Annos{$gene}});
  $anno_string = join( "_", @tmp_list);
  if( $Anno_lists{$anno_string}){
    $Anno_lists{$anno_string}[1]++; 
  }
  else{
    $Anno_lists{$anno_string}[0] = $Gene_Annos{$gene};
    $Anno_lists{$anno_string}[1] = 1;
  }

}

print "Allwent through\n";

my $check_sz = keys %Gene_Annos;
print "Old hash size: $check_sz\n";
$check_sz = keys %Anno_lists;
print "New hash size: $check_sz\n";

#foreach $anno_string (keys %Anno_lists){
#   print "\n$anno_string\n";
#   print join(", ", keys %{$Anno_lists{$anno_string}[0]});
#   print "\n";
#   print "$Anno_lists{$anno_string}[1]\n";
#}


#############################
# Now go through th GO hash #
# Create the count data     #
#############################

open(OUT,">$file_out") or die("file 4 (output) failed\nget_Smin_InfCont.pl\n" );
my %GO_counts_hash = ();
my @count_table;
my @tmp_list ;
my $immed_parents;
my $check;
my $ratio_value = "";
my $IC_value = "";
foreach my $GO_ID (keys %ParentHash){

  @count_table = (0,0);
  foreach my $ID_list (keys %Anno_lists){
  
     if( $Anno_lists{$ID_list}[0]{$GO_ID}){
        $count_table[0] = $count_table[0] + $Anno_lists{$ID_list}[1];
	$count_table[1] = $count_table[1] + $Anno_lists{$ID_list}[1];
     }
     else{
        #print $ParentHash{$GO_ID};
	#print "\n";
        if( @{$ParentHash{$GO_ID}} == grep( $Anno_lists{$ID_list}[0]{$_}, @{$ParentHash{$GO_ID}} ) ){
	  $count_table[1] = $count_table[1] + $Anno_lists{$ID_list}[1];
	  #print "$GO_ID\t". join(", ", @{$ParentHash{$GO_ID}});
	  #print "\n";
	}
     }
  }
  $GO_counts_hash{$GO_ID} = [ @count_table ];
  if( $count_table[0] > 0){
     $ratio_value = $count_table[0] /$count_table[1] ;
     if( $ratio_value == 1){
        $IC_value    = 0;
     }
     else{
        $IC_value    = -log( $ratio_value);
     }
     print OUT "$GO_ID\t$count_table[0]\t$count_table[1]\t$ratio_value\t$IC_value\n";
     #print "$GO_ID\t$GO_counts_hash{$GO_ID}\n";
  } 
}    
  
