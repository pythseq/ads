#!/usr/bin/perl
use strict;
use warnings;

# Modifies mouseFunc (2006) prospective data set to <gene\tGO1[,GO2..]\n> format

# input: 
# Gene\tGO1\tGO2\t\tGO3..
# gene_id\t0\t1\t-2\t..
# gene_id\t1\t0\t0\t..

my $fin= "data/mouseFunc/go_novelAnnotations.txt";
my $fout= "data/mouseFunc/go_novelAnnotations.genego";


print STDERR "reading $fin\n";
print STDERR "printing $fout\n";

open(IN,"<$fin") or die "Can\'t open $fin: $!\n";
open(OUT,">$fout") or die "Can\'t open $fout: $!\n";

my $l=<IN>;
chomp($l);
my @col_labels= split('\t',$l);
for(my $i=1; $i<=$#col_labels; $i++){
	$col_labels[$i] =~ s/"//g;
	$col_labels[$i] =~ s/GO:([0-9]{7})[\w\.]*/$1/;
}


while(my $l=<IN>){
	my @a= split('\t',$l);
	my $gene_id= $a[0];
	$gene_id =~ s/"//g;
	

	for(my $i=1; $i<=$#a;$i++){		# first col are the col names
		if($a[$i]==1){
			print OUT "$gene_id\t$col_labels[$i]\n";
		}
	}
}
		
close(IN);
close(OUT);
