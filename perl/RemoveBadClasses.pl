#!/usr/bin/perl
use strict;
use warnings;

# filters un-wanted classes from annotation data
# Unwanted classes are usually too broad classes
# Currently this code filters only Protein Binding (GO:0005515)
# Main purpose is to filter unwanted classes from CAFA data sets.

my $usage= 	"Perl code for removing un-wanted GO-classes.\n".
                "Removes only Protein Binding currently\n".
                "USAGE: perl FilterBadClasses.pl data_in data_out\n\n".
		"data_in \tCAFA GOA annotation table\n".
		"data_out\tannotation table without un-wanted GO classes\n\n";


if( $#ARGV< 1 ) { die $usage; }

my $fin= shift(@ARGV);
my $fout= shift(@ARGV);

open(IN,"<$fin") or die "Can\'t open $fin: $!\n";
open(OUT,">$fout") or die "Can\'t open $fout: $!\n";

my %ExclTerms = (		#Use hash. Now we can add more terms later
		"0005515" => 1
		);

while(my $line=<IN>){
	if( !($line =~ m/^[\!\#]/)) {
		my @table = split('\t',$line);
		$table[1] =~ s/\s$//;
		$table[1] =~ s/^\s*GO\://;
		if(!$ExclTerms{$table[1]}){
		   print OUT $line;
		} 
	}
}
