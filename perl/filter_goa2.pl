#!/usr/bin/perl
use strict;
use warnings;

# filters ISS and IEA fields from *.goa_ref_uniprot data

my $usage= 	"USAGE: filter_goa.pl goa.gaf goa.gocounts 1> goa_flt.gaf\n\n".
		"goa.gaf      \tGOA annotation table in GAF format\n".
		"goa.gocounts \ttable with columns: GO, count, freq, ic(optional)\n".
		"\n\n";

if( scalar (@ARGV)< 2 ) { die $usage; }

my $fin1= shift(@ARGV);
my $fin2= shift(@ARGV);
my $th	= 0.20;
my $gaf_go_col	= 5 -1;

my %go2freq;
read_tohash($fin2,0,2,\%go2freq);

open(IN,"<$fin1") or die "Can\'t open $fin1: $!\n";
my $count_flt	= 0;
my $count_pass	= 0;
my $count_ln	= 0;
while(my $l=<IN>){
       	$count_ln++; if($count_ln%50000000 == 0){print STDERR "\t $count_ln lines read\n"};
	chomp($l);
	my @sp= split(/\t/,$l,-1);
	my $go= $sp[$gaf_go_col];
	$go =~ s/GO://;
        if( !defined($go2freq{$go}) ){
		die "ERROR: no count data for go: \"$go\"\n";
	}
	if($go2freq{$go} < $th){
		print "$l\n";
		$count_pass++;
	}
	else{
		$count_flt++;
	}
}
close(IN);
printf STDERR ("filtered\t:%u\n",$count_flt);
printf STDERR ("passed  \t:%u\n",$count_pass);
printf STDERR ("total   \t:%u\n",$count_ln);


# read_hash(file,key_ind,val_ind,\%hash)
sub read_tohash{
	my $file= shift;
	my $keyi= shift;
	my $vali= shift;
	my $hashp= shift;
	print STDERR "# read_tohash from $file\n";
	open(IN,"<$file") or die "Can\'t open $file: $!\n";
	my $ln=0;
	while(my $l=<IN>){
        	$ln++; if($ln%50000000 == 0){print STDERR "\t $ln lines read\n"};
		chomp($l);
		my @sp= split(/\t/,$l,-1);
        	$hashp->{$sp[$keyi]} = $sp[$vali];
	}
	close(IN);
}

