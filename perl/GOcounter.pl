#!/usr/bin/perl
use strict;
use warnings;

# 
# Counts occurences of uniq GO-terms in GO annotation table.
# Adds to these counts occurances of parents nodes based on parent map.
# Scales counts by DATABASE_SIZE or MAX_CLASS_SIZE (modify in code).
#
# ARGUMENTS:
# input1	GOA annotation table
# input2	GO parents table

my $usage= 	"USAGE: $0 goa_file gop_file\n\n".
		"goa_file     :\tUniProt GOA annotation table\n".
		"gop_file     :\tcomma-sep list of parents for each GO-class. Format <GO\\tGOP1[,GOP2..]>\n\n";
if(scalar(@ARGV)<2) { die $usage;}

my $GOA_FILE= shift(@ARGV); # gene\tgo\n
my $GOP_FILE= shift(@ARGV); # go\tparents(comma separated)\n


# read parents to hash
my %parents;
open(IN,"<$GOP_FILE");
while(<IN>) {
	chomp;
	my($child,$parent_list)= split(/\t/);
	$child =~ s/\s+//;
	$parents{$child}= $parent_list;
	
	# DEBUG
	#print "Ch:\'$child\'\tPR:$parent_list\n"; exit;
}
close(IN);

# count genes annotated with each go
my %cnt;
my $DB_size = 0;
my $max_class_size = 0;
open(IN,"<$GOA_FILE");
while(<IN>) {
	# comment lines
	if ( (m/^\!/) || (m/^#/)){
		next;
	}

	chomp;
	my @fields = split(/\t/);
	my $go= $fields[4];
	$go =~ s/^GO://;
	
	# DEBUG
	#print "go:$go\n"; last;
	
	$cnt{$go}++;
	$DB_size++;
	
	if(defined($parents{$go})){
		foreach(split(/,/,$parents{$go})){
			$cnt{$_}++;
			$DB_size++;
		}
	}
	else{
		if(!($go==5575 || $go==8150 || $go==3674)){
			print STDERR "WARNING: no parents found for \'GO:$go\'\n";
		}
	}
}
foreach( keys (%cnt) ){
	if( $cnt{$_} > $max_class_size){
		$max_class_size= $cnt{$_};
	}
}



# output counts
print "#DB_SIZE=$DB_size\n";
print "#MAX_CLASS_SIZE=$max_class_size\n";
print "#GO\tCOUNT\tSCALED_COUNT\tIC\n";
foreach (sort { $cnt{$b}<=>$cnt{$a} } keys(%cnt) ) {
	print join("\t",$_,$cnt{$_},($cnt{$_}/$max_class_size),-log($cnt{$_}/$max_class_size)),"\n";
}

