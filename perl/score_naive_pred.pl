#! /usr/bin/perl
use strict;
use warnings;

#
# SCORES NAIVE PREDICTIONS WITH A SET OF SCORING OPTIONS
# 
# PRINTS RESULTS IN A TABLE
#
# run from /data/ilja/go/

my $goscores= "cpp/v2.4/goscores";
my $data= "data";
my $obo_file= "$data/go.obo";
my $ic_file=  "$data/uniprot.ext.ic";

my $pred = "data/uniprot.1000.100naive";
my $pos = "data/uniprot.1000.genegosc";



my %calls= (
	# GENE LIST FOR EACH GO
	"gene.mean.auc" 	=> "$goscores -p $pred -t $pos -b $obo_file -g -m LIST=gene,SUMF1=mean,SF=AUC",
	"gene.mean.jacc" 	=> "$goscores -p $pred -t $pos -b $obo_file -g -m LIST=gene,SUMF1=mean,SF=Jacc",
	
	# <GENE,GO> lists
	"gene_go.auc"		=> "$goscores -p $pred -t $pos -b $obo_file -g -m LIST=gene_go,SF=AUC",
	"gene_go.jacc"		=> "$goscores -p $pred -t $pos -b $obo_file -g -m LIST=gene_go,SF=Jacc",
	
	
	# AUC, U, U1, U2, JACC and WJacc for GO-data without thresholding
	"go.all.mean.auc"	=> "$goscores -p $pred -t $pos -b $obo_file -g -m LIST=go,TH=all,SUMF1=mean,SF=AUC",
	"go.all.mean.u"		=> "$goscores -p $pred -t $pos -b $obo_file -g -m LIST=go,TH=all,SUMF1=mean,SF=U",
	"go.all.mean.u1"	=> "$goscores -p $pred -t $pos -b $obo_file -g -m LIST=go,TH=all,SUMF1=mean,SF=U1",
	"go.all.mean.u2"	=> "$goscores -p $pred -t $pos -b $obo_file -g -m LIST=go,TH=all,SUMF1=mean,SF=U2",
	"go.all.mean.jacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=all,SUMF1=mean,SF=Jacc",
	"go.all.mean.wjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=all,SUMF1=mean,SF=wJacc",	

	
	
	# JACC and WJACC with thresholding
	"go.th.mean.max.jacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=Jacc",
	"go.th.mean.max.wjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=wJacc",
	
	
	# CAFA2012 measures
	"go.fmax"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,SF=Fmax",
	"go.smin"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,SF=Smin",
	
	
	# SIM MATRIX based measures without thresholding
	"go.all.mean.score_A.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=all,SUMF1=mean,SF=score_A,SIMF=pJacc",
	"go.all.mean.score_B.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=all,SUMF1=mean,SF=score_B,SIMF=pJacc",
	"go.all.mean.score_C.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=all,SUMF1=mean,SF=score_C,SIMF=pJacc",
	"go.all.mean.score_D.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=all,SUMF1=mean,SF=score_D,SIMF=pJacc",
	"go.all.mean.score_E.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=all,SUMF1=mean,SF=score_E,SIMF=pJacc",
	"go.all.mean.score_F.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=all,SUMF1=mean,SF=score_F,SIMF=pJacc",

	
	# SIM MATRIX based measures with thresholding
	"go.th.mean.max.score_A.id"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_A,SIMF=id",
		
	"go.th.mean.max.score_A.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_A,SIMF=pJacc",
	"go.th.mean.max.score_B.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_B,SIMF=pJacc",
	"go.th.mean.max.score_C.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_C,SIMF=pJacc",
	"go.th.mean.max.score_D.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_D,SIMF=pJacc",
	"go.th.mean.max.score_E.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_E,SIMF=pJacc",
	"go.th.mean.max.score_F.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_F,SIMF=pJacc",

	
	"go.th.mean.mean.score_A.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=mean,SF=score_A,SIMF=pJacc",
	"go.th.mean.mean.score_B.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=mean,SF=score_B,SIMF=pJacc",
	"go.th.mean.mean.score_C.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=mean,SF=score_C,SIMF=pJacc",
	"go.th.mean.mean.score_D.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=mean,SF=score_D,SIMF=pJacc",
	"go.th.mean.mean.score_E.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=mean,SF=score_E,SIMF=pJacc",
	"go.th.mean.mean.score_F.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=mean,SF=score_F,SIMF=pJacc",	


	"go.geneth.mean.max.score_A.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=geneth,SUMF1=mean,SUMF2=max,SF=score_A,SIMF=pJacc",
	"go.geneth.mean.max.score_B.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=geneth,SUMF1=mean,SUMF2=max,SF=score_B,SIMF=pJacc",
	"go.geneth.mean.max.score_C.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=geneth,SUMF1=mean,SUMF2=max,SF=score_C,SIMF=pJacc",
	"go.geneth.mean.max.score_D.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=geneth,SUMF1=mean,SUMF2=max,SF=score_D,SIMF=pJacc",
	"go.geneth.mean.max.score_E.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=geneth,SUMF1=mean,SUMF2=max,SF=score_E,SIMF=pJacc",
	"go.geneth.mean.max.score_F.pjacc"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=geneth,SUMF1=mean,SUMF2=max,SF=score_F,SIMF=pJacc",
	
	
	# SEMANTIC SIMILARITY FOR COMPARISON
	"go.th.mean.max.score_A.lin"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_A,SIMF=Lin",
	"go.th.mean.max.score_B.lin"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_B,SIMF=Lin",
	"go.th.mean.max.score_C.lin"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_C,SIMF=Lin",
	"go.th.mean.max.score_D.lin"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_D,SIMF=Lin",
	"go.th.mean.max.score_E.lin"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_E,SIMF=Lin",
	"go.th.mean.max.score_F.lin"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_F,SIMF=Lin",
	
	"go.th.mean.max.score_A.Resnik"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_A,SIMF=Resnik",
	"go.th.mean.max.score_B.Resnik"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_B,SIMF=Resnik",
	"go.th.mean.max.score_C.Resnik"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_C,SIMF=Resnik",
	"go.th.mean.max.score_D.Resnik"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_D,SIMF=Resnik",
	"go.th.mean.max.score_E.Resnik"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_E,SIMF=Resnik",
	"go.th.mean.max.score_F.Resnik"	=> "$goscores -p $pred -t $pos -b $obo_file -i $ic_file -g -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_F,SIMF=Resnik");
		
my @keys= (
	# GENE LIST FOR EACH GO
	"gene.mean.auc","gene.mean.jacc",

	# <GENE,GO> lists
	"gene_go.auc","gene_go.jacc",	
	
	# AUC, U, U1, U2, JACC and WJacc for GO-data without thresholding
	"go.all.mean.auc",
	"go.all.mean.u",
	"go.all.mean.u1",
	"go.all.mean.u2",
	"go.all.mean.jacc",
	"go.all.mean.wjacc",
	
	# JACC and WJACC with thresholding
	"go.th.mean.max.jacc","go.th.mean.max.wjacc",	
	
	# CAFA2012 measures
	"go.fmax","go.smin",
		
	# SIM MATRIX based measures without thresholding
	"go.all.mean.score_A.pjacc",
	"go.all.mean.score_B.pjacc",
	"go.all.mean.score_C.pjacc",
	"go.all.mean.score_D.pjacc",
	"go.all.mean.score_E.pjacc",
	"go.all.mean.score_F.pjacc",
	
	# SIM MATRIX based measures with thresholding
	"go.th.mean.max.score_A.id",		
	"go.th.mean.max.score_A.pjacc",
	"go.th.mean.max.score_B.pjacc",
	"go.th.mean.max.score_C.pjacc",
	"go.th.mean.max.score_D.pjacc",
	"go.th.mean.max.score_E.pjacc",
	"go.th.mean.max.score_F.pjacc",
	
	"go.th.mean.mean.score_A.pjacc",
	"go.th.mean.mean.score_B.pjacc",
	"go.th.mean.mean.score_C.pjacc",
	"go.th.mean.mean.score_D.pjacc",
	"go.th.mean.mean.score_E.pjacc",
	"go.th.mean.mean.score_F.pjacc",
	
	"go.geneth.mean.max.score_A.pjacc",
	"go.geneth.mean.max.score_B.pjacc",
	"go.geneth.mean.max.score_C.pjacc",
	"go.geneth.mean.max.score_D.pjacc",
	"go.geneth.mean.max.score_E.pjacc",
	"go.geneth.mean.max.score_F.pjacc",
	
	# SEMANTIC SIMILARITY FOR COMPARISON
	"go.th.mean.max.score_A.lin",
	"go.th.mean.max.score_B.lin",
	"go.th.mean.max.score_C.lin",
	"go.th.mean.max.score_D.lin",
	"go.th.mean.max.score_E.lin",
	"go.th.mean.max.score_F.lin",
	
	"go.th.mean.max.score_A.Resnik",
	"go.th.mean.max.score_B.Resnik",
	"go.th.mean.max.score_C.Resnik",
	"go.th.mean.max.score_D.Resnik",
	"go.th.mean.max.score_E.Resnik",
	"go.th.mean.max.score_F.Resnik");


# RUNING
foreach my $k(@keys){

	print "$k\t";
	if(system($calls{$k}) != 0){
		print STDERR "ERROR: failed to execute:",$calls{$k},"\n";
		exit(1);
	}
}

		
