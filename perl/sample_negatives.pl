#!/usr/bin/perl
use strict;
use warnings;
BEGIN{
   use Cwd;
   my $pwd = cwd();

   # PT comment: Did not get use lib to work
   # However, unshift @INC seems to work ... ?

   #use lib "$pwd/perl/PerlModules/";
   #use lib $pwd ;
   unshift @INC, "$pwd/perl/PerlModules/";
   unshift @INC, "$pwd";
}
use Set::Scalar;

# Samples negative (wrong) annotations for a given list of genes

my $usage= 	"\nUSAGE: $0 pos_set neg_set go_parents k rseed\n\n".
		"pos_set   \tannotation file with correct annotations, <gene\\tgo\\tscore>\n".
		"          \tNOTE: this must be ordered by gene!!\n".
		"neg_set   \tthis is were we write neg annotations\n".
		"go_parents\tfile containing parent GOs for each child GO, <GO\tGO1[,GO2..]>\n".
		"k         \tnumber of negative terms sampled for each gene\n".
		"rseed     \tseed for random number generator\n\n";

if(scalar(@ARGV)<5) { die $usage; }

my $pos_setf=    shift(@ARGV);
my $neg_setf=    shift(@ARGV);
my $go_parentsf= shift(@ARGV);
my $k= 	shift(@ARGV);
my $rseed= 	shift(@ARGV);


print "# reading positives from $pos_setf..\n";

my ($gene,$gene_prev,$go);
my @goa_list;
my %pos_set;
open(IN,"<$pos_setf") or die "Can\'t open $pos_setf: $!\n";
while(my $l=<IN>){
	if ( $l =~ m/^[\!#]/){ next;} # comment line
	chomp($l);
	($gene_prev,$go)= split(/\t/,$l);
	$go =~ s/^GO://;
	push(@goa_list,$go);
	last;
}
while(my $l=<IN>){
	if ( $l =~ m/^[\!#]/){ next; } # comment line
	chomp($l);
	($gene,$go)= split(/\t/,$l);
	$go =~ s/^GO://;
	if($gene ne $gene_prev){
		my @temp= @goa_list;
		$pos_set{$gene_prev}= \@temp;
		@goa_list=();
		$gene_prev= $gene;
	}
	push(@goa_list,$go);
}
if($#goa_list+1 > 0){
	my @temp= @goa_list;
	$pos_set{$gene}= \@temp;
}
close(IN);
print "\t# gene count: ",(scalar keys(%pos_set)),"\n";



print "# reading GO parents from $go_parentsf..\n";
my %go_parents;
my ($child,$parent_list);

open(IN,"<$go_parentsf") or die "Can\'t open $go_parentsf: $!\n";
while(my $l=<IN>){
	if ( $l =~ m/^[\!#]/){ next;} # comment line
	chomp($l);
	($child,$parent_list)= split(/\t/,$l);
	$child =~ s/^\s+|\s+$//g;
	my @temp= split(/,/,$parent_list);
	#if(scalar(@temp) == 0){
	#die "ERROR: ancestor list for node $child is empty\n";}
	$go_parents{$child}= \@temp;
}
close(IN);


# list of GOs to sample from
# NOTE: only include nodes with parents defined
my %go_hash;
foreach $gene(sort keys %pos_set){
	foreach $go(@{$pos_set{$gene}}){
		if(defined($go_parents{$go})){
			$go_hash{$go}= 1;
		}
	}
}
my @go_list= sort keys(%go_hash);



# sampling
print "# sampling $k neg terms for each gene..\n";
open(OUT,">$neg_setf") or die "Can\'t open $neg_setf: $!\n";

my $go_list_size= scalar(@go_list);
my $Jacc_threshold= 0.10;
my $iteration_limit= 5000;
my $counter=0;
# IMPORTANT: seeding random number generator
srand($rseed);

foreach $gene(sort keys %pos_set){
	# DEBUG
	#print "DEBUG: $gene\n";

	my @gos_sampled;
	my $iterations= 0;
	
	while(scalar(@gos_sampled) < $k){
		my $goi= int(rand($go_list_size));
		my $go= $go_list[$goi];
		
		my $neighbour_in_pos= !1;
		
		foreach my $term(@{$pos_set{$gene}}){
		
			my $Jacc= get_Jaccard($go,$term,\%go_parents);
			if( $Jacc > $Jacc_threshold ){
				$neighbour_in_pos= 1;
				last;
			}
		}
		
		if( !$neighbour_in_pos ){
			push(@gos_sampled,$go);
		}
		
		if( ($iterations++) > $iteration_limit){
			print "checked $iterations candidate negatives for gene $gene: skipping\n";
			last;
		}	
	}
	
	foreach $go (@gos_sampled){
	
		print OUT "$gene\tGO:$go\n";
	}
	
	$counter++; 
	#if($counter>5){	last;}
}
close(OUT);


# If Jaccard is undefined returns 0
#
sub get_Jaccard{
	my $child1= shift(@_);
	my $child2= shift(@_);
	my $go_parents_ref= shift(@_);
	
	if(!defined($go_parents_ref->{$child1}) || scalar(@{$go_parents_ref->{$child1}})< 1){
		print STDERR "WARNING: get_Jaccard: no parents for ch1=\'$child1\'\n";
		return 0;
	}
	if(!defined($go_parents_ref->{$child2}) || scalar(@{$go_parents_ref->{$child2}})< 1){
		print STDERR "WARNING: get_Jaccard: no parents for ch2=\'$child2\'\n";
		return 0;
	}	
	my @parents1= @{$go_parents_ref->{$child1}};
	my @parents2= @{$go_parents_ref->{$child2}};

	my $set1= Set::Scalar->new(@parents1);
	my $set2= Set::Scalar->new(@parents2);
	
	my $un= $set1->union($set2);
	my $in= $set1->intersection($set2);
		
	return ($in->size())/($un->size());
}



