	#!/usr/bin/perl
use strict;
use warnings;
use Time::HiRes qw(gettimeofday);
#
# Searching for a high-signal Decoy Prediction Set (DPS) for a given (metric,dataset) pair
# 
# Our tactic is to use SmallGo DPS and to 
# 1. iterate thresholds for class frequency and 
# 2. iterate number of classes predicted for each gene
#

# DEPENDENCIES:
my $goscores	= "./cpp/v2.6/goscores";
#perl get_naive_signal.pl;

my $usage= 	"\nUSAGE: get_smallgo_signal_table.pl data go_counts elab_file signal|score\n".
		"data		: <gene\tgo> annotation\n".
		"go_counts	: <go\tcount\tfreq> sorted by the counts\n".
		"elab_file	: scores from an elab run\n".
		"signal|score	: \"signal\" or \"score\" literal to indicate, which table to print\n".
		"\n".
		"NOTE: data and elab_file should have matching annotation set\n\n";

if(scalar(@ARGV)<4) { die $usage; }

my $data		= shift(@ARGV);
my $go_counts		= shift(@ARGV);
my $elab_file		= shift(@ARGV);
my $signal_or_score	= shift(@ARGV);


# GENERATE NAIVE DECOY DATASETS WITH DIFFERENT PARAMETERS AND SCORE THEM
my @k		= (10,20,30,40,50,100,200,400,800,1000);
my @th		= (0,10,20,30,40,50,60,70,80,100);
my $naive_file		= "naive_".(gettimeofday()*1000).".tmp";
my $signal_file		= "signal_".(gettimeofday()*1000).".tmp";

print "k\\th\t";
for(my $j=0; $j<scalar(@th); $j++){
	print "$th[$j]\t";
}
print "\n";
for(my $i=0; $i<scalar(@k); $i++){
	print "$k[$i]\t";
	for(my $j=0; $j<scalar(@th); $j++){
		
		# DPS SET
		my $call= "perl get_smallgo_pred.pl $data $naive_file $go_counts $th[$j] $k[$i]";
		(system($call)==0) or die "ERROR: failed to run:\n$call\n";
		
		# SCORE DPS SET
  		$call= "perl get_naive_signal.pl $goscores $naive_file $elab_file > $signal_file";
		(system($call)==0) or die "ERROR: failed to run:\n$call\n";
		
		open(IN,"<$signal_file") or die "Can\'t open $signal_file: $!\n";
  		my $l;
		while ($l = <IN>){
			chomp($l);
			my @sp= split(/=/,$l);
			#print "$sp[0]\n"; print "$sp[1]\n"; exit(1);

			if(($sp[0] eq "naive signal") && ($signal_or_score eq "signal")){
				print "$sp[1]\t";
				last;
			}
			if(($sp[0] eq "naive score") && ($signal_or_score eq "score")){
				print "$sp[1]\t";
				last;
			}
		}
		close(IN);
	}
	print "\n";
}
 

# CLEANUP
system("rm -f $naive_file");
system("rm -f $signal_file");

