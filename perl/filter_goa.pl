#!/usr/bin/perl
use strict;
use warnings;

# filters ISS and IEA fields from *.goa_ref_uniprot data

my $usage= 	"USAGE: filter_goa.pl data_in data_out\n\n".
		"data_in \tUniProt GOA annotation table\n".
		"data_out\tannotation table with ISS and IEA filtered\n\n";

if( $#ARGV< 1 ) { die $usage; }

my $fin= shift(@ARGV);
my $fout= shift(@ARGV);

print STDERR "reading $fin\n";
print STDERR "printing $fout\n";

open(IN,"<$fin") or die "Can\'t open $fin: $!\n";
open(OUT,">$fout") or die "Can\'t open $fout: $!\n";

my $iss=0;
my $iea=0;
my $flt=0;

while(my $l=<IN>){
	if (!($l =~ m/^[\!\#]/)) {
		my @a= split('\t',$l);

		if($a[6] =~ m/ISS/){
			$iss++;
			next;
		}
		if($a[6] =~ m/IEA/){
			$iea++;
			next;
		}
		print OUT "$l";
		$flt++;
	}
}
print STDERR "ISS: $iss\n",
		"IEA: $iea\n",
		"FLT: $flt\n",
		"TOT: ",($iss+$iea+$flt),"\n";
		
close(IN);
close(OUT);
