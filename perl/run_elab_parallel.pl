#! /usr/bin/perl
use strict;
use warnings;
use Getopt::Long qw(GetOptions);
Getopt::Long::Configure qw(gnu_getopt);

#
# RUNS A BATCH OF ELAB CALLS IN PARALLEL
#
my $usage= 	"USAGE: $0 -k|perm int -e|emode str -r|rmode int -s|srand int --max_ajac num --goc_db int --goc_on int -p|pos file -n|neg file -b|obo file -i|ic file --ic2 file --resdir dir [--pipe str] [--verbal] [--help]\n\n".
		"-k|perm  int  : num of permutations\n".
		"-e|emode str  : string specifying error levels, e.g. \"0,1,by=0.1\"\n".
		"-r|rmode int  : rotate positive data in neighbourhood of size r\n".
		"-s|srand int  : random seed\n".
		"--max_ajac num: max ancestral jaccard similarity between swaped GO term in the permutation step [0.2]\n".
		"-goc_db  int  : number of uniq GO terms in the database\n".
		"-goc_on  int  : number of uniq GO terms in GO ontology\n".
		"-p|pos   file : positive GO annotation [gene\\tGO\\tscore]\n".
		"-n|neg   file : negative GO annotation [gene\\tGO\\tscore]\n".
		"-b|obo   file : OBO 1.2 file\n".
		"--ic     file : information content file [GO\\tIC]\n".
		"--ic2    file : information content as defined by Clark & Radivojac (2013). Smin metrics are run with both --ic and --ic2 files\n".
		"--resdir dir  : directory for results\n".
		"--pipeh  str  : Part of the pipeline or subset(s) of metrics to run. Default value:\"All\"\n".
		"                This can be (case insensitive):\n".
		"                AUC  to run all AUC metrics + Fmax\n".
		"                Jacc to run all Jaccard metrics\n".
		"                Smin to run Smin-cluster\n".
		"                Sem  to run all Semantic similarity metrics\n".
		"                New  to run subset of newly added metrics\n".
		"                All  to run everything\n".
		"                e.g. --pipe AUC,Jacc,Smin\n".
		"-v|verbal     : run elab in verbal mode\n".
		"-h|help       : print this user manual\n". 
		"DEPENDENCIES  : elab executable\n\n";

# DEPENDENCIES
my $elab= "bin/elab";

my ($perm,$emode,$rmode,$srand,$max_ajac,$goc_db,$goc_on,$data_pos,$data_neg,$obo_file,$ic_file,$ic2_file,$resdir,$pipe,$v, $help);
$rmode	= -1;
$srand	= -1;
$max_ajac	= 0.2;
$pipe 	= "All"; 	# default value
$v	= !1;
$help	= !1;
GetOptions(
        'k|perm=i' 	=> \$perm,
        'e|emode=s' 	=> \$emode,
	'r|rmode=i'	=> \$rmode,
	's|srand=i'	=> \$srand,
	'max_ajac=f'	=> \$max_ajac,
	'goc_db=i'	=> \$goc_db,
	'goc_on=i'	=> \$goc_on,
	'p|pos=s'	=> \$data_pos,
	'n|neg=s'	=> \$data_neg,
	'b|obo=s'	=> \$obo_file,
	'i|ic=s'	=> \$ic_file,
	'ic2=s'		=> \$ic2_file,
	'resdir=s'	=> \$resdir,
	'pipe|pipeh=s'	=> \$pipe,
	'v|verbal'	=> \$v,
	'h|help'	=> \$help) or die $usage;
if($help){
	die $usage;
}
$pipe= lc($pipe);
$v = ($v) ? "-v":"";
if(!$perm){ print STDERR "\nmissing argument: --perm\n\n"; die $usage; }
if(!$emode){ print STDERR "\nmissing argument: --emode\n\n"; die $usage; }
if($rmode<0){ print STDERR "\nmissing argument: --rmode\n\n"; die $usage; }
if($srand<0){ print STDERR "\nmissing argument: --srand\n\n"; die $usage; }
if($max_ajac<0 || $max_ajac>1){ print STDERR "\ninvalid --max_ajac option, use values in [0,1]\n\n"; die $usage; }
if(!$goc_db){ print STDERR "\nmissing argument: --goc_db\n\n"; die $usage; }
if(!$goc_on){ print STDERR "\nmissing argument: --goc_on\n\n"; die $usage; }
if(!$data_pos){ print STDERR "\nmissing argument: --pos\n\n"; die $usage; }
if(!$data_neg){ print STDERR "\nmissing argument: --neg\n\n"; die $usage; }
if(!$obo_file){ print STDERR "\nmissing argument: --obo\n\n"; die $usage; }
if(!$ic_file){ print STDERR "\nmissing argument: --ic\n\n"; die $usage; }
if(!$ic2_file){ print STDERR "\nmissing argument: --ic2\n\n"; die $usage; }
if(!$resdir){ print STDERR "\nmissing argument: --resdir\n\n"; die $usage; }

# DEBUG
#print "perm=\t$perm \nemode=\t$emode \nrmode=\t$rmode \nsrand=\t$srand \ndata_pos=\t$data_pos \ndata_neg=\t$data_neg \nobo_file=\t$obo_file \nic_file=\t$ic_file \nresdir=\t$resdir\n";
#exit(1);

# create res_dir
if(!(-e $resdir)){
	if(!mkdir($resdir)){
	die "ERROR: failed to create directory: $resdir\n";
	}
}

# Generate seed name to output from data_pos string
# Here we remove just the folder path from name
my $seed_name = $data_pos;
$seed_name =~ s/.*\///;

my @AUC=(
	# AUC METRICS
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -g -p $data_pos -n $data_neg -b $obo_file -e $emode $v -m LIST=gene_go,SF=AUC3,GOC=$goc_on -o $resdir/US_AUCROC.elab",
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -g -p $data_pos -n $data_neg -b $obo_file -e $emode $v -m LIST=gene_go,SF=AUCPR -o $resdir/US_AUCPR.elab",
	
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -g -p $data_pos -n $data_neg -b $obo_file -e $emode $v -m LIST=go,TH=all,SUMF1=mean,SF=AUC3,GOC=$goc_on -o $resdir/GC_AUCROC.elab",
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -g -p $data_pos -n $data_neg -b $obo_file -e $emode $v -m LIST=go,TH=all,SUMF1=mean,SF=AUCPR -o $resdir/GC_AUCPR.elab",
	
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -g -p $data_pos -n $data_neg -b $obo_file -e $emode $v -m LIST=gene,TH=all,SUMF1=mean,SF=AUC3 -o $resdir/TC_AUCROC.elab",
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -g -p $data_pos -n $data_neg -b $obo_file -e $emode $v -m LIST=gene,TH=all,SUMF1=mean,SF=AUCPR -o $resdir/TC_AUCPR.elab",
	
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -g -p $data_pos -n $data_neg -b $obo_file -e $emode $v -m LIST=go,SF=Fmax -o $resdir/Fmax.elab"
);
my @jacc=(
	# JACCARD WITH THRESHOLDING
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -g -p $data_pos -n $data_neg -b $obo_file -e $emode $v -m LIST=gene_go,TH=th,SUMF=max,SF=Jacc -o $resdir/max_US_Jacc.elab",
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -g -p $data_pos -n $data_neg -b $obo_file -e $emode $v -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=Jacc -o $resdir/max_GC_Jacc.elab"
);
my @Smin=(
	# Smin cluster
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -g -p $data_pos -n $data_neg -b $obo_file -i $ic2_file -e $emode $v -m LIST=go,SF=Smin -o $resdir/Smin.ic2",
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -g -p $data_pos -n $data_neg -b $obo_file -i $ic_file -e $emode $v -m LIST=go,SF=Smin -o $resdir/Smin.ic"
);
my @sem=(
	# SEMANTIC SIMILARITY METRICS WITH THRESHOLDING
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -p $data_pos -n $data_neg -b $obo_file -i $ic_file -e $emode $v -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_A,SIMF=id -o $resdir/max_id_score_A.elab",
	
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -p $data_pos -n $data_neg -b $obo_file -i $ic_file -e $emode $v -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_A,SIMF=Resnik -o $resdir/max_Resnik_score_A.elab",
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -p $data_pos -n $data_neg -b $obo_file -i $ic_file -e $emode $v -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_B,SIMF=Resnik -o $resdir/max_Resnik_score_B.elab",
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -p $data_pos -n $data_neg -b $obo_file -i $ic_file -e $emode $v -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_C,SIMF=Resnik -o $resdir/max_Resnik_score_C.elab",
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -p $data_pos -n $data_neg -b $obo_file -i $ic_file -e $emode $v -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_D,SIMF=Resnik -o $resdir/max_Resnik_score_D.elab",
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -p $data_pos -n $data_neg -b $obo_file -i $ic_file -e $emode $v -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_E,SIMF=Resnik -o $resdir/max_Resnik_score_E.elab",
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -p $data_pos -n $data_neg -b $obo_file -i $ic_file -e $emode $v -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_F,SIMF=Resnik -o $resdir/max_Resnik_score_F.elab",	

	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -p $data_pos -n $data_neg -b $obo_file -i $ic_file -e $emode $v -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_A,SIMF=Lin -o $resdir/max_Lin_score_A.elab",
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -p $data_pos -n $data_neg -b $obo_file -i $ic_file -e $emode $v -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_B,SIMF=Lin -o $resdir/max_Lin_score_B.elab",
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -p $data_pos -n $data_neg -b $obo_file -i $ic_file -e $emode $v -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_C,SIMF=Lin -o $resdir/max_Lin_score_C.elab",
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -p $data_pos -n $data_neg -b $obo_file -i $ic_file -e $emode $v -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_D,SIMF=Lin -o $resdir/max_Lin_score_D.elab",
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -p $data_pos -n $data_neg -b $obo_file -i $ic_file -e $emode $v -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_E,SIMF=Lin -o $resdir/max_Lin_score_E.elab",
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -p $data_pos -n $data_neg -b $obo_file -i $ic_file -e $emode $v -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_F,SIMF=Lin -o $resdir/max_Lin_score_F.elab",
		
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -p $data_pos -n $data_neg -b $obo_file -i $ic_file -e $emode $v -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_A,SIMF=pJacc -o $resdir/max_AJacc_score_A.elab",
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -p $data_pos -n $data_neg -b $obo_file -i $ic_file -e $emode $v -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_B,SIMF=pJacc -o $resdir/max_AJacc_score_B.elab",
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -p $data_pos -n $data_neg -b $obo_file -i $ic_file -e $emode $v -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_C,SIMF=pJacc -o $resdir/max_AJacc_score_C.elab",
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -p $data_pos -n $data_neg -b $obo_file -i $ic_file -e $emode $v -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_D,SIMF=pJacc -o $resdir/max_AJacc_score_D.elab",
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -p $data_pos -n $data_neg -b $obo_file -i $ic_file -e $emode $v -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_E,SIMF=pJacc -o $resdir/max_AJacc_score_E.elab",
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -p $data_pos -n $data_neg -b $obo_file -i $ic_file -e $emode $v -m LIST=go,TH=th,SUMF1=mean,SUMF2=max,SF=score_F,SIMF=pJacc -o $resdir/max_AJacc_score_F.elab"
);

my @data=(
	# Here we generate ADS datasets. These can take a lot of disk space
	"$elab -k $perm -r $rmode -s $srand -d $max_ajac -p $data_pos -n $data_neg -b $obo_file -i $ic_file -e $emode $v -m data -o $resdir/$seed_name.elab",

);
my @all= (@AUC,@jacc,@Smin,@sem);

# SELECTING THE PART OF THE PIPE TO RUM
my @tasks;
my @pipe_parts= split(/,/,$pipe);
foreach my $part(@pipe_parts){
	if($part eq 'auc'){push(@tasks,@AUC);}
	elsif($part eq 'jacc'){push(@tasks,@jacc);}
	elsif($part eq 'smin'){push(@tasks,@Smin);}
	elsif($part eq 'sem'){push(@tasks,@sem);}
	elsif($part eq 'all'){@tasks = @all;}
	elsif($part eq 'data'){@tasks = @data;}
	else{
		print STDERR "WARNING: $0: unrecognised pipeline part \"$part\"\n";
	}
}
# DEBUG
#foreach my $t(@tasks){ print "$t\n";}exit(1);




# FORKING
my $CHILD = 0;
my $pid;
my $th_count =0;
my @th_failed_list;
my @th_passed_list;
my %pid2th_hash;


print "PID($$): starting ",scalar(@tasks)," tasks\n";

for(my $th_i=0; $th_i<scalar(@tasks); $th_i++){

	my $pid = fork();
	if(not defined $pid){
		print "WARNING: skipping thread $th_i\n";
		push(@th_failed_list,$th_i);
		next;
	}
	$th_count++;
	$pid2th_hash{$pid}= $th_i;
	
	if($pid == $CHILD){
		# DEBUG:
		# print "\tPID($$): $tasks[$th_i]\n"; exit(0);
		
		if( system($tasks[$th_i])==0 ){ 
			exit(0);
		}
		else{
			exit(1);
		}
	}
}

# wait for children to finnish
for(1..$th_count){
	my $pid= wait();
	
	if( !defined($pid2th_hash{$pid}) ){
		next;
	}
	if($? != 0){
		push(@th_failed_list,$pid2th_hash{$pid});
	}
	else{
		push(@th_passed_list,$pid2th_hash{$pid});
	}
}

print "PID($$) SUMMARY:\n";
print "tasks:\t",scalar(@tasks),"\n";
print "passed:\t",scalar(@th_passed_list),"\n";
print "failed:\t",scalar(@th_failed_list),"\n";
if(scalar(@th_failed_list) == 0){
	print "ALL PASSED\n";
}
else{
	foreach my $thi(@th_failed_list){
  		print "$thi: $tasks[$thi]\n";
	}
}
#exit(0);
