#!/usr/bin/perl
use strict;
use warnings;
use Time::HiRes qw(gettimeofday);
#
# Searching for a high-signal Decoy Prediction Set (DPS) for a given (metric,dataset) pair
# 
# Our tactic is to use Rand GO DPS and to 
# 1. iterate number of classes predicted for each gene
#

# DEPENDENCIES:
my $goscores	= "./cpp/v2.6/goscores";
#perl get_naive_signal.pl;

my $usage= 	"\nUSAGE: get_randgo_signal_table.pl data go_counts signal|score elab_file1 [elab_file2..]\n".
		"data		: <gene\tgo> annotation\n".
		"go_counts	: <go\tcount\tfreq> sorted by the counts\n".
		"signal|score	: \"signal\" or \"score\" literal to indicate, which table to print\n".
		"elab_file1	: scores from an elab run\n".
		"               : several files can be given in a single run\n".
		"\n".
		"NOTE: data and elab_files should have matching annotation set\n\n";

if(scalar(@ARGV)<4) { die $usage; }

my $data		= shift(@ARGV);
my $go_counts		= shift(@ARGV);
my $signal_or_score	= shift(@ARGV);
my @elab_files		= @ARGV;


# GENERATE NAIVE DECOY DATASETS WITH DIFFERENT PARAMETERS AND SCORE THEM
my @k		= (10,20,30,40,50,100,200,400,800,1000);
my $mu		= 0.5;
my $std		= 0.5;
my $naive_file		= "naive_".(gettimeofday()*1000).".tmp";
my $signal_file		= "signal_".(gettimeofday()*1000).".tmp";

print "k\\elab_file\t";
for(my $j=0; $j<scalar(@elab_files); $j++){
	$elab_files[$j] =~ m/([^\/]+)$/;
	print "$1\t";
}
print "\n";

for(my $i=0; $i<scalar(@k); $i++){
	print "$k[$i]\t";
	
	# DPS SET
	my $call= "perl get_randgo_pred.pl $data $naive_file $go_counts $k[$i] $mu $std";
	(system($call)==0) or die "ERROR: failed to run:\n$call\n";	
	
	
	for(my $j=0; $j<scalar(@elab_files); $j++){
	
		# SCORE DPS SET
  		$call= "perl get_naive_signal.pl $goscores $naive_file $elab_files[$j] > $signal_file";
		(system($call)==0) or die "ERROR: failed to run:\n$call\n";
		
		open(IN,"<$signal_file") or die "Can\'t open $signal_file: $!\n";
  		my $l;
		while ($l = <IN>){
			chomp($l);
			my @sp= split(/=/,$l);
			#print "$sp[0]\n"; print "$sp[1]\n"; exit(1);

			if(($sp[0] eq "naive signal") && ($signal_or_score eq "signal")){
				print "$sp[1]\t";
				last;
			}
			if(($sp[0] eq "naive score") && ($signal_or_score eq "score")){
				print "$sp[1]\t";
				last;
			}
		}
		close(IN);
	}
	print "\n";
}
 

# CLEANUP
system("rm -f $naive_file");
system("rm -f $signal_file");

