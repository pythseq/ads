#!/usr/bin/perl
use strict;
use warnings;

# adds Norm dist random scores to data

my $usage= 	"USAGE: addscores.pl data_in data_out mu std rseed\n\n".
		"data_in \t<gene\\tgo> annotation\n".
		"data_out\t<gene\\tgo\\tscore> annotation\n".
		"mu\tpar of standard dist\n".
		"std\tpar of standard dist\n".
		"rseed\tseed for random number generator\n\n";

if( scalar(@ARGV)< 5 ) { die $usage; }

my $fin= shift(@ARGV);
my $fout= shift(@ARGV);
my $mu= shift(@ARGV);
my $std= shift(@ARGV);
my $rseed= shift(@ARGV);

print STDERR "reading $fin\n";
print STDERR "printing $fout\n";

open(IN,"<$fin") or die "Can\'t open $fin: $!\n";
open(OUT,">$fout") or die "Can\'t open $fout: $!\n";


srand($rseed);


while(my $l=<IN>){
	if ( $l =~ m/^[\!#]/){ # comment line
		print OUT $l;
		next;
	}
	
	chomp($l);
	
	# Norm dist rand scores
	my $r = rand_norm_val($mu,$std);
	
	print OUT "$l\t$r\n";
}		
close(IN);
close(OUT);


#################
# Sub-functions #
#################

sub rand_norm_val {

   # code copied from these places
   #
   # http://code.activestate.com/recipes/143083-normally-distributed-random-numbers/
   # http://users.wfu.edu/rollins/normal_random.html
   # http://mathworld.wolfram.com/Box-MullerTransformation.html

   my ($mu, $std) = @_;
   my $x = rand(1);
   my $pi = 3.14159;
   $x = sqrt( -2 * log(rand()) ) * cos( 2 * $pi * rand());
   $x = $std*$x + $mu;
   return $x;
}
