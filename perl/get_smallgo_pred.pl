#!/usr/bin/perl
use strict;
use warnings;

# Creates annotation set with a defined number of smallest GO classes assigned to
# all genes.
# Annotations are scored by their frequency relative to the largest GO-class
#

my $usage= 	"USAGE: get_smallgo_pred.pl data_in data_out go_counts th k [mu std rseed]\n\n".

		"data_in \t: <gene\\tgo> annotation (only gene col is used)\n".
		"data_out\t: <gene\\tgo\\tscore> annotation\n".
		"go_counts\t: <go\\tcount\\tfreq> sorted by the counts\n".
		"th\t : GOs with counts < th will be excluded from prediction set\n".
		"k\t : num of pred GO-terms\n".
		"[mu]\t : mu parameter for Normal score dist [optional]\n".
		"[std]\t : std parameter for Normal score dist [optional]\n".
		"[rseed]\t: rand seed for sampling Normal dist [optional]\n\n".
		
		"NOTE \t: by default frequencies k/k to 1/k  are used as scores\n".
		"     \t: alternatively scores are sampled from Normal(mu,std)\n\n";

if( scalar(@ARGV)< 5 ) { die $usage; }

my $fin		= shift(@ARGV);
my $fout	= shift(@ARGV);
my $fcounts	= shift(@ARGV);
my $th		= shift(@ARGV);
my $k		= shift(@ARGV);
my $mu		= 0;
my $std		= 0;
my $rseed	= 1;
my $use_norm_scores	= 0;
if( scalar(@ARGV)>=3){
$use_norm_scores	= 1;
$mu		= shift(@ARGV);
$std		= shift(@ARGV);
$rseed		= shift(@ARGV);
srand($rseed);
}


# debug
my $DEBUG = 0;
if($DEBUG){
print STDERR "fin\t: $fin\n";
print STDERR "fout\t: $fout\n";
print STDERR "go_counts\t: $fcounts\n";
print STDERR "th\t: $th\n", 
print STDERR "k\t: $k\n";}


# READING GO IDS FOR SMALLES CLASSES
my %gofreq_hash;
my $counter=0;

open(IN,"<$fcounts") or die "Can\'t open $fcounts: $!\n";
while(my $l=<IN>){
	if ( $l =~ m/^[\!#]/){ # comment line
		next;
	}
	chomp($l);
	my @sp = split(/\t/,$l);
	
	# THRESHOLD counts < th
	if($sp[1] < $th){
		next;}
		
	$gofreq_hash{$sp[0]}= $sp[2];
}		
close(IN);


# READING GENES FROM <GENE,GO> ANNOTATION
my %gene_hash;
open(IN,"<$fin") or die "Can\'t open $fin: $!\n";
while(my $l=<IN>){
	if ( $l =~ m/^[\!#]/){ # comment line
		next;
	}
	chomp($l);
	my @fields = split(/\t/,$l);
	$gene_hash{$fields[0]}= 1;
}		
close(IN);

# PRINTING SMALLGO PREDICTIONS
open(OUT,">$fout") or die "Can\'t open $fout: $!\n";
my @gene_list	= sort keys(%gene_hash);
my @go_list	= sort {$gofreq_hash{$a}<=>$gofreq_hash{$b}} keys %gofreq_hash;
# DEBUG
#for(my $i=0; $i<10; $i++){ print "$go_list[$i]\t:$gofreq_hash{$go_list[$i]}\n";} exit(1);

foreach my $gene (@gene_list){
	for(my $i=0; $i<$k;$i++){
		if($use_norm_scores){
			my $norm_score = rand_norm_val($mu,$std);
			print OUT "$gene\t$go_list[$i]\t$norm_score\n";
		}
		else{
			#print OUT "$gene\t$go_list[$i]\t$gofreq_hash{$go_list[$i]}\n";
			print OUT "$gene\t$go_list[$i]\t",(($k-$i)/$k),"\n";
		}
	}
}
close(OUT);
	
	
#################
# Sub-functions #
#################

sub rand_norm_val {

   # code copied from these places
   #
   # http://code.activestate.com/recipes/143083-normally-distributed-random-numbers/
   # http://users.wfu.edu/rollins/normal_random.html
   # http://mathworld.wolfram.com/Box-MullerTransformation.html

   my ($mu, $std) = @_;
   my $x = rand(1);
   my $pi = 3.14159;
   $x = sqrt( -2 * log(rand()) ) * cos( 2 * $pi * rand());
   $x = $std*$x + $mu;
   return $x;
}
