#! /usr/bin/perl
use strict;
use warnings;

#
# SCORES False Positive Sets with variants of AUC metrics and prints scores to file:
# rows	are metrics
# cols  are FP sets
#
my $usage= 	"USAGE: perl $0 data_pos obo_file ic_file FPS1 FPS2 FPS3 [FPS4..]\n\n".
		"data_pos     : positive GO annotation [gene\\tGO\\tscore]\n".
		"obo_file     : OBO 1.2 file\n".
		"ic_file      : information content file [GO\\tIC]\n".
		"FPS1         : i.e. Naive FP data set\n".
		"FPS2         : i.e. smallgo data set\n".
		"FPS3         : i.e. randgo data set\n".
		
		"DEPENDENCIES : elab executable\n\n";

# DEPENDENCIES
my $goscores = "bin/goscores";


if( scalar(@ARGV)< 6 ) { die $usage; }

my $data_pos	= shift(@ARGV);
my $obo_file	= shift(@ARGV);
my $ic_file	= shift(@ARGV);
my @fps_list 	= @ARGV;

# AUC METRICS TO SCORE	
my @metric_list= ("US AUC1","US AUC2","US AUC3a","US AUC3b","US AUCPR",
		"GC AUC1","GC AUC2","GC AUC3a","GC AUC3b","GC AUCPR",
		"TC AUC1","TC AUC2","TC AUC3a","TC AUCPR");
my @method_list= (
	# AUC METRICS. UNSTRUCTURED DATA. ALL DATA POOLED
	"LIST=gene_go,SF=AUC1",
	"LIST=gene_go,SF=AUC2",	
	"LIST=gene_go,SF=AUC3a",
	"LIST=gene_go,SF=AUC3b",
	"LIST=gene_go,SF=AUCPR",

	# AUC METRICS. GENE CENTRIC RESULTS. DATA GROUPED AROUND EACH GENE
	"LIST=go,TH=all,SUMF1=mean,SF=AUC1 ",
	"LIST=go,TH=all,SUMF1=mean,SF=AUC2 ",	
	"LIST=go,TH=all,SUMF1=mean,SF=AUC3a ",
	"LIST=go,TH=all,SUMF1=mean,SF=AUC3b ",
	"LIST=go,TH=all,SUMF1=mean,SF=AUCPR ",

	# AUC METRICS. GO CENTRIC RESULTS. DATA GROUPED AROUND GO TERMS
	"LIST=gene,TH=all,SUMF1=mean,SF=AUC1 ",
	"LIST=gene,TH=all,SUMF1=mean,SF=AUC2 ",	
	"LIST=gene,TH=all,SUMF1=mean,SF=AUC3a ",
	"LIST=gene,TH=all,SUMF1=mean,SF=AUCPR ",
);
	

# Create the header for the output
print "metric";
foreach my $fps(@fps_list){
	my $label = $fps;
	$label =~ s/.+\///g;
	print "\t$label";
}print "\n";


my $tmp_res= "";
for(my $i=0; $i<scalar(@method_list); $i++){
	
	print "$metric_list[$i]";
	
	for(my $j=0; $j<scalar(@fps_list); $j++){
		
		my $task= "$goscores -p $fps_list[$j] -t $data_pos -b $obo_file -i $ic_file -g -m $method_list[$i] ";
		#print "$task\n";
		$tmp_res = `$task`;
		chomp($tmp_res);
		print "\t$tmp_res";
	}
	print "\n";
}
	
