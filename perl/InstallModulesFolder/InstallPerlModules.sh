 #############################################
 # Installing required perl modules in unix
 # Currently just adds Set-Scalar
 #
 # I planned List::Util and List:MoreUtils
 # for all (and any) style testing
 # Did not get those to work. Skip them now
 ##############################################
 
 # get the files
 wget http://search.cpan.org/CPAN/authors/id/D/DA/DAVIDO/Set-Scalar-1.29.tar.gz .
 #wget http://search.cpan.org/CPAN/authors/id/P/PE/PEVANS/Scalar-List-Utils-1.48.tar.gz .
 #wget http://search.cpan.org/CPAN/authors/id/R/RE/REHSACK/List-MoreUtils-0.419.tar.gz .
 
 #unpack
 tar -zxvf Set-Scalar-1.29.tar.gz
 #tar -zxvf Scalar-List-Utils-1.48.tar.gz
 #tar -zxvf List-MoreUtils-0.419.tar.gz		# This should be better than utils. It has dependencies
 						# Don't want spend time solving them now
						# Currently just using grep instead of moreutils
 
 # run install commands for folders
 cd Set-Scalar-1.29
 perl Makefile.PL PREFIX=../../PerlModules/ LIB=../../PerlModules/
 make
 make test
 make install
 cd ..
 
 
 ########################################
 # Document out the remaining install commands
 ########################################
 #
 #cd Scalar-List-Utils-1.48
 #perl Makefile.PL PREFIX=../../PerlModules/ LIB=../../PerlModules/
 #make
 #make test
 #make install
 #cd ..
 #
 #cd List-MoreUtils-0.419
 #perl Makefile.PL PREFIX=../../PerlModules/ LIB=../../PerlModules/
 #make
 #make test
 #make install
 #cd ..

