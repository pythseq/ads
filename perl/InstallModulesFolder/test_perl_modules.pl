use strict;
use warnings;

BEGIN{
   use Cwd;
   my $pwd = cwd();
   
   # PT comment: Did not get use lib to work
   # However, unshift @INC seems to work ... ?
   
   unshift @INC, "$pwd/PerlModules/";
   unshift @INC, "$pwd/perl/PerlModules/";
   unshift @INC, "$pwd";
   unshift @INC, "$pwd/../PerlModules/";

   $pwd =~ s/(\/[^\/]+\/$)|(\/[^\/]+$)//;  #Remove folder from the folder path
   unshift @INC, "$pwd/PerlModules/";
}
use Set::Scalar;
my $s = Set::Scalar->new;
$s->insert('a', 'b');

print "All went OK with set::scalar\n";

#use List::MoreUtils "all" ;
#use List::Util "all" ;

print "No check for List:Util or List:MoreUtils\n";

exit ;
my @test = ( "aa", "bb", "cc", "dd");

if( all {$_ eq "cc"} @test){
   print "match found in test\n";
}
else{
   print "match NOT found in test\n";
}

print "All went OK\n";
