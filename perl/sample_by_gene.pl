#!/usr/bin/perl
use strict;
use warnings;

# Samples randomly a given number of genes (without replacement) from <gene,go> annotation file
# Writes <gene,go> annotations for the sampled genes to another file

my $usage= 	"USAGE: $0 data.in data.out N rseed\n".
		"data.in \tinput annotation file, <gene,..> \n".
		"data.out\toutput annotation file, <gene,..> \n".
		"N       \tnum of genes to sample\n".
		"rseed   \tseed for the random number generator\n\n";

if( scalar(@ARGV)<4) { die $usage; }

my $fin		= shift(@ARGV);
my $fout	= shift(@ARGV);
my $N		= shift(@ARGV);
my $rseed	= shift(@ARGV);

open(IN,"<$fin") or die "Can\'t open $fin: $!\n";
open(OUT,">$fout") or die "Can\'t open $fout: $!\n";

srand($rseed);


# parcing <gene,go> data
print "reading $fin\n";
my ($gene,$gene_prev);
my @goa_list;
my %gene2goa_hash;

while(my $l=<IN>){
	if ( $l =~ m/^[\!#]/){ # comment line
		next;
	}
	chomp($l);
	($gene_prev)= split(/\t/,$l);
	push(@goa_list,$l);
	last;
}
while(my $l=<IN>){
	if ( $l =~ m/^[\!#]/){ # comment line
		next;
	}
	chomp($l);
	($gene)= split(/\t/,$l);
	if($gene ne $gene_prev){
		my @temp= @goa_list;
		$gene2goa_hash{$gene}= \@temp;
		@goa_list=();
		$gene_prev= $gene;
	}
	push(@goa_list,$l);
}
if($#goa_list+1 > 0){
	my @temp= @goa_list;
	$gene2goa_hash{$gene}= \@temp;
}


# sampling
print "sampling $N genes to file $fout\n";

my @genes= sort {$a cmp $b} (keys(%gene2goa_hash));
my @sample;
my %insample;
my $data_size= scalar(@genes);
if($N >= $data_size){ die "Not enough genes in annotation file for $N sample\n";} 
my $i=0;
while($i<$N){
	my $gi= int(rand($data_size));
	my $gene= $genes[$gi];
	if(!defined($insample{$gene})){
		push(@sample,$gene);
		$insample{$gene}= 1;
		$i++;
	}
}
@sample= sort @sample;

foreach $gene(@sample){
	@goa_list= @{$gene2goa_hash{$gene}};
	foreach my $goa (@goa_list){
		print OUT "$goa\n";
	}
}
	
close(IN);
close(OUT);
