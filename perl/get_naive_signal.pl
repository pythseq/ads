#!/usr/bin/perl
use strict;
use warnings;

#
# Estimating signal level for naive classifier against dilution series file (ELAB file)
#

my $usage= 	"\nUSAGE: get_naive_signal.pl goscores naive_file elab_file1 [elab_file2..]\n".
		"goscores    \t: path to goscores utility\n".
		"naive_file  \t: predictions of a naive classifier\n".
		"elab_file   \t: scores from an elab run\n".
		"            \t: when several files are analysed, prints a table\n\n";

if(scalar(@ARGV)<3) { die $usage; }

my $goscores=     shift(@ARGV);
my $naive_file=   shift(@ARGV);
my @elab_files=    @ARGV;
my $tmp_file=   "out.tmp";


if($#elab_files > 0){
  print "\tnaive_score\tnaive_signal\n";
}

foreach my $elab_file(@elab_files){
  my $pos_file;
  my $obo_file;
  my $ic_file = "";
  my $mode;
  my $scoref;
  my $prop_par= "";
  my @error_levels;
  my @score_mat;
  my $ncol;
  my $nrow;
  my $naive_score;
  my $naive_error; 

  open(IN,"<$elab_file") or die "Can\'t open $elab_file: $!\n";
  my $linen=0;
  while(my $l=<IN>){
	$linen++;
	chomp($l);
	if ( $l =~ m/^[\!#]/){
		$l =~ s/^[\!#]//;
		
		if($l =~ m/goa_pos\:\s*([\w\/\.]+)/){
			$pos_file= $1;
		}
		elsif($l =~ m/obo_file\:\s*([\w\/\.]+)/){
			$obo_file= $1;
		}
		elsif($l =~ m/ic_file\:\s*([\w\/\.]+)/){
			$ic_file= "-i $1"; # this will be "-i ic_file" or "" empty
		}
		elsif($l =~ m/mode\:\s*([\w=,]+)/){
			$mode= $1;
		}
		elsif($l =~ m/prop_par\:\s*[Tt]/){
			$prop_par= "-g";
		}
	}
	else{
		# ERROR LEVELS
		@error_levels= split(/\t/,$l);
		$ncol = $#error_levels +1;
		if( $ncol<2){
			print STDERR "ERROR: missing values at line: $elab_file:$linen\n";
			exit(1);
		}
		
		# SCORE MATRIX
		for(my $i=0; $i<$ncol; $i++){
			my @col;
			push(@score_mat,\@col);
		}
		while(my $l=<IN>){
			$linen++;
			chomp($l);
			my @row= split(/\t/,$l);
			if($#row+1 < $ncol){
				print STDERR "ERROR: missing values at line: $elab_file:$linen\n";
				exit(1);
			}
			for(my $i=0; $i<=$ncol; $i++){
				push(@{$score_mat[$i]},$row[$i]);
			}
		}
	}
  }
  $nrow= $#{$score_mat[0]} + 1;
  close(IN);
  
  # SCORING FUNCTION
  my @temp= split(/,/,$mode);
  for(my $i=0; $i<=$#temp; $i++){
  	my @pair= split(/=/,$temp[$i]);
  	if($pair[0] eq "SF"){
		$scoref= $pair[1];
		last;
	}
  }

  # MEAN SCORE FOR EACH ERROR LEVEL
  my @score_mean;
  for(my $i=0; $i<$ncol; $i++){
	push(@score_mean,get_mean($score_mat[$i]));
  }

  # SCORE NAIVE
  my $call = "$goscores -p $naive_file -t $pos_file -b $obo_file $ic_file $prop_par -m $mode 1> $tmp_file";
  #print STDERR "$call\n";

  if(system($call) != 0){
	print STDERR "ERROR: failed to execute: $call\n";
	exit(1);
  }
  open(IN,"<$tmp_file") or die "Can\'t open $tmp_file: $!\n";
  my $l=<IN>;
  chomp($l);
  $naive_score= $l;
  close(IN);
  #print "naive score= $naive_score\n";


  # COMPARE NAIVE SCORE AGAINST DILUTION SERIES
  if($scoref eq "Smin"){			# THESE SCORE ARE MINIMIZED: WE LOOK FOR THE FIRST SCORE THAT IS LARGER THAN NAIVE SCORE
    for(my $i=0; $i<$ncol; $i++){
	if($score_mean[$i]>$naive_score){
		if($i == 0){
			$naive_error = $error_levels[0];
		}
		else{
			$naive_error = ($error_levels[$i]+$error_levels[$i-1])/2.0;
		}
		last;
	}
	elsif($i == $ncol-1){
		$naive_error = $error_levels[$ncol-1];
	}
    }
  }
  else{					# THESE SCORES ARE MAXiMIZED
    for(my $i=0; $i<$ncol; $i++){		
	if($score_mean[$i]<$naive_score){
		if($i == 0){
			$naive_error = $error_levels[0];
		}
		else{
			$naive_error = ($error_levels[$i]+$error_levels[$i-1])/2.0;
		}
		last;
	}
	elsif($i == $ncol-1){
		$naive_error = $error_levels[$ncol-1];
	}
    }
  }
  
  if($#elab_files == 0){
  	print "naive score=$naive_score\n";
	print "naive signal=",(1-$naive_error),"\n";
  }
  else{
  	$elab_file =~ m/\/([^\/]+)$/;
	my $elab_label= $1;
	$elab_label =~ s/_/ /g;
  	print "$elab_label\t$naive_score\t",(1-$naive_error),"\n";
  }


# DEBUG
#print "elab:  $elab_file\n";
#print "naive: $naive_file\n";
#print "pos:   $pos_file\n";
#print "obo:   $obo_file\n";
#print "IC:    $ic_file\n";
#print "mode:  $mode\n";
#print "error levels: [";
#foreach my $e(@error_levels){ print "\t$e";}print "]\n";

#print "score mat: $nrow x $ncol\n";
#print "score mean:[";
#foreach my $s(@score_mean){ print "\t$s";}print "]\n";
}





sub get_mean{
	my @v= @{shift(@_)};
	my $sum= 0;
	foreach my $x(@v){
		$sum+= $x;
	}
	return ($sum/($#v+1));
}
