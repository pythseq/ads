	DEMORUNS FOR PIPELINE.PL
Following text represents various demo runs for pipeline.pl. Lighter runs, shown here, can be used to test that everything is installed correctly and to get familiar with parameters. More complex and heavier runs can be used as starting point for your own research. Finally, the last command repeats the manuscript analysis and creates summary tables and figures. Notice that the lower runs are more time consuming.

1. Simple run that checks preliminary processing steps before elab runs, excluding IC2 calculation
	perl pipeline.pl --resdir test --data data/uniprot.1000.genego --pipev 1,2,4
Notes: 
	This does not perform any elab runs. Only steps 1,2 and 4 are being performed.
	Resulting files are actually directed to data directory. We consider these files to be input data files.
	So nothing goes to defined result directory, yet.
	Input will be our uniprot dataset, located to data/uniprot.1000.genego file.
1.B. Simple run that checks preliminary processing steps before elab runs, with your own data
	perl pipeline.pl --datadir MyData/  --data path/to/data/xxx --obo path/to/data/xx.obo --goa path/to/data/xx.goa --pipev 1,2,3,4
Notes: 
	Here the processed dataset will be your own data (path/to/data/xxx)
		This will be the set of correct annotations, used as a starting point for elab runs.
	OBO structure will be taken from file path/to/data/xx.obo 
	GO annotations will be collected from file path/to/data/xx.goa
		This is used to calculate the GO class sizes and IC scores for GO classes
	This command does not perform any elab runs. Only steps 1,2, 3 and 4 are being performed
	Step 3 will be very time consuming.
	Resulting files are directed to specified data directory. We set it to be MyData here.
		This way we are not overwriting files in the data dir
2. Simple run that performs just a small elab run, after test 1
	perl pipeline.pl --resdir test --data data/uniprot.1000.genego --perms 50 --pipev 5 --pipeh Smin
Notes:
	IMPORTANT: This will work only if the run 1 was done
	Results are directed to test directory
	Input files will be ones derived from uniprot dataset (data/uniprot.1000.genego file)
	Only the elab step will be performed (--pipev 5)
	Number of generated datasets at each signal level (--perms) is set to 50. Use this only for testing. For real science use values 200 - 1000 
	This run only does tests with Smin related Evaluation Metrics (--pipeh Smin)
		Therefore this only creates two parallel runs at parallelized step. 

3. Simple run that checks preliminary steps (excluding IC2 calculation) and performs small elab run
	perl pipeline.pl --resdir test --data data/cafa2012.genego --perms 40 --pipev 1,2,4,5 --pipeh Jacc
Notes:
	Results are again directed to test directory
	Input files will be ones derived from CAFA dataset (data/cafa2012.genego file)
	Preliminary steps (1,2,4) and elab step (5) will be performed.
	Number of generated datasets at each signal level (--perms) is set to 40. Use this only for testing. For real science use values 200 - 1000 
	This run only does tests with Jaccard related Evaluation Metrics (--pipeh Jacc)
		Therefore this also creates only two parallel runs at parallelized step. 
4. Simple run that performs preliminary steps (excluding IC2 calculation), does all analysis steps and creates result tables and figures
	perl pipeline.pl --resdir test --data data/cafa2012.genego --perms 100 --pipev 1,2,4,5,6,7,9 --pipeh AUC
Notes:
	This is the first example of the whole analysis task, with preliminary steps (1,2,4), analysis steps (5,6,7) and summaries (7, 9 and 10)
	Input files will be ones derived from CAFA dataset (data/cafa2012.genego file) in steps 1,2 and 4
	You could skip steps 1,2 and 4, if you have performed them already in previous runs for processed dataset.
	Again we skip the calculus of IC2 (step 3), as it is significantly more time consuming in our pipe.
	Analysis steps include:
		Elab runs (step 5)
		Generation False Positive Sets (step 6)
		Scoring False Positive Sets (step 7)
	Summaries of the results are generated with steps 7 and 9.
		We skip here step 10, as it requires results from all the compared evaluation metrics.
		Here we limited the analysis only to AUC evaluation metrics.
	Results are again directed to test directory. This will overwrite previous result tables, if those exist
	Number of generated datasets at each signal level (--perms) is set to 100. 
	This run only does tests Area Under Curve related Evaluation Metrics (--pipeh AUC)
		This generates 7 parallel runs
5. Repeat manuscript work, but with a significantly smaller number of repeats (=100)
	perl pipeline.pl --resdir test_p_100 --perms 100 
Notes:
	Here results are directed to folder test_p_100 
	In principle steps 1,2 and 4 could be omitted, if they were already done with all datasets
	Perms parameter again sets the number of repeats (here 100)
	This code processes all three datasets (discussed in manuscript)
	Now parallel part will generate 30 parallel runs
6. Repeat manuscript work with default number of repeats (1000 repeats !)
	perl pipeline.pl --resdir repeat_manuscript
Notes
	Here we direct the results to folder repeat_manuscript
	This code processes all three datasets.
	This run will generate 30 parallel runs. Each run will process 11*1000 datasets.
	Therefore, make sure you have enough computing resources for this run. 