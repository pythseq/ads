#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>
#include <queue>
#include <unordered_map>
#include <cstring>
#include <algorithm>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

// OTHER FILES
#include <golib.h>
#include <gograph.h>

using namespace std;


struct parameters{
	unsigned int K;
	unsigned int R;
	unsigned int seed;
	double max_ajac_sim;	
	double e_min;
	double e_max;
	double e_by;
	bool avoid_term_collisions;
	bool propagate_parents;
	bool add_goa_negatives;
	bool rotate_pos_ids;
	bool verbal;
	char* prog_name;
	char* goa_pos_file;
	char* goa_neg_file;
	char* obo_file;
	char* ic_file;
	char* stats_file;
	char* jacc_file;
	string emode;
	string mode;
};
struct score_parameters{	
	string list;	// type of lists that are compared: GO-lists or gene-lists
	string thf; 	// thresholding function
	string sumf1;	// summary function to summarise score accross genes/GOs
	string sumf2;	// summary function to summarise score accross thresholds
	string scoref;	// similarity score between two lists
	string simf;	// semantic similarity
	unsigned int minClassSize;	// Minimun size for GO class in Positive Set.
	unsigned int goclass_count;	// Number of uniq GO classes (in database or ontology, set by upper stream calls).
	
	score_parameters(string _list="NULL",
			string _thf="NULL",
			string _sumf1="NULL",
			string _sumf2="NULL",
			string _scoref="NULL",
			string _simf="NULL",
			unsigned int _classSize=10,
			unsigned int _goclass_count=0):
	list(_list),thf(_thf),sumf1(_sumf1),sumf2(_sumf2),scoref(_scoref),simf(_simf),minClassSize(_classSize),goclass_count(_goclass_count){};
};
void print_parameters(parameters pars){
	cout 
	<< "\tK       : "<<pars.K<<"\n"
	<< "\tR       : "<<pars.R<<"\n"
	<< "\tseed    : "<<pars.seed<<"\n"
	<< "\televels : "<<pars.e_min<<":"<<pars.e_by<<":"<<pars.e_max<<"\n"
	<< "\tavoid_term_collisions  : "<<(pars.avoid_term_collisions?'T':'F')<<"\n"
	<< "\tmax_ajac_sim           : "<<pars.max_ajac_sim<<"\n"
	<< "\tpropagate_parents      : "<<(pars.propagate_parents?'T':'F')<<"\n"
	<< "\tadd_goa_negatives      : "<<(pars.add_goa_negatives?'T':'F')<<"\n"
	<< "\trotate_pos_ids         : "<<(pars.rotate_pos_ids?'T':'F')<<"\n"
	<< "\tgoa_pos  : "<<pars.goa_pos_file<<"\n"
	<< "\tgoa_neg  : "<<((pars.goa_neg_file==NULL)?"NULL":pars.goa_neg_file)<<"\n"
	<< "\tobo_file : "<<((pars.obo_file==NULL)?"NULL":pars.obo_file)<<"\n"
	<< "\tic_file  : "<<((pars.ic_file==NULL)?"NULL":pars.ic_file)<<"\n"
	<< "\temode    : "<<pars.emode<<"\n"
	<< "\tmode     : "<<pars.mode<<"\n";
}
void print_score_parameters(score_parameters score_pars){
	cout 	<< "\tLIST\t: "<< score_pars.list <<"\n"
		<< "\tTHF \t: "<< score_pars.thf <<"\n"
		<< "\tSUMF1\t: "<< score_pars.sumf1 <<"\n"
		<< "\tSUMF2\t: "<<score_pars.sumf2<<"\n"
		<< "\tSF\t: "<<score_pars.scoref<<"\n"
		<< "\tSIMF\t: "<<score_pars.simf<<"\n"
		<< "\tMINCS\t: "<<score_pars.minClassSize<<"\n"
		<< "\tGOC\t: "<<score_pars.goclass_count<<"\n";
}


/*
 * Returns a random permutation of data in index representation.
 * The data itself is not permuted.
 *
 * data			vector of go_annotations (not permuted)
 * ind0			indices of left member of a swap pair
 * ind1			indices of the right members of a swap pair
 * pmap			map of node ancestors
 * max_ajacc_sim	max jaccard similarity between ancestors of swapped nodes
 *
 * returns	number of term collisions
 * 
 */
int get_permutation(	vector<go_annotation> &data, 
			vector<unsigned int> &ind0, 
			vector<unsigned int> &ind1,
			unordered_map<unsigned int,vector<unsigned int> > &pmap,
			double max_ajacc_sim){
			
	unsigned int I		= data.size();
	unsigned int I_half	= floor(I/2.0);
	vector<int> ind(I,0);
	for(unsigned int i=0; i<I; i++)
		ind[i]= i;
	vector<int> perm	= permute(ind); // random sample of the same size as indices
	ind0.clear();
	ind1.clear();
	ind0.resize(I_half);
	ind1.resize(I_half);;
	for(unsigned int i=0; i<I_half; i++){
		ind0[i]= perm[i];
		ind1[i]= perm[i+I_half];
	}
		
		
	int collisions= 0;
	
	// if the row in the left set (ind0) has the same GO annotation as the paired row in the right set (ind1)
	// we walk the remaining part of the right set swapping
	// until we find a row that has a different annotation
	unsigned int i,j,temp;
	for(i=0; i<I_half; i++){
		j = i;
		while( (data[ind0[i]].go == data[ind1[i]].go)  || 
			(get_Jaccard(pmap[data[ind0[i]].go], pmap[data[ind1[i]].go]) > max_ajacc_sim) ){
			j++;
			if(j>=I_half){ // could not avoid collision
				collisions++;
				break;
			}
			temp= ind1[i];
			ind1[i]= ind1[j];
			ind1[j]= temp;
		}
	}
	return collisions;
}

/*
 * Samples error rates.
 * Estimates the number of swaps to be performed for data of size data_size.
 * corrects error rates to match integer swap counts.
 *
 * in:
 * pars		prog parameters
 * data_size	size of the data
 * out:		a pair of vectors: <errors,swaps>
 */
vector<vector<double> > get_errors_and_swaps(const parameters pars,unsigned int data_size){
	// errors rates
	vector<double> errors;
	
	// linear sampling
	double e= pars.e_min;
	while(e<=pars.e_max){
		errors.push_back(e);
		e+= pars.e_by;
	}
	// TODO: exp2 sampling
	
	
	vector<double> swaps(errors.size(),0);
	for(unsigned int i=0; i<errors.size();i++){
		swaps[i]= ceil(errors[i]*double(data_size)*0.5);
		if(swaps[i]*2 > data_size)
			swaps[i]= floor(double(data_size)/2.0);
	}
	
		
	// round errors to match swaps
	for(unsigned int i=0; i<errors.size(); i++){
		errors[i]= swaps[i]*2/double(data_size);
	}
	
	/* DEBUG
	cout << "errors\tswaps\n";
	for(unsigned int i=0; i<errors.size(); i++){
		cout <<errors[i]<<"\t"<<swaps[i]<<"\n";
	}
	exit(0);*/
	
	vector<vector<double> > temp(2);
	temp[0]= errors;
	temp[1]= swaps;
	return temp;
}


/*
 * Print function by PT. This is used repetitively, when creating many datasets in one run
 * Ilja's code simply copied and wrapped inside the function.  
 */

void print_data_table(const parameters pars,
			const score_parameters score_pars, 
			const vector<vector<double> > &score_matrix,
			char* file_name,
			const vector<double> &error){

        // I used the same variable names as in the generate_scores code
	// This saved me from rewriting the variable names

	if(pars.verbal){
		cout << "# print score to "<<file_name<<"\n";
	}
	FILE * out = fopen (file_name,"w");
	fprintf( out,"# goa_pos:  %s\n",pars.goa_pos_file);
	fprintf( out,"# goa_neg:  %s\n",pars.goa_neg_file);
	fprintf( out,"# obo_file: %s\n",pars.obo_file);
	fprintf( out,"# ic_file:  %s\n",pars.ic_file);
	fprintf( out,"# prop_par: %c\n",(pars.propagate_parents?'T':'F'));
	fprintf( out,"# rot_pos:  %c\n",(pars.rotate_pos_ids?'T':'F'));
	fprintf( out,"# R:        %i\n",pars.R);
	fprintf( out,"# max_ajac_sim: %.3f\n",pars.max_ajac_sim);
	if(pars.seed != 0) fprintf( out,"# rseed:    %i\n",pars.seed);
	fprintf( out,"# mode:     LIST=%s,TH=%s,SUMF1=%s,SUMF2=%s,SF=%s,SIMF=%s,MINCS=%i,GOC=%i\n",
					score_pars.list.c_str(),
					score_pars.thf.c_str(),
					score_pars.sumf1.c_str(),
					score_pars.sumf2.c_str(),
					score_pars.scoref.c_str(),
					score_pars.simf.c_str(),
					score_pars.minClassSize,
					score_pars.goclass_count);
	fprintf(out,"%1.6f",error[0]);
	for(unsigned int ci=1; ci<error.size(); ci++)
		fprintf(out,"\t%1.6f",error[ci]);
	fprintf(out,"\n");
	
	for(unsigned int pi=0; pi<score_matrix.size(); pi++){
		fprintf(out,"%1.6f",score_matrix[pi][0]);
		for(unsigned int ci=1; ci<score_matrix[pi].size(); ci++)	
			fprintf(out,"\t%1.6f",score_matrix[pi][ci]);
		fprintf(out,"\n");
        }
	fclose(out);
}

void print_annotation(const vector<go_annotation> &data, char* file_name){
	FILE* OUT = fopen (file_name,"w");
	fprintf(OUT,"#gene\tgo\tscore\n"); // headers
	for(unsigned int i=0; i<data.size(); i++){
		fprintf(OUT,"%s\t%07u\t%f\n",
		data[i].gene.c_str(),
		data[i].go,
		data[i].score);
	}
	fclose(OUT);
}


/*
 * For each annotation line in data_pred print Jaccard index between predicted GO and the closest correct GO for the same gene.
 *
 * data_pred		list of predicted gene-go annotations
 * data_pos		list of correct gene-go annotations
 * error_level		assumed error level for the data_pred
 * pi			permutation index for data_pred
 * OUT			will print to this stream
 *
 * print to OUT in format: <ci\tpi\tGO_pred\terror_level\tJacc\n>
 * ci		index of error level
 * pi		index of permutation
 * GO_pred	predicted GO
 * error_level	error level corresponding to ci
 * Jacc		max_i Jaccard( parents(GO_pred),parents(GO_pos_i))
 */
void print_jaccard(const vector<go_annotation> &data_pred_,
			const vector<go_annotation> &data_pos_,
			unordered_map<unsigned int,vector<unsigned int> > &pmap,
			unsigned int ci,		
			unsigned int pi,
			double error_level,
			FILE* OUT){
	vector<go_annotation> data_pred = copy(data_pred_);
	vector<go_annotation> data_pos  = copy(data_pos_);
	
	sort(data_pred.begin(),data_pred.end(),compare_gene_score);
	sort(data_pos.begin(),data_pos.end(),compare_gene_score);
	
	// group by gene
	vector<vector<go_annotation> > data_pred_bygene_all = group_go_annotation_data(data_pred,"gene");
	vector<vector<go_annotation> > data_pos_bygene_all  = group_go_annotation_data(data_pos,"gene");
	
	// filter pred and pos data with matching gene-labels
	vector<vector<go_annotation> > data_pred_bygene;
	vector<vector<go_annotation> > data_pos_bygene;
	match_gene_labels(data_pred_bygene_all,data_pos_bygene_all, data_pred_bygene,data_pos_bygene,true);
	
	// print max Jaccard index for each predicted GO
	for(unsigned int gi=0; gi<data_pred_bygene.size(); gi++){
		
		vector<double> tmp(data_pos_bygene[gi].size(),0);
		double max_jacc = 0;
		
		for(unsigned int k=0; k<data_pred_bygene[gi].size(); k++){
			
			for(unsigned int l=0; l<data_pos_bygene[gi].size(); l++)
				tmp[l]= get_Jaccard( pmap[data_pred_bygene[gi][k].go], pmap[data_pos_bygene[gi][l].go] );
			max_jacc = max(tmp);
			
			fprintf(OUT,"%u\t%u\t%07u\t%1.6f\t%1.6f\n",ci,pi,data_pred_bygene[gi][k].go,error_level,max_jacc);
		}
	}
}

/*
 * Function that creates the score matrix. 
 * This is where we store evaluation metric results for a set of metrics.
 * This is 3-D matrix
 */

vector<vector<vector<double> > > create_3D_score_matrix(unsigned int ii, unsigned int jj, unsigned int kk){

	vector<vector<vector<double> > > matrix_out(ii);
	for(unsigned int si=0; si<ii; si++){
		vector<vector<double> > score_matrix(jj);
		for(unsigned int pi=0; pi<jj; pi++){
			vector<double> row(kk,0);
			score_matrix[pi]= row;
		}
		matrix_out[si]= score_matrix;
	}
	return matrix_out;
}
/*
 * Creates 2D score matrix.
 * This is where we store evaluation metric results for a single metric.
 */
vector<vector<double> > create_score_matrix(unsigned int nrow,unsigned int ncol){
	vector<vector<double> > score_matrix(nrow);
	for(unsigned int ri=0; ri<nrow; ri++){
		vector<double> row(ncol,0);
		score_matrix[ri]= row;
	}
	return score_matrix;
}

/*
 * Generates scores or permuted data for a set of parameters.
 * 
 * pars			Parameters used to generate permuted data.
 *
 * score_pars		Parameters used to score permuted data agains positive data.
 *			This list can contain more than one set of different scoring options
 *			that will all be applied to each permutation.  
 *			
 */

void generate_scores(const parameters pars, score_parameters score_pars){

	// READING DATA
	if(pars.verbal){
		cout << "# reading data\n" <<flush;}
	vector<go_annotation> data_pos= read_go_annotation_data(pars.goa_pos_file);
	vector<go_annotation> data_neg;
	if(pars.add_goa_negatives){
		data_neg= read_go_annotation_data(pars.goa_neg_file);
	}
	
	// SANITY CHECK
	double min_score	= get_min_score(data_pos);
	double max_score	= get_max_score(data_pos);
	if( !(min_score < max_score) ){
		cerr << "ERROR: check data scores: min_score(data) < max_score(data) must hold\n";
		exit(1);
	}	
	
	go_graph graph;
	vector<go_node> go_nodes;
	if(pars.obo_file != NULL){
		if(pars.verbal){
		cout << "# reading obo file\n"<<flush;
		}
		go_nodes = read_obo_file(pars.obo_file, true); // include obsolete nodes
		graph= create_go_graph(go_nodes);
	}
	//unsigned int go_count_ontology = graph.nodes.size(); 
	
	
	unordered_map<unsigned int,vector<unsigned int> > pmap;
	if(pars.obo_file != NULL){
		if(pars.verbal){
		cout << "# searching for GO parents\n"<<flush;
		}
		pmap = get_parent_map(graph,"isa,partof,consider,replacedby,altid",true,true,true);
	}
	
	
	unordered_map<unsigned int,double> icmap;
	if(pars.ic_file != NULL){
		if(pars.verbal){
		cout << "# reading information content\n";
		}
		read_information_content(pars.ic_file,icmap);
	}	
	
	
	vector<vector<unsigned int> > data_pos_relatives; // list of relatives for each data_pos entry
	if(pars.rotate_pos_ids){
		if(pars.verbal){
		cout<< "# searching for GO neighbours\n"<<flush;
		}
		
		for(unsigned int i=0; i<data_pos.size(); i++){

			vector<unsigned int> relatives = get_relatives(graph,
									data_pos[i].go,
									"isa,partof,consider,replacedby,altid",
									"parents",
									pars.R,
									true);
			// node itself
			// relatives.push_back(data_pos[i].go);
			
			data_pos_relatives.push_back(relatives); 			
		}
	}

	// errors and swaps
	vector<vector<double> > temp= get_errors_and_swaps(pars,data_pos.size());
	vector<double> error= temp[0];
	vector<double> swaps= temp[1];

	// no parent copy
	vector<go_annotation> data_pos_noparents= copy(data_pos);
	if(pars.propagate_parents)
		propagate_parents(data_pos, pmap);

	// score matrices
	double concurrent_metrics = 1;
	if(score_pars.scoref == "Smin"){
		concurrent_metrics = 6;
	}
	vector<vector<vector<double> > > score_matrices = create_3D_score_matrix(concurrent_metrics,pars.K,error.size());

	
	// output files for permuted data, one for each error category
	vector<FILE*> OUT_FILES(error.size());
	if(pars.mode == "data"){
		if(pars.verbal){
		cout << "# printing permuted data to:\n";
		}
		for(unsigned int ci=0; ci<error.size(); ci++){
			char file[1000];
			sprintf(file,"%s.c%u",pars.stats_file,ci);
			cout << "# \t"<<file<<"\n";
			OUT_FILES[ci] = fopen(file,"w");
			
			fprintf( OUT_FILES[ci],"# goa_pos:  %s\n",pars.goa_pos_file);
			fprintf( OUT_FILES[ci],"# goa_neg:  %s\n",pars.goa_neg_file);
			fprintf( OUT_FILES[ci],"# obo_file: %s\n",pars.obo_file);
			fprintf( OUT_FILES[ci],"# prop_par: %c\n",(pars.propagate_parents?'T':'F'));
			fprintf( OUT_FILES[ci],"# rot_pos:  %c\n",(pars.rotate_pos_ids?'T':'F'));
			fprintf( OUT_FILES[ci],"# R:        %i\n",pars.R);
			if(pars.seed != 0) fprintf( OUT_FILES[ci],"# rseed:    %i\n",pars.seed);
			fprintf( OUT_FILES[ci],"# error:    %f\n",error[ci]);
			fprintf( OUT_FILES[ci],"# perm\tgene\tgo\tscore\n"); // headers
		}
	}
	FILE* OUT_JACC = NULL;
	if(pars.jacc_file != NULL){
		OUT_JACC = fopen(pars.jacc_file,"w");
	}
	
	if(pars.verbal){
	cout << "# permuting data\n" << flush;}
	
	for(unsigned int pi=0; pi<pars.K; pi++){ // pi is permutation index
		if(pars.verbal && pi>0 && pi%10 == 0)
			cout << "# "<<pi<<" perm done\n" << flush;
		
				
		// preparing permutations for all error categories:
		vector<unsigned int> ind0;
		vector<unsigned int> ind1;
		get_permutation(data_pos_noparents,ind0,ind1,pmap,pars.max_ajac_sim);
		
		for(unsigned int ci=0; ci<error.size(); ci++){ // ci is index for error category

			// we permute data without parent nodes and add parents later
			vector<go_annotation> data_pred= copy(data_pos_noparents); 
				
			// rotate positive data ids in proximate neighbourhood
			if(pars.rotate_pos_ids){
				unsigned int data_size 		= data_pred.size();
				unsigned int rotated_fraction	= rand() % data_size;
				for(unsigned int i=0; i<= rotated_fraction; i++){
					unsigned int relatives_size	= data_pos_relatives[i].size();
					if(relatives_size == 0) // root node: no parents
						continue;
					unsigned int rand_i	= rand() % relatives_size;	
					data_pred[i].go		= data_pos_relatives[i][rand_i];
				}
			}
			
			// permute annotations
			unsigned int go_temp;
			for(unsigned int i=0; i<swaps[ci]; i++){
				go_temp= data_pred[ind0[i]].go;
				data_pred[ind0[i]].go= data_pred[ind1[i]].go;
				data_pred[ind1[i]].go= go_temp;
			}
			
			if(pars.add_goa_negatives)
				data_pred.insert(data_pred.end(),data_neg.begin(),data_neg.end());
				
			// print Jaccard similarity of rotated/permuted data
			if(pars.jacc_file != NULL){
				print_jaccard(data_pred,data_pos_noparents,pmap,ci,pi,error[ci],OUT_JACC);
			}
			
			if(pars.propagate_parents)
				propagate_parents(data_pred, pmap);	
			
			// SCORE PERMUTED DATA
			if(pars.mode != "data"){
				vector<double> score_list;
				
				if(score_pars.scoref == "Fmax"){
					score_matrices[0][pi][ci] = get_score_cafa_fmax(data_pred,data_pos);
				}
				
				else if(score_pars.scoref == "AUC"){
					score_matrices[0][pi][ci] = get_AUC(data_pred,data_pos,10,pars.verbal);
				}
				
				else if(score_pars.scoref == "Smin"){
					//PT Modified
					vector<double> Smin_Result = get_score_cafa_smin_PT(data_pred,data_pos,icmap);
					score_matrices[0][pi][ci] = Smin_Result[0];
					score_matrices[1][pi][ci] = Smin_Result[1];
					score_matrices[2][pi][ci] = Smin_Result[2];
					score_matrices[3][pi][ci] = Smin_Result[3];
					score_matrices[4][pi][ci] = Smin_Result[4];
				}
				
				else if(score_pars.list == "go"){
					if(score_pars.thf == "all"){
						unordered_map<string,double> temp= get_score_bygene(data_pred,
											data_pos,
											pmap,
											icmap,
											score_pars.scoref,
											score_pars.simf,
											score_pars.goclass_count,
											pars.verbal);
						score_list= get_values(temp);
					}
					else if(score_pars.thf == "th"){
						score_list= get_score_bygene_th(data_pred,
										data_pos,
										pmap,
										icmap,
										score_pars.scoref,
										score_pars.simf,
										score_pars.sumf1,
										pars.verbal);
					}
					else{
						cerr<<"ERROR: generate_scores(): invalid TH:"<<score_pars.thf<<"\n";exit(1);
					}
					
					// summarising scores to a single value
						// across genes with SUMF1
					if(score_pars.thf=="all"){
						if(score_pars.sumf1=="max")
							score_matrices[0][pi][ci]= max(score_list);
						else if(score_pars.sumf1=="mean")
							score_matrices[0][pi][ci]= mean(score_list);
						else if(score_pars.sumf1=="median")
							score_matrices[0][pi][ci]= median(score_list);
					}
						// across thresholds with SUMF2
					else if(score_pars.thf =="th"){
						if(score_pars.sumf2=="max")
							score_matrices[0][pi][ci]= max(score_list);
						else if(score_pars.sumf2=="mean")
							score_matrices[0][pi][ci]= mean(score_list);
						else if(score_pars.sumf2=="median")
							score_matrices[0][pi][ci]= median(score_list);
					}				
				}
			
				else if(score_pars.list == "gene"){
				
					score_list= get_score_bygo(data_pred,
								data_pos,
								score_pars.scoref,
								score_pars.sumf1,
								score_pars.minClassSize,
								pars.verbal);
					if(score_pars.sumf1 == "max")
						score_matrices[0][pi][ci]= max(score_list);		
					else if(score_pars.sumf1 == "mean")
						score_matrices[0][pi][ci]= mean(score_list);
					else if(score_pars.sumf1 == "median")
						score_matrices[0][pi][ci]= median(score_list);
				}
			
				else if(score_pars.list=="gene_go"){
				
					score_matrices[0][pi][ci]= get_score_singlelist(
								data_pred,
								data_pos,
								score_pars.scoref,
								score_pars.thf,
								score_pars.sumf2,
								score_pars.goclass_count);
				}
			}
			
			
			// PRINT PERMUTED DATA
			if(pars.mode == "data"){
			for(unsigned int i=0; i<data_pred.size(); i++){
				fprintf(OUT_FILES[ci],"%u\t%s\t%07u\t%f\n",
									pi,
									data_pred[i].gene.c_str(),
									data_pred[i].go,
									data_pred[i].score);
			}}		
		}
	}

	if(pars.mode != "data"){
	     char tmp_file_name[1000];
	     if(score_pars.scoref == "Smin"){
	     	score_pars.scoref= "Smin1";
	     	sprintf(tmp_file_name,"%s.Smin1.elab",pars.stats_file);
		print_data_table( pars, score_pars, score_matrices[0], tmp_file_name, error);
		score_pars.scoref= "Smin2";
		sprintf(tmp_file_name,"%s.Smin2.elab",pars.stats_file);
		print_data_table( pars, score_pars, score_matrices[1], tmp_file_name, error);
		score_pars.scoref= "Smin3";
		sprintf(tmp_file_name,"%s.Smin3.elab",pars.stats_file);
		print_data_table( pars, score_pars, score_matrices[2], tmp_file_name, error);
		score_pars.scoref= "SimGIC";
		sprintf(tmp_file_name,"%s.SimGIC.elab",pars.stats_file);
		print_data_table( pars, score_pars, score_matrices[3], tmp_file_name, error);
		score_pars.scoref= "SimGIC2";
		sprintf(tmp_file_name,"%s.SimGIC2.elab",pars.stats_file);
		print_data_table( pars, score_pars, score_matrices[4], tmp_file_name, error);
	     }
	     else{
	     	sprintf(tmp_file_name,"%s",pars.stats_file);
	     	print_data_table( pars, score_pars, score_matrices[0], tmp_file_name, error);
	     }
	} 	
		
	
	// PRINT POSITIVE DATA
	if(pars.mode == "data"){
	char tmp_file_name[1000];
	sprintf(tmp_file_name,"%s.true",pars.stats_file);
	if(pars.verbal)
		cout << "# printing pos data to: "<<tmp_file_name<<"\n";
	print_annotation(data_pos,tmp_file_name);
	}
}

void print_usage_short(char* name){
	cerr	<<"USAGE: "<< name <<" -k int -r int -d num -s int -g -p goa_pos -n goa_neg -b obo_file -i ic_file -o output -v -e str -m LIST=str,TH=str,SUMF1=str,SUMF2=str,SF=str,SIMF=str,MINCS=int,GOC=int\n\n"
		<<"-h   : print help manual\n";
}

void print_usage(char* name){
	cerr << "# elab v2.5: permuting and scoring Gene Onthology Annotation files\n"
		<< "# copyright (C) 2017 Ilja Pljusnin & Petri Toronen, University of Helsinki\n"
		<< "\n"
		<< "USAGE: "<< name <<" -k int -e str -r int -d num -s int -g -p goa_pos -n goa_neg -b obo_file -i ic_file -o output -j output2 -vh -m LIST=str,TH=str,SUMF1=str,SUMF2=str,SF=str,SIMF=str\n\n"
		<< "-k int [100]\t: num of permutations \n"
		<< "-g [off]    \t: propagate annotation to parents\n"
		<< "-r num [off]\t: rotate positive data ids in proximate neighbourhood, num denotes the size of the neighbourhood\n"
		<< "-s num [off]\t: random seed\n"
		<< "-d num [0.2]\t: max ajaccard similarity for permutation step, values in [0,1]\n"
		<< "-v [off]\t: verbal mode\n"
		<< "-h      \t: print help\n"
		
		<< "-p file \t: positive GO annotation [gene\\tgo\\tscore]\n"
		<< "-n file \t: negative GO annotation [gene\\tgo\\tscore]\n"
		<< "-b file \t: OBO 1.2 file\n"
		<< "-i file \t: information content file [GO\\tIC]\n"
		<< "-o file \t: output file\n"
		<< "-j file \t: print Jaccard distance of perm data to file. Format [error\\tperm\\tGO\\tJacc]\n"
		
		<< "-e str [0,0.2,by=0.02] \t: string literal specifying error levels, options:\n"
		<< "   min,max,by=num[,length=num]\n"
		<< "       min        : min error\n"
		<< "       max        : max error\n"
		<< "       by=num     : increase error linearly from min to max by num\n"
		<< "       length=num : increase error linearly from min to max with num error levels\n"
		
		<< "-m str [data]\t: output mode/evaluation method, valid options:\n"
		<< "   data      \t: permuted data\n"
		<< "   LIST=str,TH=str,SUMF1=str,SUMF2=str,SF=str,SIMF=str,MINCS=int,GOC=int\n"
		<< "             \t: parameters defining evaluation of permuted data(predicted) against positive\n\n"
		
		<< "      LIST=[go]   evaluate:\n"
		<< "            gene   : gene-lists for each go\n"
		<< "            go     : go-lists for each gene\n"
		<< "            gene_go: single-list of gene-go pairs\n\n"
		
		<< "      TH=[th]  threshold pred scores with options:\n"
		<< "            all    : for each gene evaluates ALL predictions against positives (no theshold)\n"
		<< "                     summarize scores across genes with SUMF1\n"
		<< "            geneth : for each gene evaluates predictions of all possible lengths (1st, 1st & 2nd, .., all)\n"
		<< "                     summarize scores first across thresholds (SUMF2), then across genes (SUMF1)\n"
		<< "            th     : like in CAFA I thresholds all data at stepwise increasing thresholds\n"
		<< "                     summarize scores first across genes (SUMF1), then across thresholds (SUMF2)\n\n"
		
		<< "      SUMF1=[mean] summarize scores across genes/gos with:\n"
		<< "            max|mean|median\n\n"
		
		<< "      SUMF2=[max] summarize scores across thresholds with:\n"
		<< "            max|mean|median\n\n"
		     
		<< "      SF=[Jacc]     score list of predicted gos/genes agains positives with:\n"
		<< "            U       : Mann-Whitney U-statistic\n"
		<< "            AUC     : golib.h:get_AUC(): multi-class AUC as defined by Ferri et al(2009)\n"
		<< "            AUC1    : area under ROC curve, scaled by (|TP||FN|) with pseudocounts\n"
		<< "            AUC2    : area under ROC curve, scaled by (|P||FN|) with pseudocounts\n"
		<< "            AUC3    : area under ROC curve, scaled by (|P||N|)\n"
		<< "            AUCPR   : auc by trapezoidal rule, under Precision Recall curve (not ROC)\n"
		<< "            Jacc    : JaccardIndex(pred,pos)\n"
		<< "            wJacc   : Jaccard index weighted by information content\n"
		<< "            Fmax    : Fmax metric (CAFA I & II)\n"
		<< "            Smin    : prints a collection of metrics related to Smin:\n"
		<< "                Smin1  : Smin metric (CAFA II)\n"
		<< "                Smin2  : S=ri+mi Smin variant\n"
		<< "                Smin3  : S=mean(Smin_per_gene) Smin variant\n"
		<< "                SimUI  : unweighted Jaccard\n"
		<< "                SimGIC : IC weighted Jaccard\n"
		<< "                SimGIC2: hybrid of weighted Jaccard and Smin\n"
		<< "            score_A : mean( sim_mat*(pred,pos) )\n"
		<< "            score_B : mean( col_max(sim_mat(pred,pos)) )\n"
		<< "            score_C : mean( row_max(sim_mat(pred,pos)) )\n"
		<< "            score_D : mean( score_B,score_C)\n"
		<< "            score_E : min( score_B,score_C)\n"
		<< "            score_F : mean( pooled(col_max(sim_mat),row_max(sim_mat)) )\n\n"
		<< "            *similarity matrix with rows for predictions and columns for positives\n\n"
		
		<< "      SIMF  compute similarity matrix with semantic similarity:\n"
		<< "            pJacc   : JaccardIndex( parents(pred),parents(pos) )\n"
		<< "            Resnik  : Resnik (1995)\n"
		<< "            Lin     : Lin (1998)\n"
		<< "            PathLin : Koskinen et al(2014)\n"
		<< "            JC      : Jiang & Conrath (1997)\n"
		<< "            id      : identity function (baseline)\n\n"
		
		<< "      MINCS Minimum size of GO class in Positive Set. Default to 10\n"
		<< "      GOC   Number of uniq GO classes (applied to AUC metrics)\n\n";
		
}

int main (int argc, char** argv) {


	if(argc<2){
		print_usage_short(argv[0]);
		exit(0);
	}

	// INPUT PARAMETERS
	parameters pars;
	pars.K=100;
	pars.R=0;
	pars.max_ajac_sim = 0.2;
	pars.seed= 0;
	pars.avoid_term_collisions= 	true;
	pars.propagate_parents= 	false;
	pars.add_goa_negatives= 	false;
	pars.rotate_pos_ids= 		false;
	pars.verbal= 			false;
	pars.prog_name= 	argv[0];
	pars.goa_pos_file= 	NULL;
	pars.goa_neg_file= 	NULL;
	pars.obo_file = 	NULL;
	pars.ic_file  =		NULL;
	pars.stats_file = 	NULL;
	pars.jacc_file	= 	NULL;
	pars.emode= 		"0,0.2,by=0.02";
	pars.mode = 		"data";
	
	score_parameters score_pars("go","th","mean","max","Jacc","NULL",10,46316);
	
	int option= 0;
	double val= 0;
	while ((option = getopt(argc, argv,"hk:r:d:s:gp:n:b:i:o:j:ve:m:")) != -1) {
        switch (option) {
		case 'h':
			print_usage(argv[0]);
			exit(0);
		case 'k' : 
	     		pars.K = atoi(optarg);
             		break;
		case 'r':
			pars.R = atoi(optarg);
			if(pars.R>0){
				pars.rotate_pos_ids= true;}
			break;
		case 'd':
			val = atof(optarg);
			if(val >=0 && val<=1){
				pars.max_ajac_sim = val;}
			break;
		case 's':
			pars.seed = atoi(optarg);
			break;
		case 'g':
			pars.propagate_parents= true;
			break;						
		case 'p':
			pars.goa_pos_file= optarg; 
                	break;
		case 'n':
			pars.goa_neg_file= optarg;
			pars.add_goa_negatives= true;
			break;
		case 'b' :
	     		pars.obo_file= optarg;
			break;
		case 'i' :
			pars.ic_file= optarg;
			break;
 		case 'o' :
	     		pars.stats_file= optarg;
                	break;
		case 'j' :
			pars.jacc_file= optarg;
			break;
		case 'v':
			pars.verbal= true;
			break;
		case 'e':
			pars.emode= string(optarg);
			break;
		case 'm':
			pars.mode= string(optarg);
			break;			
		case '?':
        		cerr << "option -"<<optopt<<" requires an argument\n";
        		exit(1);
             	default:
	     		print_usage(pars.prog_name); 
                	exit(1);
        	}
    	}
	
	// parsing -e mode
	vector<string> pairs= split(pars.emode,",");
	if(pairs.size()<3 || pairs.size()>4){
		cerr << "ERROR: invalid -e argument:\""<<pars.emode<<"\"\n";
		exit(1);
	}			
	pars.e_min= atof(pairs[0].c_str());
	pars.e_max= atof(pairs[1].c_str());
	for(unsigned int i=2; i<pairs.size(); i++){
		vector<string> pair= split(pairs[i],"=");
		if(pair.size() != 2){
			cerr << "ERROR: invalid -e argument:\""<<pars.emode<<"\"\n";
			exit(1);
		}
		if(pair[0]== "by"){
			pars.e_by= atof(pair[1].c_str());
			break;
		}
		else if(pair[0]== "length"){
			pars.e_by= (pars.e_max - pars.e_min)/atoi(pair[1].c_str());
		}
		else{
			cerr << "ERROR: invalid -e argument:\""<<pars.emode<<"\"\n";
			exit(1);
		}
	}
	if(pars.e_min < 0.0 || pars.e_max > 1.0 || pars.e_min >= pars.e_max || pars.e_by < 0.0 || pars.e_by > 1.0){
		cerr << "ERROR: invalid -e argument:\""<<pars.emode<<"\"\n";
		exit(1);
	}
	// DEBUG
	//cerr << "[min:by:max]"<<pars.e_min<<":"<<pars.e_by<<":"<<pars.e_max<<"\n"; exit(0);
		
	
	// PARSING -m mode
	if(pars.mode != "data"){
		vector<string> pair_list= split(pars.mode,",");
		for(unsigned int i=0; i<pair_list.size(); i++){
			vector<string> pair= split(pair_list[i],"=");
			if(pair.size() != 2){
				cerr << "ERROR: invalid -m argument:\""<<pair_list[i]<<"\"\n";
				exit(1);
			}

			if(pair[0] == "LIST" && (pair[1]=="gene" || pair[1]=="go" || pair[1]=="gene_go")){
				score_pars.list=pair[1];
			}
			else if(pair[0] == "TH" && (pair[1]=="all"
							|| pair[1]=="geneth"
							|| pair[1]=="th")){
				score_pars.thf= pair[1];
			}
			else if(pair[0] == "SUMF" && (pair[1]=="mean"
							|| pair[1]=="median"
							|| pair[1]=="max")){
				score_pars.sumf1 = pair[1];
				score_pars.sumf2 = pair[1];
			}
			else if(pair[0] == "SUMF1" && (pair[1]=="mean" 
							|| pair[1]=="median"
							|| pair[1]=="max")){
				score_pars.sumf1= pair[1];
			}
			else if(pair[0] == "SUMF2" && (pair[1]=="mean" 
							|| pair[1]=="median"
							|| pair[1]=="max")){
				score_pars.sumf2= pair[1];
			}			
			else if(pair[0] == "SF" && (pair[1]=="U"
							|| pair[1]=="U1"
							|| pair[1]=="U2"
							|| pair[1]=="U3"
							|| pair[1]=="AUC"							
							|| pair[1]=="AUC1"
							|| pair[1]=="AUC2"
							|| pair[1]=="AUC3"
							|| pair[1]=="AUCPR"
							|| pair[1]=="Jacc"
							|| pair[1]=="wJacc"
							|| pair[1]=="Fmax"
							|| pair[1]=="Smin"
							|| pair[1]== "score_A"
							|| pair[1]== "score_B"
							|| pair[1]== "score_C"
							|| pair[1]== "score_D"
							|| pair[1]== "score_E"
							|| pair[1]== "score_F")){
				score_pars.scoref= pair[1];
			}

			else if(pair[0] == "SIMF" && (pair[1]==   "pJacc" ||
							pair[1]== "Resnik"||
							pair[1]== "Lin" ||
							pair[1]== "PathLin" ||
							pair[1]== "JC" ||
							pair[1]== "id")){
				score_pars.simf= pair[1];
			}
			else if(pair[0] == "MINCS"){
				score_pars.minClassSize = atoi(pair[1].c_str());
			}
			else if(pair[0] == "GOC"){
				score_pars.goclass_count = atoi(pair[1].c_str());
			}
			else{
				cerr << "ERROR: invalid -m argument:\""<<pair_list[i]<<"\"\n";
				exit(1);
			}
		}
	}
	
	
	// SANITY CHECK
	if(pars.goa_pos_file == NULL){
		cerr << "ERROR: missing -p goa_pos\n\n";
		exit(1);
	}
	if(pars.stats_file == NULL){
		cerr << "ERROR: missing -o output\n\n";
		exit(1);
	}
	if(pars.ic_file==NULL){
		if(score_pars.scoref == "Smin"){
			cerr<<"ERROR: -m SF=Smin requires -i ic_file\n";
			exit(1);
		}
		if(score_pars.simf=="Resnik" 
			|| score_pars.simf=="Lin" 
			|| score_pars.simf=="PathLin" 
			|| score_pars.simf=="JC"){
			cerr << "ERROR: using SIMF="<<score_pars.simf<<" requires -i ic_file\n";
			exit(1);
		}
	}
	if(pars.obo_file == NULL){
		if(pars.propagate_parents){
			cerr<< "ERROR: parent propagation requires obo file: try -g -b obo_file\n";
			exit(1);
		}
		if(pars.rotate_pos_ids){
			cerr<< "ERROR: rotation of positive data ids requires obo file: try -r "<<pars.R<<" -b obo_file\n";
			exit(1);
		}			
		if(score_pars.simf !=  "NULL"){ 
			cerr<< "ERROR: using -m SIMF requires -b obo_file\n";
			exit(1);
		}
	}
	
	// DEBUG
	if(pars.verbal){
		cout << "# input parameters:\n";
		print_parameters(pars);
		if( pars.mode != "data")
			print_score_parameters(score_pars);
	}
	
	// PERMUTATIONS
	if(pars.seed == 0)
		srand(time(NULL));
	else
		srand(pars.seed);

	generate_scores(pars,score_pars);
	
	exit(0);
}
	








