#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>
#include <queue>
#include <unordered_map>
#include <cstring>
#include <algorithm>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

// OTHER FILES
#include <golib.h>
#include <gograph.h>

using namespace std;


/*
 * UTILITY PROGRAM THAT PROPAGATES IC FROM NODES WITH DEFINED IC TO THEIR CLOSEST NEIGHBORS WITH NO IC
 *
 */
 

struct parameters{
	char* prog_name;
	char* obo_file;
	char* ic_file_in;
	char* ic_file_out;
	bool verbal;
};



void print_usage_short(char* name){
	cerr << "USAGE: "<< name <<" -i ic_file_in -o ic_file_out -b obo_file -v\n\n";
}
		
int main (int argc, char** argv) {


	if(argc<2){
		print_usage_short(argv[0]);
		exit(0);
	}

	// INPUT PARAMETERS
	parameters pars;
	pars.prog_name= 	argv[0];
	pars.obo_file = 	NULL;
	pars.ic_file_in  =	NULL;
	pars.ic_file_out = 	NULL;
	pars.verbal= 		false;
	
	int option= 0;
	while ((option = getopt(argc, argv,"i:o:b:v")) != -1) {
        switch (option) {
		case 'i' :
			pars.ic_file_in= optarg;
			break;
 		case 'o' :
	     		pars.ic_file_out= optarg;
                	break;
		case 'b' :
			pars.obo_file= optarg;
			break;
		case 'v':
			pars.verbal= true;
			break;			
		case '?':
        		cerr << "option -"<<optopt<<" requires an argument\n";
        		exit(1);
             	default:
	     		print_usage_short(pars.prog_name); 
                	exit(1);
        	}
    	}
	
	// SANITY CHECK
	if(pars.ic_file_in == NULL){
		cerr << "ERROR: missing -i ic_file_in\n\n";
		exit(1);
	}
	if(pars.ic_file_out == NULL){
		cerr << "ERROR: missing -o ic_file_out\n\n";
		exit(1);
	}
	if(pars.obo_file == NULL){
		cerr << "ERROR: missing -b obo_file\n\n";
		exit(1);
	}
	

	// DO THE JOB
	if(pars.verbal)
		cout << "# reading obo file\n"<<flush;
	vector<go_node> go_nodes = read_obo_file(pars.obo_file,false); // do not include obsolete nodes
	go_graph graph= create_go_graph(go_nodes);
	
	if(pars.verbal)
		cout << "# reading ic file\n"<<flush;
	unordered_map<unsigned int,double> icmap;
	read_information_content(pars.ic_file_in,icmap);
	
	unsigned int ic_parsed= icmap.size();
	unsigned int ic_propagated_nodes= 0;
	unsigned int ic_propagated_altids= 0;
	
	if(pars.verbal)
		cout<< "# extending ic\n"<<flush;
	for(unsigned int i=0; i<go_nodes.size(); i++){
	
		go_node node= go_nodes[i];
		
		vector<unsigned int> rel_list;
		if(icmap.count(node.id) == 0){
			// get 1 relative with defined IC
			rel_list= get_relatives(graph,
						node.id,
						"isa,partof,consider,replacedby,altid",
						"relatives",
						1,
						true,
						icmap);
			if(rel_list.size()>0 && icmap.count(rel_list[0])>0){
				icmap[node.id] = icmap[rel_list[0]];
				ic_propagated_nodes++;
			}
			else{
				fprintf(stderr,"WARNING: failed to propagate IC to node GO:%07u\t",go_nodes[i].id);
			}
		}
		
		// propagating to alt ids
		for(unsigned int j=0; j<node.altid_list.size(); j++){
			if( icmap.count(node.altid_list[j]) == 0){
				icmap[node.altid_list[j]]= icmap[node.id];
				ic_propagated_altids++;
			}
		}
	}
	if(pars.verbal){
	cout << "# \tIC parsed:         "<<ic_parsed<<"\n";
	cout << "# \tIC prop to nodes:  "<<ic_propagated_nodes<<"\n";
	cout << "# \tIC prop to altids: "<<ic_propagated_altids<<"\n\n";}
	
	if(pars.verbal)
		cout<< "# printing extended ic\n";
	print_information_content(pars.ic_file_out,icmap);
		
	exit(0);
}
	
