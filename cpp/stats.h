#include <iostream>
#include <vector>
#include <unordered_map>
#include <set>
#include <algorithm>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <stdio.h>
#include <float.h>
using namespace std;

 /*
 * BASIC STATISTICS
 *
 */
double sum(const vector<double> &v){
	double sum=0;
	unsigned int n= v.size();
	for(unsigned int i=0; i<n; i++)
		sum+= v[i];
	return sum;
}
double mean(const vector<double> &v){
	return (sum(v)/double(v.size()));
}
double median(const vector<double> &v_original){
	
	vector<double> v= v_original;
	sort(v.begin(),v.end());
	
	unsigned int n= v.size();
	
	if(n == 0)
		return 0;
	else if(n%2 == 1)
		return v[n/2];
	else
		return (v[n/2]+v[n/2-1])/2.0;
}
double max(vector<double> &v){

	return *max_element(v.begin(),v.end());
}
double min(vector<double> &v){

	return *min_element(v.begin(),v.end());
}

 
 /*
  * Samples v.size() number of elements from v without replacement and saves them to v.
  * 
  * Permutes v.
  * 
  * Based on pseudorandom number generator rand()
  *
  * Note: you might want to call srand(time(NULL)) before a series of calls to this function
  */
vector<int> permute(const vector<int> &v){
	
	unsigned int N= v.size();
	unsigned int r;

	int temp;
	vector<int> copy(N,0);
	for(unsigned int i=0; i<N; i++)
		copy[i]= v[i];
	
	
	for(unsigned int i=0; i<N-1; i++){

		r= (int)floor(rand()%(N-i))+i;
		temp= copy[r];
		copy[r]= copy[i];
		copy[i]= temp;
	}
	return copy;
}


/* PT addition
 * This code is for generation of ranks.
 *
 * Aim is to allow also ties between values
 * value for ties is the average
 *
 * value is float as the average value is often float
 * Input data must ordered before using this code
 *
 * There are some rank-functions that might do better job!!
 */
vector<double> create_rank_vector(const vector<double> data_in ){

  vector<double> output( data_in.size(), 0);
  vector<unsigned int> ind_vect;
  unsigned int tmp_sum = 0;
  double check = 0;
  unsigned int ii;
  double  tmp_score = 0;  
  for( unsigned int i=0; i<data_in.size(); i++){
	ii = i + 1;
	if( i == 0){
  		check = data_in[i];
		tmp_sum = ii;
		output[i] = ii;
		ind_vect.push_back( i );
  	}
	else{
		if( data_in[i] == check){
			ind_vect.push_back( i );
			tmp_sum = tmp_sum + ii;
		}
		
		else{
			
			if( ind_vect.size() > 1){
			   tmp_score = double (tmp_sum) / ind_vect.size();			   
			   for(unsigned int j=0; j<ind_vect.size(); j++){
			      output[ind_vect[j]] = tmp_score;
			   }
			}
			ind_vect.clear();
			ind_vect.push_back( i );
			tmp_sum = ii;
			output[i] = ii;
			check = data_in[i];
			
		} // IF-Else data_in
		 
	} //IF-ELSE i == 0
  } //FOR loop
  if( ind_vect.size() > 1){
	tmp_score = double (tmp_sum) / ind_vect.size();			   
	for(unsigned int j=0; j<ind_vect.size(); j++){
		output[ind_vect[j]] = tmp_score;
	}
  }
  return output;
}

/* Following is rank vector calculus for precision recall style analysis
 * Aim: Give score data + binary vector. Positive and negative count is 
 * generated at each point when score value changes.
 * This code takes into the consideration the ties
 *	INPUTS:
 * data_in = scores vector. This can include ties. SHould be sorted still
 * prediction = boolean vector (correct/wrong) prediction
 *	OUTPUTS
 * vector of TP counts
 * vector of FP counts
 * 
 * NOTE length of vector depends on the unique values in score vector
 */

vector<vector<unsigned int>> create_rank_vect_PR(vector<double> data_in, vector<bool> prediction ){

  if( data_in.size() != prediction.size()){
	cerr << "Error in create_rank_vect_PR (stats.h) Input sizes don't match!\n";
	cerr << "data_in length " << data_in.size() << " prediction sz " << prediction.size() << "\n";
  }
  unsigned int pos_count = 0;
  unsigned int neg_count = 0;
  vector<unsigned int> pos_count_vector;
  vector<unsigned int> neg_count_vector;
  double check = data_in[0];
  for( unsigned int i=0; i<data_in.size(); i++){
	if( data_in[i] != check){
		pos_count_vector.push_back( pos_count );
		neg_count_vector.push_back( neg_count );
		check = data_in[i];
		 
	} //IF-ELSE i == 0

	if(prediction[i]){
		pos_count++;	 
	}
	else{
		neg_count++;
	}

  } //FOR loop
  pos_count_vector.push_back( pos_count );
  neg_count_vector.push_back( neg_count );
  vector<vector<unsigned int>> all_out(2);
  all_out[0] = pos_count_vector;
  all_out[1] = neg_count_vector;
  return all_out;
}

/* 
 * Calculate the area under any curve with trapezoidal rule
 *
 */

double Get_AreaUnderCurve( vector<double> Xvalues, vector<double> Yvalues){

	if( Xvalues.size() != Yvalues.size()){
		cerr << "Error in Get_AreaUnderCurve (stats.h) Input sizes don't match!\n";
		cerr << "Xvalues length " << Xvalues.size() << " Yvalues sz " << Yvalues.size() << "\n";
	}
        double ValueOut = Xvalues[0]*Yvalues[0];
	if( Xvalues.size() > 1){	
		for( unsigned int i=1; i<Xvalues.size(); i++){
			ValueOut += 0.5*( Xvalues[i] - Xvalues[i-1])*( Yvalues[i] + Yvalues[i-1]);
		}
	}
	if( ValueOut > 1 || ValueOut < 0){
		cerr << "errors in Area calculus \n";
	        double check = Xvalues[0]*Yvalues[0];
		cerr << "\n" << check << " ";
		for( unsigned int i=1; i<Xvalues.size(); i++){
			 cerr << check << " ";
			 check+= 0.5*( Xvalues[i] - Xvalues[i-1])*( Yvalues[i] + Yvalues[i-1]);
		}
		cerr <<"\n";
		
		for( unsigned int i=1; i<Xvalues.size(); i++){
			 check = 0.5*( Xvalues[i] - Xvalues[i-1])*( Yvalues[i] + Yvalues[i-1]);
			 cerr << check << " ";
		}
		cerr <<"\n";
		cerr << "area calculus ctrl output ends\n";	
	}
	return ValueOut;
}


/*
 * Calculates ROC AUC by trapezoidal rule.
 * 
 * Input
 * 
 * pred		list of predictions
 * pos		list of positives
 * scale
 *	1:	A / (|TP||FP|)
 *	2:	A / (|P||FP|)
 *	3:	A / (|P||N|), requires N_size parameter
 * N_size	size of the negative set
 *
 * NOTE: If TP or FP sets used in the denominator (scale==1 OR scale==2) are empty, 
 *       adds pseudocount of one pos/neg prediction to the end of the pred-list
 *
 */
template <typename T>
double get_auc(const vector<T> &pred, const vector<T> &pos, unsigned int scale, unsigned int N_size=0){
	
	if(scale == 3 && N_size == 0){
		fprintf(stderr,"# ERROR: get_auc: scale=3 requires N_size\n");
		exit(1);
	}
	
	set<T> pos_set;
	for(unsigned int i=0; i<pos.size(); i++)
		pos_set.insert(pos[i]);
	
	vector<bool> pred_binary(pred.size(),false);
	unsigned int TP_size= 0;
	unsigned int FP_size= 0;
	unsigned int P_size=0;
	
	for(unsigned int i=0; i<pred.size(); i++){
		if(pos_set.count(pred[i])>0){
			pred_binary[i]= true;
			TP_size++;
		}
		else{
			pred_binary[i]= false;
			FP_size++;
		}
	}
	
	
	if(scale == 1){
		//pseudocounts
		if(TP_size == 0){
			pred_binary.push_back(true);
			TP_size++;
		}
		if(FP_size == 0){
			pred_binary.push_back(false);
			FP_size++;
		}
		P_size = TP_size;
		N_size = FP_size;		
	}
	else if(scale == 2){
		// pseudocounts
		if(FP_size == 0){
			pred_binary.push_back(false);
			FP_size++;
		}
		P_size = pos.size();
		N_size = FP_size;		
	}
	else if(scale == 3){
		P_size = pos.size();
		//N_size as parameter
	}
	else{
		fprintf(stderr,"# ERROR: get_auc(scale=%u,N_size=%u): undefined scale\n", scale,N_size);
		exit(1);
	}
	
	if(P_size == 0 || N_size == 0){
		fprintf(stderr,"# ERROR: get_auc(scale=%u,N_size=%u): denominator is zero: P_size=%3u,N_size=%3u\n",scale,N_size,P_size,N_size);
		exit(1);
	}
		
	
	// calculate AUC
	unsigned int FP= 0;
	unsigned int TP= 0;
	unsigned int FP_prev= 0;
	double AUC=0;
	
	for(unsigned int i=0; i<pred_binary.size(); i++){
	
		if(pred_binary[i])
			TP++;
		else
			FP++;
			
		if(FP != FP_prev){
			AUC+= TP;
			FP_prev= FP;
		}
	}
	AUC+= double(N_size-FP)*double(TP);
	AUC = AUC/( double(P_size)*double(N_size));
	
	// DEBUG
	if(AUC > 1){
		fprintf(stderr,"# WARNING: get_auc: |P|=%3u, |N|=%3u, AUC=%3f\n", P_size,N_size,AUC);
	}
	
	return AUC;
}



/*
 * Returns U statistic for positives in a list of predictions
 *
 * pred		vector of predictions
 * pos		vector of positives
 * scale	scales U, valid values are 0,1 and 2
 *	0	does not scale (default)
 *	1	scales U by |TP||FP|, corresponds to AUC1
 *	2	scales U by |P||FP|, corresponts to AUC2
 *	3	scales U by |P||N|, requires N_size par
 * N_size	size of the negative set
 * 
 *
 * returns	U statistic/scaled U statistic for positive predictions
 *
 * NOTE: for scales 0, 1 and 2 we use pseudocounts
 *	if all positives, we add one negative to the end of pred list
 *	if all negatives, we add one positive to the end of pred list
 *
 */
template <typename T>
double get_U(const vector<T> &pred, const vector<T> &pos, unsigned int scale=0, unsigned int N_size=0){

	set<T> pos_set;
	for(unsigned int i=0; i<pos.size(); i++)
		pos_set.insert(pos[i]);
	
	vector<bool> pred_binary(pred.size(),false);
	long long unsigned int TP_ranksum= 0;
	long long unsigned int TP_size= 0;
	long long unsigned int FP_size= 0;
	unsigned int P_size=0;

	for(unsigned int i=0; i<pred.size(); i++){
		if(pos_set.count(pred[i])>0){
			pred_binary[i]= true;
			TP_size++;
		}
		else{
			pred_binary[i]= false;
			FP_size++;
		}
	}

	if(scale == 0 || scale == 1){
		//pseudocounts
		if(TP_size == 0){
			pred_binary.push_back(true);
			TP_size++;
		}
		if(FP_size == 0){
			pred_binary.push_back(false);
			FP_size++;
		}
		P_size = TP_size;
		N_size = FP_size;		
	}
	else if(scale == 2){
		// pseudocounts
		if(FP_size == 0){
			pred_binary.push_back(false);
			FP_size++;
		}
		P_size = pos.size();
		N_size = FP_size;
	}
	else if(scale == 3){
		P_size = pos.size();
		//N_size as parameter
	}
	else{
		fprintf(stderr,"ERROR: get_U: undefined par value: scale=%u\n",scale);
		exit(1);
	}	

	// RANKSUM
	unsigned int n= pred_binary.size() + (N_size-FP_size);
	for(unsigned int i=0; i<pred_binary.size(); i++){
		if(pred_binary[i])
			TP_ranksum+= (n-i);
	}
	
	double U= double(TP_ranksum) - (double(TP_size)*double(TP_size+1))/2.0;
	
	
	if(scale > 0){
		U = U / ( double(P_size) * double(N_size) );
	}
	
	if((scale>0 && U>1) || U < 0 || U != U ){  // last check for NaN
		fprintf(stderr,"#ERROR: get_U: invalid U value:\n");
		fprintf(stderr,"scale=%u\nTP_size=%u\nFP_size=%u\nP_size=%u\nN_size=%u\nTP_ranksum=%f\nU=%f\n",
			scale,int(TP_size),int(FP_size),int(P_size),int(N_size),double(TP_ranksum),double(U));
		exit(1);
	}
	return U;
}

	 
/* PT addition
 *
 * Returns Mann-Whitney U-statistic for positives in a list of predictions.
 * This version handles predictions with ties correctly.
 *
 * pred		vector of predictions
 * pos		vector of positives
 * scores	vector od prediction scores
 * scale	scales U, valid values are 0,1 and 2
 *	0	does not scale (default)
 *	1	scales U by |TP||FP|, corresponds to AUC1
 *	2	scales U by |P||FP|, corresponts to AUC2
 *	3	scales U by |P||N|, requires N_size par
 * N_size	size of the negative set
 * 
 *
 * returns	U statistic/scaled U statistic for positive predictions
 *
 * NOTE: for scales 0, 1 and 2 we use pseudocounts
 *	if all positives, we add one negative to the end of pred list
 *	if all negatives, we add one positive to the end of pred list
 *
 * PT modification.Code below has AddPointsToVectors (boolean) variable.
 * It selects whether we add unobserved predictions to the end of the vector
 *
 * old version of this code is get_U_with_ranks_old
 */
 
 template <typename T>
double get_U_with_ranks(const vector<T> &pred, const vector<T> &pos, vector<double> &scores, 
                        unsigned int scale=0, unsigned int N_size=0, bool AddPointsToVectors = true){

	set<T> pos_set;
	for(unsigned int i=0; i<pos.size(); i++)
		pos_set.insert(pos[i]);
	
	vector<bool> pred_binary(pred.size(),false);
	double TP_ranksum= 0;
	long long unsigned int TP_size= 0;
	long long unsigned int FP_size= 0;
	// PT addition. Variables below are a controls
	// FP_ranksum could be used to confirm that calculations are correct
	double FP_ranksum= 0;
	double flipped_TP_ranksum= 0;
	double flipped_FP_ranksum= 0;	
	unsigned int P_size=0;

	// Define TP_size and FP_size in the
	// code below
	for(unsigned int i=0; i<pred.size(); i++){
		if(pos_set.count(pred[i])>0){
			pred_binary[i]= true;
			TP_size++;
		}
		else{
			pred_binary[i]= false;
			FP_size++;
		}
	}
	
	// PT Addition
	// 
	double new_low_value = min(scores) - 0.01;
	unsigned int old_length = pred.size();
	//if(scale == 0){
	//}
	if(scale == 0 || scale == 1){
		//pseudocounts
		if(TP_size == 0){
			pred_binary.push_back(true);
			scores.push_back(new_low_value);
			TP_size++;
		}
		if(FP_size == 0){
			pred_binary.push_back(false);
			scores.push_back(new_low_value);
			FP_size++;
		}
		P_size = TP_size;
		N_size = FP_size;
		
		
	}
	else if(scale == 2){

		if(FP_size == 0){
			pred_binary.push_back(false);
			scores.push_back(new_low_value);
			FP_size++;
		}
		P_size = pos.size();
		N_size = FP_size;
				
	}
	else if(scale == 3){
		P_size = pos.size();
		//N_size as parameter
	}
	else{
		fprintf(stderr,"ERROR: get_U: undefined par value: scale=%u\n",scale);
		exit(1);
	}	
	
	if( AddPointsToVectors){
		// PT: Need to add extra points to vectors, if we have N_size or P_size as input
		// Otherwise we calculate AUC under subset of the curve
        	if( P_size > TP_size){
			unsigned int add_length = P_size - TP_size;
			vector<bool> pred_added(add_length,true);
			vector<double> scores_added(add_length,new_low_value);
			pred_binary.reserve( pred_binary.size() + pred_added.size());
			pred_binary.insert( pred_binary.end(), pred_added.begin(), pred_added.end());
			scores.reserve( scores.size() + scores_added.size());
			scores.insert( scores.end(), scores_added.begin(), scores_added.end());
			TP_size = P_size;
		}
		if( N_size > FP_size){
			unsigned int add_length = N_size - FP_size;
			vector<bool> pred_added(add_length,false);
			vector<double> scores_added(add_length,new_low_value);
			pred_binary.reserve( pred_binary.size() + pred_added.size());
			pred_binary.insert( pred_binary.end(), pred_added.begin(), pred_added.end());
			scores.reserve( scores.size() + scores_added.size());
			scores.insert( scores.end(), scores_added.begin(), scores_added.end());
			FP_size = N_size;
		}
	}
	// Ilja: There was a bug below. n = pred.size() ... causes neg. U values with pseudo counts
	//       It should rather be n = pred.size() + 1
	// RANKSUM
	unsigned int n= pred_binary.size() + (N_size-FP_size) + 1;
	// PT addition: control the code. Not used in calculus
	// Used when debugging 
	unsigned int n2= pred_binary.size() + (N_size-TP_size) + 1;
	
	// PT addition: Create rank vector. This allows ties in scores
	// might also use builtin function
	vector<double> rank_values = create_rank_vector(scores);	
	for(unsigned int i=0; i<pred_binary.size(); i++){
		if(pred_binary[i]){
			TP_ranksum+= rank_values[i];
			flipped_TP_ranksum+= (n - rank_values[i]);
		}
		else{ // control with FP ranks. 
			FP_ranksum+= rank_values[i];
			flipped_FP_ranksum+= (n2 - rank_values[i]);
			
		}
	}
	
	double U= double(flipped_TP_ranksum) - (double(TP_size)*double(TP_size+1))/2.0;
	
	
	if(scale > 0){
		U = U / ( double(P_size) * double(N_size) );
	}
	if( ( U > 1 && scale > 0) || U < 0 || U != U ){  // last check for NaN
	    double unscaled_U= double(flipped_TP_ranksum) - (double(TP_size)*double(TP_size+1))/2.0;
	    cerr << "Error in get_U_with_ranks. U and Parameters below \n";
	    cerr << " U: " << U << " n " << n << " unscaled_U " << unscaled_U <<"\n";
	    cerr << "scale: " << scale << " N_size: " << N_size ;
	    cerr << " AddPointsToVectors: " << AddPointsToVectors ;
	    cerr << " P size: " << P_size << " FP size " << FP_size << " TP size " << TP_size << "\n";
	    cerr << " pred length: " << pred.size() << " pos size: " <<  pos.size() << " old length " << old_length << "\n" ;
	    cerr << " pred_binary length: "<< pred_binary.size() << " scores length: " << scores.size() << "\n";
	    cerr << "TP_ranksum: " << TP_ranksum << " FP_ranksum: " << FP_ranksum;
	    cerr << " flipped_TP_ranksum " << flipped_TP_ranksum << " flipped_FP_ranksum " << flipped_FP_ranksum <<"\n";
            if( rank_values.size() < 20 ){
	    	cerr << "Lists \n";
		for(unsigned int i=0; i<rank_values.size(); i++){
			cerr << scores[i] << " ";  
		}
		cerr << "\n";
		for(unsigned int i=0; i<rank_values.size(); i++){
		       cerr << rank_values[i] << " ";  
		}
		cerr << "\nflipped ranks\n";
		for(unsigned int i=0; i<rank_values.size(); i++){
		       cerr << n - rank_values[i] << " ";  
		}
	    }
	    cerr << "\n\n";
	}
	return U;
}	

 
template <typename T>
double get_U_with_ranks_old(const vector<T> &pred, const vector<T> &pos, vector<double> &scores, 
                        unsigned int scale=0, unsigned int N_size=0){

	set<T> pos_set;
	for(unsigned int i=0; i<pos.size(); i++)
		pos_set.insert(pos[i]);
	
	vector<bool> pred_binary(pred.size(),false);
	double TP_ranksum= 0;
	long long unsigned int TP_size= 0;
	long long unsigned int FP_size= 0;
	// PT addition. Variables below are a controls
	// FP_ranksum could be used to confirm that calculations are correct
	double FP_ranksum= 0;
	double flipped_TP_ranksum= 0;
	double flipped_FP_ranksum= 0;	
	unsigned int P_size=0;

	// Define TP_size and FP_size in the
	// code below
	for(unsigned int i=0; i<pred.size(); i++){
		if(pos_set.count(pred[i])>0){
			pred_binary[i]= true;
			TP_size++;
		}
		else{
			pred_binary[i]= false;
			FP_size++;
		}
	}
	
	// PT Addition
	// 
	double new_low_value = min(scores) - 0.01;
	unsigned int old_length = pred.size();
	if(scale == 0 || scale == 1){
		//pseudocounts
		if(TP_size == 0){
			pred_binary.push_back(true);
			scores.push_back(new_low_value);
			TP_size++;
		}
		if(FP_size == 0){
			pred_binary.push_back(false);
			scores.push_back(new_low_value);
			FP_size++;
		}
		P_size = TP_size;
		N_size = FP_size;	
	}
	else if(scale == 2){
		if(FP_size == 0){
			pred_binary.push_back(false);
			scores.push_back(new_low_value);
			FP_size++;
		}
		P_size = pos.size();
		N_size = FP_size;			
	}
	else if(scale == 3){
		P_size = pos.size();
		//N_size as parameter
	}
	else{
		fprintf(stderr,"ERROR: get_U: undefined par value: scale=%u\n",scale);
		exit(1);
	}	

	// RANKSUM
	unsigned int n= pred_binary.size() + (N_size-FP_size) + 1;
	// PT addition: control the code. Not used in calculus
	// Used when debugging 
	unsigned int n2= pred_binary.size() + (N_size-TP_size) + 1;
	
	// PT addition: Create rank vector. This allows ties in scores
	// might also use builtin function
	vector<double> rank_values = create_rank_vector(scores);	
	for(unsigned int i=0; i<pred_binary.size(); i++){
		if(pred_binary[i]){
			TP_ranksum+= rank_values[i];
			flipped_TP_ranksum+= (n - rank_values[i]);
		}
		else{ // control with FP ranks. 
			FP_ranksum+= rank_values[i];
			flipped_FP_ranksum+= (n2 - rank_values[i]);
			
		}
	}
	
	double U= double(flipped_TP_ranksum) - (double(TP_size)*double(TP_size+1))/2.0;
	
	
	if(scale > 0){
		U = U / ( double(P_size) * double(N_size) );
	}
	if( ( U > 1 && scale > 0) || U < 0 || U != U ){  // last check for NaN
	    double unscaled_U= double(flipped_TP_ranksum) - (double(TP_size)*double(TP_size+1))/2.0;
	    cerr << "Error in get_U_with_ranks. U and Parameters below \n";
	    cerr << " U: " << U << " n " << n << " unscaled_U " << unscaled_U <<"\n";
	    cerr << "scale: " << scale << " N_size: " << N_size ;
	    cerr << " P size: " << P_size << " FP size " << FP_size << " TP size " << TP_size << "\n";
	    cerr << " pred length: " << pred.size() << " pos size: " <<  pos.size() << " old length " << old_length << "\n" ;
	    cerr << " pred_binary length: "<< pred_binary.size() << " scores length: " << scores.size() << "\n";
	    cerr << "TP_ranksum: " << TP_ranksum << " FP_ranksum: " << FP_ranksum;
	    cerr << " flipped_TP_ranksum " << flipped_TP_ranksum << " flipped_FP_ranksum " << flipped_FP_ranksum <<"\n";
	    cerr << "Listat \n";
	    for(unsigned int i=0; i<rank_values.size(); i++){
	       cerr << scores[i] << " ";  
	    }
	    cerr << "\n";
	    for(unsigned int i=0; i<rank_values.size(); i++){
	       cerr << rank_values[i] << " ";  
	    }
	    cerr << "\nflipped ranks\n";
	    for(unsigned int i=0; i<rank_values.size(); i++){
	       cerr << n - rank_values[i] << " ";  
	    }
	    
	    cerr << "\n\n";
	    exit(1);
	}
	return U;
}

template <typename T>
double get_U_with_ranks_and_details(const vector<T> &pred, const vector<T> &pos, vector<double> &scores, 
                        unsigned int scale=0, unsigned int N_size=0){

	cout << "\nrunning get_U_with_ranks_and_details\n";
	set<T> pos_set;
	for(unsigned int i=0; i<pos.size(); i++)
		pos_set.insert(pos[i]);
	
	vector<bool> pred_binary(pred.size(),false);
	double TP_ranksum= 0;
	long long unsigned int TP_size= 0;
	long long unsigned int FP_size= 0;
	// PT addition. Variable below is a control variable
	// FP_ranksum could be used to confirm that calculations are correct
	double FP_ranksum= 0;
	double flipped_TP_ranksum= 0;
	double flipped_FP_ranksum= 0;
	unsigned int P_size=0;

	// Define TP_size and FP_size in the
	// code below
	for(unsigned int i=0; i<pred.size(); i++){
		if(pos_set.count(pred[i])>0){
			pred_binary[i]= true;
			TP_size++;
		}
		else{
			pred_binary[i]= false;
			FP_size++;
		}
	}

	// PT Addition
	// 
	double new_low_value = min(scores) - 0.01;
	unsigned int old_length = pred.size();

	//if(scale == 0){
	//}
	if(scale == 0 || scale == 1){
		/*
		// old version. Did not understand this
		P_size = TP_size;
		N_size = FP_size;
		
		//pseudocounts
		if(P_size == 0){
			pred_binary.push_back(true);
			scores.push_back(new_low_value);
			P_size++;
		}
		if(N_size == 0){
			pred_binary.push_back(false);
			scores.push_back(new_low_value);
			N_size++;
		}
		*/
		
		if(TP_size == 0){
			pred_binary.push_back(true);
			scores.push_back(new_low_value);
			TP_size++;
		}
		if(FP_size == 0){
			pred_binary.push_back(false);
			scores.push_back(new_low_value);
			FP_size++;
		}
		P_size = TP_size;
		N_size = FP_size;
		
		
	}
	else if(scale == 2){
		/* Old version. Did not understand this
		P_size = pos.size();
		N_size = FP_size;
		
		// pseudocounts
		if(N_size == 0){
			pred_binary.push_back(false);
			scores.push_back(new_low_value);
			N_size++;
		}
		*/
		if(FP_size == 0){
			pred_binary.push_back(false);
			scores.push_back(new_low_value);
			FP_size++;
		}
		P_size = pos.size();
		N_size = FP_size;
				
		
	}
	else if(scale == 3){
		P_size = pos.size();
		//N_size as parameter
	}
	else{
		fprintf(stderr,"ERROR: get_U: undefined par value: scale=%u\n",scale);
		exit(1);
	}	

	// RANKSUM
	unsigned int n= pred.size() + (N_size-FP_size) + 1;
	// // PT addition: control the code. NOT USED CURRENTLY
	unsigned int n2= pred.size() + (N_size-TP_size) + 1;
	
	cout << "n: " << n << " size: " << pred.size() << " N_size: " << N_size ;
	cout << " FP_size: " << FP_size << "\n"; 
	cout << "Score values\n";
	for(unsigned int i=0; i<scores.size(); i++){
		cout << scores[i] << " ";
	}
	cout << "\nGenerated Rank values\n";	
	// PT addition: Create rank vector. This allows ties in scores
	// might also use builtin function
	vector<double> rank_values = create_rank_vector(scores);	
	for(unsigned int i=0; i<rank_values.size(); i++){
		cout << rank_values[i] << " ";
	}
	cout << "\nFlipped rank sums (N -rank_value)\n";
	for(unsigned int i=0; i<pred_binary.size(); i++){
		double tmp = n - rank_values[i];
		cout << tmp << " ";
		if(pred_binary[i]){
			TP_ranksum+= rank_values[i];
			flipped_TP_ranksum+= (n - rank_values[i]);
		}
		else{ // control with FP ranks. 
			FP_ranksum+= rank_values[i];
			flipped_FP_ranksum+= (n2 - rank_values[i]);
		}
	}
	cout << "\n";
	cout << "TP-ranksum " << TP_ranksum << " \n";
	cout << "FP-ranksum " << FP_ranksum << " \n";
	cout << "flipped TP-ranksum " << flipped_TP_ranksum << " \n";
	cout << "flipped FP-ranksum " << flipped_FP_ranksum << " \n";
	double U= double(TP_ranksum) - (double(TP_size)*double(TP_size+1))/2.0;	
	cout << U <<"\n";
	if(scale > 0){
		U = U / ( double(P_size) * double(N_size) );
	}
	cout << "output " << U << " scale " << scale << "\n";
	cout << "scale: " << scale << " N_size: " << N_size ;
	cout << " P size: " << P_size << " FP size " << FP_size << " TP size " << TP_size << "\n";
	cout << " pred length: " << pred.size() << " pos size: " <<  pos.size() << " old length " << old_length << "\n" ;
	cout << " pred_binary length: "<< pred_binary.size() << " scores length: " << scores.size() << "\n";
	cout << "TP_ranksum: " << TP_ranksum << " FP_ranksum: " << FP_ranksum;
	cout << " flipped_TP_ranksum " << flipped_TP_ranksum << " flipped_FP_ranksum " << flipped_FP_ranksum <<"\n";
	
	cout << "\n\n test Precision REcall \n\n";
	vector<vector<unsigned int>> two_results = create_rank_vect_PR(scores, pred_binary);
	vector<unsigned int> total_counts( two_results[0].size(), 0);
	vector<double> precision( two_results[0].size());
	vector<double> recall( two_results[0].size());
	//unsigned int GO_class_size = max( two_results[0]);
	cout << "pos counts \n";
	for(unsigned int i=0; i<two_results[0].size(); i++){
		cout << two_results[0][i] << " ";
		total_counts[i] = two_results[0][i] + two_results[1][i];
		precision[i] = double(two_results[0][i]) / total_counts[i] ;
		recall[i]    = double(two_results[0][i]) / TP_size ;
	}
	cout << "\nneg counts \n";
	for(unsigned int i=0; i<two_results[1].size(); i++){
		cout << two_results[1][i] << " ";
	}
	cout << "\n total counts \n";
	for(unsigned int i=0; i<two_results[1].size(); i++){
		cout << total_counts[i] << " ";
	}
	cout << "\n ";
	double AUC_PR = Get_AreaUnderCurve(  precision, recall);
	cout << "AUC precision recall " << AUC_PR << "\n";

	return U;
}	 




template <typename T>
double get_PR_AUC_ranks(const vector<T> &pred, const vector<T> &pos, vector<double> &scores ){

	set<T> pos_set;
	unsigned int TP_size = pos.size();
	for(unsigned int i=0; i<pos.size(); i++)
		pos_set.insert(pos[i]);
	
	vector<bool> pred_binary(pred.size(),false);
	for(unsigned int i=0; i<pred.size(); i++){
		if(pos_set.count(pred[i])>0){
			pred_binary[i]= true;
		}
		else{
			pred_binary[i]= false;
		}
	}
	
	vector<vector<unsigned int>> two_results = create_rank_vect_PR(scores, pred_binary);

	vector<unsigned int> total_counts( two_results[0].size(), 0);
	vector<double> precision( two_results[0].size());
	vector<double> recall( two_results[0].size());

	for(unsigned int i=0; i<two_results[0].size(); i++){
		total_counts[i] = two_results[0][i] + two_results[1][i];
		precision[i] = double(two_results[0][i]) / total_counts[i] ;
		recall[i]    = double(two_results[0][i]) / TP_size ;
	}
	
	double output = Get_AreaUnderCurve( recall, precision);
	
	if( output < 0 || output > 1 || output != output ){  // last check for NaN
	    cerr << "Error in get_PR_AUC_ranks. Output and Parameters below \n";
	    cerr << "output " << output << " ";
	    cerr << "TP_size " << TP_size << "\n";
	    double check_val;
	    for(unsigned int i=1; i<two_results[0].size(); i++){
	    	check_val = 0.5*(recall[i] - recall[i-1])*(precision[i]+precision[i-1]);
		cerr<< check_val << " ";
	    }
	    cerr << "\n";
	    for(unsigned int i=0; i<two_results[0].size(); i++){
	    	cerr<< two_results[0][i] << " ";
	    }
	    cerr << "\n";
	    for(unsigned int i=0; i<two_results[0].size(); i++){
	    	cerr<< two_results[1][i] << " ";
	    }
	    cerr << "\n";
	    for(unsigned int i=0; i<two_results[0].size(); i++){
	    	cerr<< precision[i] << " ";
	    }
	    cerr << "\n";
	    for(unsigned int i=0; i<two_results[0].size(); i++){
	    	cerr<< recall[i] << " ";
	    }
	    cerr << "\n";
	}
	return output;
}	
 

/*
 * Returns intersection of v1 and v2
 *
 * time complexity: N*LOG(N)
 */
template <typename T>
vector<T> set_intersection(const vector<T> &_v1,const vector<T> &_v2){
	vector<T> v1 = _v1;
	vector<T> v2 = _v2;
	unsigned int i=0;
	unsigned int j=0;
	unsigned int n1=v1.size();
	unsigned int n2=v2.size();
	vector<T> result;
	
	// sort
	if(!is_sorted(v1.begin(),v1.end()))
		sort(v1.begin(),v1.end());
	if(!is_sorted(v2.begin(),v2.end()))
		sort(v2.begin(),v2.end());
	
	while(i<n1 && j<n2){
		if(v1[i]<v2[j]) ++i;
		else if(v1[i]>v2[j]) ++j;
		else{
			result.push_back(v1[i]);
			++i;++j;
		}
	}
	return result;
}


/*
 * Returns set union of v1 and v2
 *
 * time complexity same as for sorting: N*LOG(N)
 */
template <typename T>
vector<T> set_union(const vector<T> &_v1,const vector<T> &_v2){
	vector<T> v1 = _v1;
	vector<T> v2 = _v2;
	unsigned int i=0;
	unsigned int j=0;
	unsigned int n1=v1.size();
	unsigned int n2=v2.size();
	vector<T> result;

	// sort
	if(!is_sorted(v1.begin(),v1.end()))
		sort(v1.begin(),v1.end());
	if(!is_sorted(v2.begin(),v2.end()))
		sort(v2.begin(),v2.end());
	
	while(true){
    		if (i==n1){
			for(unsigned int k=j; k<n2; k++)
				result.push_back(v2[k]);
			return result;
		}
    		if(j==n2){
			for(unsigned int k=i; k<n1; k++)
				result.push_back(v1[k]);
			return result;
		}

		if(v1[i]<v2[j]){ result.push_back(v1[i]); ++i; }
    		else if (v1[i]>v2[j]){ result.push_back(v2[j]); ++j; }
		else { result.push_back(v1[i]); ++i; ++j; }
	}
	
	return result;
}


/*
 * Returns set difference: v1 - v2
 *
 * time complexity same as for sorting: N*LOG(N)
 */
template <typename T>
vector<T> set_difference(const vector<T> &_v1,const vector<T> &_v2){
	vector<T> v1 = _v1;
	vector<T> v2 = _v2;
	unsigned int i=0;
	unsigned int j=0;
	unsigned int n1=v1.size();
	unsigned int n2=v2.size();
	vector<T> result;
	
	// sort
	if(!is_sorted(v1.begin(),v1.end()))
		sort(v1.begin(),v1.end());
	if(!is_sorted(v2.begin(),v2.end()))
		sort(v2.begin(),v2.end());
	
	while(i<n1 && j<n2){
    		if(v1[i]<v2[j]){ result.push_back(v1[i]); ++i; }
    		else if (v1[i]>v2[j]){  ++j; }
		else { ++i; ++j; }
	}
	while(i<n1){
		result.push_back(v1[i]);
		++i;
	}
	
	return result;
}


/*
 * Returns Jaccard index for vectors pred and pos.
 *
 */
template <typename T>
double get_Jaccard(const vector<T> &pred,
			const vector<T> &pos){

	vector<T> I= set_intersection(pred,pos);
	vector<T> U= set_union(pred,pos);
	return (double(I.size())/double(U.size()));
}


/*
 * Returns Jaccard index weighted by Information Content.
 *
 */
double get_WJaccard(const vector<unsigned int> &pred, 
			const vector<unsigned int> &pos, 
			unordered_map<unsigned int,double> &icmap){
	
	vector<unsigned int> I= set_intersection(pred,pos);
	vector<unsigned int> U= set_union(pred,pos);
	double intersect_wsum = 0;
	double union_wsum = 0;
	for(unsigned int k=0; k<I.size(); k++){
		if( icmap.count(I[k]) == 0){
			fprintf(stderr,"ERROR: get_WJaccard(): undefined IC for GO:%07u\n",I[k]);
			exit(1);}
		intersect_wsum += icmap[I[k]];
	}
	
	for(unsigned int k=0; k<U.size(); k++){
		if( icmap.count(U[k]) == 0){
			fprintf(stderr,"ERROR: get_WJaccard(): undefined IC for GO:%07u\n",U[k]);
			exit(1);}
		union_wsum += icmap[U[k]];
		}				
	return (intersect_wsum / union_wsum);				
}


template <typename T>
double get_Dice(vector<T> &pred,vector<T> &pos){
	
	vector<T> I= set_intersection(pred,pos);
	return (double(I.size())/double(pred.size()+pos.size()));
} 



/*
 * Encodes a tuple of int keys <key1,key2> into a single long long int.
 * 
 * Keys can be in any range [0,2^32]
 */
long long unsigned int encode_key(unsigned int key1, unsigned int key2){

	long long unsigned int key1_long= key1;
	long long unsigned int key2_long= key2;
	return ((key1_long << 32) | key2_long);
}
/*
 * Decodes a single long long int key into a tuple of int keys <key1,key2>
 *
 * Returns an array containing key1 and key2
 */
unsigned int* decode_key(long long unsigned int key){
	unsigned int* keys = new unsigned int[2];
	long long unsigned int mask= 4294967296-1; // 2^32-1 = suffix of 32 one bits
	keys[0]= (key >> 32);
	keys[1]= (key & mask);
	return keys;
}
vector<long long unsigned int> encode_keys(vector<unsigned int> keys1,vector<unsigned int> keys2){
	
	if(keys1.size() != keys2.size()){
		cerr << "ERROR: encode_keys: keys1.size()!=keys2.size()\n";
		exit(1);
	}
	vector<long long unsigned int> keys_new(keys1.size(),0);
	for(unsigned int i=0; i<keys_new.size(); i++)
		keys_new[i]= encode_key(keys1[i],keys2[i]);
	
	return keys_new;
}





/*
 * WORKING WITH MATRICES
 *
 */
void validate_matrix(const vector<vector<double> > &mat){
	if(mat.size() == 0){
		cerr << "ERROR: validate(matrix): no rows\n";
		exit(1);
	}
	else if(mat[0].size() == 0){
		cerr << "ERROR: validate(matrix): no cols\n";
		exit(1);
	}
	unsigned int nrow= mat.size();
	unsigned int ncol= mat[0].size();
	for(unsigned int i=0; i<nrow; i++){
		if(mat[i].size() != ncol){
			cerr << "ERROR: validate(matrix): inconsistent rows\n";
			exit(1);
		}
	}
}
 
vector<double> as_vector(const vector<vector<double> > &mat){
	validate_matrix(mat);
	
	vector<double> v;
	for(unsigned int i=0; i<mat.size(); i++){
		v.insert(v.end(),mat[i].begin(),mat[i].end());
	}
	return v;
}

vector<vector<double> > invert(const vector<vector<double> > &mat){
	
	validate_matrix(mat);

	unsigned int nrow= mat.size();
	unsigned int ncol= mat[0].size();
	
	vector<vector<double> > imat(ncol);
	for(unsigned int i=0; i<ncol; i++){
		vector<double> row(nrow,0);
		imat[i]= row;
	}
	
	for(unsigned int i=0; i<nrow; i++)
		for(unsigned int j=0; j<ncol; j++)
			imat[j][i]= mat[i][j];
	
	return imat;
}
vector<double> row_max(vector<vector<double> > mat){
	
	validate_matrix(mat);
	
	vector<double> v(mat.size(),0);
	
	for(unsigned int i=0; i<mat.size(); i++){
		v[i]= max(mat[i]);
	}
	return v;
}
	
vector<double> col_max(vector<vector<double> > mat){
	
	return row_max(invert(mat));
}



	

	
