#include <math.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <iostream>

#include <utils.h>

using namespace std;




int main(){
	
	vector<double> v({5,4,3,2,1,6,7,10,9,8});
	
	vector<unsigned int> ind(10,0);
	for(unsigned int i=0; i<10; i++){
		ind[i] = i;
	}

	cout << "i: "<< to_string(v) <<"\n";
	cout << "v: "<< to_string(v) <<"\n";
	
	sort(ind.begin(),ind.end(),[&v](unsigned int i1,unsigned int i2){return v[i1] < v[i2]; });
	vector<double> v_sorted(v);
	for(unsigned int i=0; i<10; i++){
		v_sorted[i] = v[ind[i]];
	}
	v = v_sorted;
	
	cout << "after sorting\n";
	cout << "i: "<< to_string(v) <<"\n";
	cout << "v: "<< to_string(v) <<"\n";
}	


