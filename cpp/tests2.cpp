#include <iostream>
#include <vector>
#include <set>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <stdio.h>
#include <float.h>
#include <getopt.h>

// OTHER FILES
//#include <stats.h> // PT added
#include <golib2_dev.h>
#include <gograph.h>
using namespace std;


/*
 * UTILITY FUNCTIONS
 */
 
/*
 * Returns a c-string representation of a vector of ints.
 *
 */
char* to_string(vector<long long unsigned int> v){
	string s="{";
	
	if(v.size()>0)
		s+=to_string(v[0]);
	for(unsigned int i=1; i<v.size(); i++)
		s+= ","+to_string(v[i]);
		
	s+="}";
	
	char* c= new char[s.size()+1];
	strcpy(c,s.c_str());
	
	return c;
}
char* to_string(vector<unsigned int> v){
	vector<long long unsigned int> v_(v.begin(),v.end());
	return (to_string(v_));
}


/* 
 * TESTS FOR <stats.h> methods
 */

void test_rand(unsigned int nbins){
	
	if(nbins > 100000){
		cerr << "ERROR: nbins > 100000 is not supported\n";
		exit(1);
	}
	const unsigned int nrolls = 1000*nbins;
        int counts[nbins];
        for(unsigned int i=0; i<nbins; i++)
                counts[i]=0;

        srand(time(NULL));

        for(unsigned int i=0; i<nrolls; i++){
                int r= rand() % nbins;
                counts[r]++;
        }

        printf("# test stdlib.h::rand(nrolls=%u,nbins=%u)\n",nrolls,nbins);
        for(unsigned int i=0; i<nbins; i++)
                printf("%2u\t%6u\n",i,counts[i]);
}

void test_rand2(){
	const int nrolls = 10000;
        const int bins= 5;
        int counts[bins];
        for(int i=0; i<bins; i++)
                counts[i]=0;

        srand(time(NULL));
	
	for(int i=0; i<nrolls; i++){
		double r=  rand()/(RAND_MAX+0.0);
		
		if(r < 0.2)
			counts[0]++;
		else if(r < 0.4)
			counts[1]++;
		else if(r < 0.6)
			counts[2]++;
		else if(r < 0.8)
			counts[3]++;
		else
			counts[4]++;
		
	
	}
	printf("# test2 stdlib.h::rand(nrolls=%u,bins=%u)\n",nrolls,bins);
        for(int i=0; i<bins; i++)
                printf("%2u\t%6u\n",i,counts[i]);
}
	
void test_permute(){
	const unsigned int n = 10;
	const unsigned int nperm= 100;
	vector<int> perm_0(n,0);
	for(unsigned int i=0; i<n; i++)
		perm_0[i]= i;
	
	cout<< "# test stats.h:permute():\n";
	for(unsigned int pi=0; pi<nperm; pi++){
	
		vector<int> perm= permute(perm_0);
	
		printf("perm %3u:\t",pi);
		for(unsigned int i=0; i<perm.size(); i++)
			printf("%2u ",perm[i]);
		printf("\n");
	}
}

/*
 * BASIC STATISTICS
 */
void test_basic_stats(){
	cout << "# test <stats.h>:min(),max(),mean(),median()\n";
	
	unsigned int iterations=10;
	unsigned int n_max=10;
	srand(time(NULL));
	
	cout<< "it\tmax\tmin\tmean\tmedian\tvector\n";
	for(unsigned int i=0; i<iterations; i++){
	
		unsigned int n= rand() %n_max;
		vector<double> v(n+1,0);
		for(unsigned j=0; j<v.size(); j++)
			v[j]= rand()/(RAND_MAX+0.0);
		
		printf("%2u\t%2.2f\t%2.2f\t%2.2f\t%2.2f\t",i,max(v),min(v),mean(v),median(v));
		for(unsigned int j=0; j<v.size(); j++)
			printf(",%2.2f",v[j]);
		printf("\n");
	}
}

/*
 * MATRIX STATS
 */
void test_matrix_stats(){
	cout << "# test <stats.h>:as_vector,row_max,row_min\n";
	
	unsigned int iterations=5;
	unsigned int nrow_max=10;
	unsigned int ncol_max=10;
	srand(time(NULL));
	
	for(unsigned int it=0; it<iterations; it++){
	
		unsigned int nrow= rand() %nrow_max+1;
		unsigned int ncol= rand() %ncol_max+1;
		
		vector<vector<double> > mat(nrow);
		for(unsigned int i=0; i<nrow;i++){
			vector<double> row(ncol,0);
			mat[i]= row;
			
			for(unsigned int j=0; j<ncol; j++)
				mat[i][j]= (rand()/(RAND_MAX+0.0))*10.0;
		}
		
		cout <<"it "<<(it+1)<<":\n";
		cout <<"mat:\n";
		for(unsigned int i=0; i<mat.size(); i++){
			for(unsigned int j=0; j<mat[0].size(); j++){
				printf("%2.1f ",mat[i][j]);
			}
			printf("\n");
		}
		
		cout <<"as_vector(mat): [";
		vector<double> v= as_vector(mat);
		for(unsigned int i=0; i<v.size(); i++){
			printf(" %2.1f",v[i]);
		}
		printf("]\n");
		
		cout <<"row_max(mat): [";
		v= row_max(mat);
		for(unsigned int i=0; i<v.size(); i++){
			printf(" %2.1f",v[i]);
		}		
		printf("]\n");
		
		cout <<"col_max(mat): [";
		v= col_max(mat);
		for(unsigned int i=0; i<v.size(); i++){
			printf(" %2.1f",v[i]);
		}
		printf("]\n");			
	}
}

void test_get_auc(){
	printf("#test stats.h:get_auc/get_U\n");


	// simulated data
	
	// pos 0 to 49
	vector<unsigned int> pos(50,0);
	for(unsigned int id=0; id<50; id++)
		pos[id]= id;
	// negatives 50 to 99
	//unsigned int pos_size= 50;
	unsigned int neg_size= 50;
			
	
	// pred with diff accuracies
	double acc[] = {0, 0.01,0.05,0.10,0.5,0.7,0.8,0.9,0.95,0.99,1.0};
	unsigned int acc_len= sizeof(acc)/sizeof(acc[0]);
	
	srand(time(NULL));
	
	printf("# pred with variable accuracy\n");
	printf("it  acc  AUC1  AUC2  AUC3  U  U1  U2  U3\n");
	for(unsigned int acci=0; acci<acc_len; acci++){
		
		
		unsigned int it_num=10;
		vector<double> auc1(it_num,0);
		vector<double> auc2(it_num,0);
		vector<double> auc3(it_num,0);
		vector<double> U(it_num,0);
		vector<double> U1(it_num,0); // scaled U statistic
		vector<double> U2(it_num,0);
		vector<double> U3(it_num,0);
		for(unsigned int it=0; it<it_num; it++){
		
		vector<unsigned int> pred;
		vector<unsigned int> pos_pred;
		vector<unsigned int> neg_pred;
		
		// pred for pos
		for(unsigned int pos_id=0; pos_id<50; pos_id++){
		
			double r= rand()/(RAND_MAX+0.0);
			
			if(r < acc[acci]){
				pos_pred.push_back(pos_id); // TP
			}
			else{
				neg_pred.push_back(pos_id); // FN
			}
		}
		
		// pred for negatives
		for(unsigned int neg_id=50; neg_id<100; neg_id++){
			 
			 double r= rand()/(RAND_MAX+0.0);
			 
			 if(r < acc[acci]){
			 	//neg_pred.push_back(neg_id); // TN
			}
			else{
				pos_pred.push_back(neg_id); // FP
			}
		}
		
		pred.insert(pred.end(),pos_pred.begin(),pos_pred.end());
		pred.insert(pred.end(),neg_pred.begin(),neg_pred.end());
		
		
		// AUC
		auc1[it] = get_auc(pred,pos,1);
		auc2[it] = get_auc(pred,pos,2);
		auc3[it] = get_auc(pred,pos,3,neg_size);
		U[it]=    get_U(pred,pos);
		U1[it]=  get_U(pred,pos,1);
		U2[it]=  get_U(pred,pos,2);
		U3[it]= get_U(pred,pos,3,neg_size);
		
		printf("%2u %1.2f %1.3f  %1.3f  %1.3f  %4.f  %1.3f  %1.3f  %1.3f\n",
			(it+1),acc[acci],auc1[it],auc2[it],auc3[it],U[it],U1[it],U2[it],U3[it]);
		}
		
		printf("mean      %1.3f  %1.3f  %1.3f  %1.3f  %1.3f  %1.3f  %1.3f\n",
			mean(auc1),mean(auc2),mean(auc3),mean(U),mean(U1),mean(U2),mean(U3));
		printf("\n");
		
	}
	
	printf("# all pos, no negatives (perfect classifier):\n");
	printf("AUC1=%1.3f AUC3=%1.3f \t U1=%1.3f U3=%1.3f\n",
		get_auc(pos,pos,1),
		get_auc(pos,pos,3,neg_size),
		get_U(pos,pos,1),
		get_U(pos,pos,3,neg_size));
	
	printf("# all negatives, no pos (worst case):\n");
	vector<unsigned int> pred;
	for(unsigned int id=50; id<100; id++)
		pred.push_back(id);

	printf("AUC1=%1.3f AUC3=%1.3f \t U1=%1.3f U3=%1.3f\n",
		get_auc(pred,pos,1),
		get_auc(pred,pos,3,neg_size),
		get_U(pred,pos,1),
		get_U(pred,pos,3,neg_size));

	
	printf("# all pos, one negative:\n");
	pred= pos;
	pred.push_back(99);

	printf("AUC1=%1.3f AUC3=%1.3f \t U1=%1.3f U3=%1.3f\n",
		get_auc(pred,pos,1),
		get_auc(pred,pos,3,neg_size),
		get_U(pred,pos,1),
		get_U(pred,pos,3,neg_size));
		
	printf("# all negatives, one positive:\n");
	pred.clear();
	for(unsigned int i=50; i<100; i++)
		pred.push_back(i);
	pred.push_back(0);
	
	printf("AUC1=%1.3f AUC3=%1.3f \t U1=%1.3f U3=%1.3f\n",
		get_auc(pred,pos,1),
		get_auc(pred,pos,3,neg_size),
		get_U(pred,pos,1),
		get_U(pred,pos,3,neg_size));
}

void test_get_U(){

	cout << "#test stats.h:get_U\n";
	
	// class:T F F F F F T T T T  T  F
	// pred: 1 2 3 4 5 6 7 8 9 10 11 12
	// pos:  1 7 8 9 10 11
	
	unsigned int pred_tmp[]= {1,2,3,4,5,6,7,8,9,10,11,12};
	unsigned int pos_tmp[]= {1,7,8,9,10,11};
	
	// Make positive sets 
	unsigned int very_pos1[]= {1,2,3,5,6,8};
	unsigned int very_pos2[]= {1,2,3,4,7,8};
	unsigned int very_pos3[]= {1,2,3,4,8,10};
	
	unsigned int less_pos1[]= {1,3,4,6,7,9};
	unsigned int less_pos2[]= {2,3,4,6,7,10};
	unsigned int less_pos3[]= {2,3,6,7,9,10};
	
	unsigned int random1[]= {1,3,5,7,9,11};
	unsigned int random2[]= {2,4,6,8,10,12};
	unsigned int random3[]= {4,5,6,7,8,9};
	
	double scores1[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
	double scores2[] = {1, 1, 3, 4, 5, 5, 5, 8, 9, 10, 11, 12};	// some ties
	double scores3[] = {1,1,3,4,5,6,6,6,11,11,12,12};		// some ties
	double scores4[] = {1,1,1,1,1,1,1,1,1 ,1 ,1, 1};		// all ties
	
		
	vector<unsigned int> pred(pred_tmp,(pred_tmp+12));
	vector<unsigned int> pos(pos_tmp,(pos_tmp+6));

	double auc_a=	get_auc(pred, pos,1);		// This works
	
	double auc1=	get_auc(pred_tmp,very_pos1,1);	// These do not work 
	double auc2=	get_auc(pred_tmp,very_pos1,2);
	double U=	get_U(pred_tmp,very_pos1);
	double U1= 	get_U(pred_tmp,very_pos1,1);
	double U2= 	get_U(pred_tmp,very_pos1,2);
	
	double U_r=	get_U_with_ranks(pred_tmp,very_pos1,scores1);
	double U_r1= 	get_U_with_ranks(pred_tmp,very_pos1,scores1,1);
	double U_r2= 	get_U_with_ranks(pred_tmp,very_pos1,scores1,2);
	
	cout << "# 6 TP and 6 FP: T F F F F F T T T T T F\n";
	cout << "AUC2= "<< auc2<<" ";
	cout << "U = "<<U<<" ";
	cout << "U1= "<<U1<<" ";
	cout << "U2= "<<U2<<" ";
	cout << "U_r = "<<U_r<<" ";
	cout << "U_r1= "<<U_r1<<" ";
	cout << "U_r2= "<<U_r2<<" ";
	cout << "\n";

	U_r=	get_U_with_ranks(pred_tmp,very_pos1,scores2);
	U_r1= 	get_U_with_ranks(pred_tmp,very_pos1,scores2,1);
	U_r2= 	get_U_with_ranks(pred_tmp,very_pos1,scores2,2);
	
	auc2=	get_auc(pred_tmp,very_pos1,1);
	auc2=	get_auc(pred_tmp,very_pos1,2);
	U=	get_U(pred_tmp,very_pos1);
	U1= 	get_U(pred_tmp,very_pos1,1);
	U2= 	get_U(pred_tmp,very_pos1,2);
	
	// ADD PRINT COMMANDS HERE
	
	U_r=	get_U_with_ranks(pred_tmp,very_pos1,scores4);
	U_r1= 	get_U_with_ranks(pred_tmp,very_pos1,scores4,1);
	U_r2= 	get_U_with_ranks(pred_tmp,very_pos1,scores4,2);
	
	cout << "# 6 TP and 6 FP: T F F F F F T T T T T F\n";
	cout << "AUC2= "<< auc2<<" ";
	cout << "U = "<<U<<" ";
	cout << "U1= "<<U1<<" ";
	cout << "U2= "<<U2<<" ";
	cout << "U_r = "<<U_r<<" ";
	cout << "U_r1= "<<U_r1<<" ";
	cout << "U_r2= "<<U_r2<<" ";
	cout << "\n";
		
}


/*
 * Terst merging two unsigned ints into a single long long unsigned int for hashing.
 */
void test_hash_key_encoding(){
	cout << "#test stats.h:encode_key()/decode_key()\n";

	unsigned int test_num= 10;
	
	srand(time(NULL));
	
	// key1: gene
	vector<unsigned int> genes(test_num,0);
	unsigned int genes_min=0;
	unsigned int genes_max=100001;
	for(unsigned int i=0; i<genes.size(); i++){
		unsigned int temp= floor((rand()/(RAND_MAX+0.0))*genes_max);
		genes[i]= temp;
	}
	genes[0]= genes_min;
	genes[1]= genes_max;
	
	// key2: GO
	vector<unsigned int> gos(test_num,0);
	unsigned int gos_min=0; 
	unsigned int gos_max= 10000001;
	for(unsigned int i=0; i<gos.size(); i++){
		unsigned int temp= floor((rand()/(RAND_MAX+0.0))*gos_max);
		gos[i]= temp;
	}
	gos[0]= gos_min;
	gos[1]= gos_max;
	
	// encode keys
	vector<long long unsigned int> keys= encode_keys(genes,gos);
	
	
	cout << "sizeof(unsigned int): "<<sizeof(unsigned int)<<"\n";
	cout << "sizeof(long long unsigned int): "<<sizeof(long long unsigned int)<<"\n";
	
	printf("gene\tgo\tkey\tgene_dec\tgo_dec\n");
	for(unsigned int i=0; i<test_num; i++){
		long long unsigned int key= keys[i];
		unsigned int* key_decoded= decode_key(key);
		
		cout <<genes[i]<<"\t"<<gos[i]<<"\t"<<key<<"\t"<<key_decoded[0]<<"\t"<<key_decoded[1]<<"\n";
	}
}



void test_set_operations(){
	cout << "#test stats.h:set_intersection(),set_union(),set_difference()\n";
	
	unsigned int a1[10]= {1,2,3,4,5,6,7,8,9,10};
	unsigned int a2[10]= {6,7,8,9,10,11,12,13,14,15};
	unsigned int a3[10]= {11,12,13,14,15,16,17,18,19,20};
	
	vector<unsigned int> v0(0);
	vector<unsigned int> v1(a1,a1+10);
	vector<unsigned int> v2(a2,a2+10);
	vector<unsigned int> v3(a3,a3+10);
	//vector<long long unsigned int> vi;
	//double J;
	
	cout<<"v1"<<to_string(v1)<<"\n";
	cout<<"v2"<<to_string(v2)<<"\n";
	cout<<"v3"<<to_string(v3)<<"\n";
	
	cout <<" v1\tv2\tintersection\tunion\tdifference\n";
	printf("%s\t%s\t%s\t%s\t%s\n",to_string(v1),to_string(v2),to_string(set_intersection(v1,v2)),to_string(set_union(v1,v2)),to_string(set_difference(v1,v2)));
	printf("%s\t%s\t%s\t%s\t%s\n",to_string(v1),to_string(v1),to_string(set_intersection(v1,v1)),to_string(set_union(v1,v1)),to_string(set_difference(v1,v1)));
	printf("%s\t%s\t%s\t%s\t%s\n",to_string(v1),to_string(v3),to_string(set_intersection(v1,v3)),to_string(set_union(v1,v3)),to_string(set_difference(v1,v3)));

printf("%s\t%s\t%s\t%s\t%s\n",to_string(v1),to_string(v0),to_string(set_intersection(v1,v0)),to_string(set_union(v1,v0)),to_string(set_difference(v1,v0)));
	
}

void test_get_Jaccard(){
	cout << "#test stats.h:set_intersection() set_union() get_Jaccard() get_Dice()\n";
	
	unsigned int a1[10]= {1,2,3,8,9,10,4,5,6,7};
	unsigned int a2[10]= {12,13,8,9,14,15,6,7,10,11};
	unsigned int a3[10]= {16,17,18,19,20,11,12,13,14,15};
	
	vector<unsigned int> v0(0);
	vector<unsigned int> v1(a1,a1+10);
	vector<unsigned int> v2(a2,a2+10);
	vector<unsigned int> v3(a3,a3+10);
	//vector<long long unsigned int> vi;
	//double J;
	
	cout<<"v1"<<to_string(v1)<<"\n";
	cout<<"v2"<<to_string(v2)<<"\n";
	cout<<"v3"<<to_string(v3)<<"\n";
	
	cout <<" v1\tv2\tint\tunion\tJaccard\n";
	printf("%s\t%s\t%s\t%s\t%1.3f\n",to_string(v1),to_string(v2),to_string(set_intersection(v1,v2)),to_string(set_union(v1,v2)),get_Jaccard(v1,v2));
	printf("%s\t%s\t%s\t%s\t%1.3f\n",to_string(v1),to_string(v1),to_string(set_intersection(v1,v1)),to_string(set_union(v1,v1)),get_Jaccard(v1,v1));
	printf("%s\t%s\t%s\t%s\t%1.3f\n",to_string(v1),to_string(v3),to_string(set_intersection(v1,v3)),to_string(set_union(v1,v3)),get_Jaccard(v1,v3));

printf("%s\t%s\t%s\t%s\t%1.3f\n",to_string(v1),to_string(v0),to_string(set_intersection(v1,v0)),to_string(set_union(v1,v0)),get_Jaccard(v1,v0));
	
}	



/* TEST <golib.h>
 *
 * read_go_annotation_data
 * group_go_annotation_data
 * read_goparents
 * read_information_content
 * propagate_parents
 * print_go_annotation_data
 * print_go_annotation_data (structured by gene)
 *
 */
void test_golib(const char* goa_file, const char* gop_file, const char* ic_file){

	cout << "#test golib.h\n";

	cout << "# test data1= read_go_annotation_data("<< goa_file<<"):\n";
	vector<go_annotation> data1= read_go_annotation_data(goa_file);
	print_go_annotation_data(data1);
	
	cout << "# test get_min_score(data1)\n";
	cout << "min_score = "<< get_min_score(data1)<<"\n";

	cout << "# test get_max_score(data1)\n";
	cout << "max_score = "<< get_max_score(data1)<<"\n";	

	cout << "# test data2= group_go_annotation_data(data1,\"gene\"):\n";
	sort(data1.begin(),data1.end(),compare_gene_score);
	vector<vector<go_annotation> > data2= group_go_annotation_data(data1,"gene");
	print_go_annotation_data(data2);
	
	cout << "# test data3= group_go_annotation_data(data1,\"go\"):\n";
	sort(data1.begin(),data1.end(),compare_go_score);
	vector<vector<go_annotation> > data3= group_go_annotation_data(data1,"go");
	print_go_annotation_data(data3);	
	
	cout << "# test data1_copy= copy(data1)\n";
	vector<go_annotation> data1_copy= copy(data1);
	data1[0].gene= "modified";
	data1[0].go =0;
	cout << "# data1 (modified):\n";
	print_go_annotation_data(data1);
	cout << "# data1_copy:\n";
	print_go_annotation_data(data1_copy);

	cout << "# test pmap= read_goparents("<< gop_file<<"):\n";
	unordered_map<unsigned int,vector<unsigned int> > pmap;
	read_goparents(gop_file,pmap);
	print_goparents(pmap,10);
	
	
	cout << "# test icmap= read_information_content("<< ic_file<<"):\n";
	unordered_map<unsigned int,double >  icmap;
	read_information_content(ic_file,icmap);
	vector<unsigned int> keys;
	for (auto it = icmap.begin(); it != icmap.end(); ++it ){keys.push_back(it->first);}
	sort(keys.begin(),keys.end());
	for(unsigned int i=0; i<keys.size() && i<10; i++)
		printf("%07u\t%1.6f\n",keys[i],icmap[keys[i]]);
		
	
	cout << "# test propagate_parents:\n";
	cout << "# data1:\n";
	data1= data1_copy;
	sort(data1.begin(),data1.end(),compare_gene_score);
	print_go_annotation_data(data1);
	cout << "# propagate_parents(data1):\n";
	propagate_parents(data1,pmap);
	print_go_annotation_data(data1);
	
}	

/*
 * TESTING GENE ONTOLOGY GRAPH METHODS
 */

void test_create_go_graph(const char* obo_file){

	cout << "# test <golib.h>:create_go_graph()\n";
	
	cout << "# read_obo_file_strtok("<<obo_file<<")\n";
	vector<go_node> go_nodes= read_obo_file(obo_file);
	cout << go_nodes.size() << " nodes parsed\n" << flush;
	
	cout << "# create_go_graph()\n";
	go_graph graph= create_go_graph(go_nodes);
	
	
	// GRAPH STATISTICS
	unsigned int obsolete_num=0;
	
	unsigned int isa_par_num=0;
	unsigned int partof_par_num=0;
	unsigned int isa_ch_num=0;
	unsigned int partof_ch_num=0;
	unsigned int regulates_par_num=0;
	unsigned int regulates_ch_num=0;
	unsigned int replacedby_par_num=0;
	unsigned int consider_par_num=0;
	
	vector<unsigned int> ids;
	for (auto it = graph.isa_parents.begin(); it != graph.isa_parents.end(); ++it ){
		isa_par_num += (it->second).size();
	}
	for (auto it = graph.partof_parents.begin(); it != graph.partof_parents.end(); ++it ){
		partof_par_num += (it->second).size();
	}
	for (auto it = graph.regulates_parents.begin(); it != graph.regulates_parents.end(); ++it ){
		regulates_par_num += (it->second).size();
	}
	for (auto it = graph.replacedby_parents.begin(); it != graph.replacedby_parents.end(); ++it ){
		replacedby_par_num += (it->second).size();
	}
	for (auto it = graph.consider_parents.begin(); it != graph.consider_parents.end(); ++it ){
		consider_par_num += (it->second).size();
	}		
	
	for (auto it = graph.isa_children.begin(); it != graph.isa_children.end(); ++it ){
		isa_ch_num += (it->second).size();
	}
	for (auto it = graph.partof_children.begin(); it != graph.partof_children.end(); ++it ){
		partof_ch_num += (it->second).size();
	}
	for (auto it = graph.regulates_children.begin(); it != graph.regulates_children.end(); ++it ){
		regulates_ch_num += (it->second).size();
	}
		
	for (auto it = graph.nodes.begin(); it != graph.nodes.end(); ++it ){
		ids.push_back(it->first);
		if(it->second.is_obsolete)
			obsolete_num++;
	}	
	
	cout<<"NODES:\n";
	cout<<"number\t"<<graph.nodes.size()<<"\n";
	cout<<"obsolete\t"<<obsolete_num<<"\n\n";
	
	cout <<"EDGES:\n";
	cout<<"is_a parent edges:\t"<<isa_par_num<<"\n";
	cout<<"is_a child edges:\t"<<isa_ch_num<<"\n";
	cout<<"part_of parent edges:\t"<<partof_par_num<<"\n";
	cout<<"part_of child edges:\t"<<partof_ch_num<<"\n";
	cout<<"regulates parent edges:\t"<<regulates_par_num<<"\n";
	cout<<"regulates child edges:\t"<<regulates_ch_num<<"\n";
	cout<<"replaced_by edges:\t"<<replacedby_par_num<<"\n";
	cout<<"consider edges:\t"<<consider_par_num<<"\n\n";
	
		
	cout<<"ROOTS:\n";
	cout<<"cellular_component root:\n";
	if( graph.nodes.count(5575)==1){
		print(graph.nodes[5575]);
	}
	else{
		cout<<"none found\n";}
	cout<<"\n";
	
	cout<<"biological_process root:\n";
	if( graph.nodes.count(8150)==1){
		print(graph.nodes[8150]);
	}
	else{
		cout<<"none found\n";}
	cout<<"\n";
	
	cout<<"molecular_function root:\n";
	if( graph.nodes.count(3674)==1){
		print(graph.nodes[3674]);
	}
	else{
		cout<<"none found\n";}
	cout<<"\n";
	
	cout<<"RANDOM NODES:\n";
	for(unsigned int i=0; i<5; i++){
		unsigned int j= rand()%ids.size();
		print(graph.nodes[ids[j]]);
		cout<<"\n";
	}
	cout<<"\n";
}

void test_get_relatives(const char* obo_file,unsigned int query_id, string relations, string direction, unsigned int max_num){
	cout << "# test <golib.h>:get_relatives()\n";
	
	cout << "# reading obo file..\n"<<flush;
	vector<go_node> go_nodes= read_obo_file(obo_file); 
	
	cout << "# creating graph..\n"<<flush;
	go_graph graph= create_go_graph(go_nodes);
	
	cout << "# get_relatives("<<query_id<<","<<relations<<","<<direction<<"):\n"<<flush;
	vector<unsigned int> relatives = get_relatives(graph,query_id,relations,direction,max_num);
	for(unsigned int i=0; i<relatives.size(); i++)
		printf("%07u\n",relatives[i]);
	
}

void test_get_parent_map(const char* obo_file,string relations){
	cout << "# test <golib.h>:get_parent_map()\n";
	
	cout << "# reading obo file..\n"<<flush;
	vector<go_node> go_nodes= read_obo_file(obo_file); 
	
	cout << "# creating graph..\n"<<flush;
	go_graph graph= create_go_graph(go_nodes);
	
	cout << "# get_parent_map..\n"<<flush;
	unordered_map<unsigned int, vector<unsigned int> > pmap= get_parent_map(graph, relations,false,false);
	cout <<"size: "<<pmap.size()<<"\n";
	cout <<"pmap:\n";
	print_goparents(pmap,10);
}


/*
 * TESTS <UTILS.H>
 *
 */
void test_get_prefix(unsigned int nrow=10,unsigned int ncol=10){
	cout << "# test <utils.h>:get_prefix\n";
	
		
	vector<vector<double> > mat(nrow);
	for(unsigned int i=0; i<nrow;i++){
		vector<double> row(ncol,0);
		mat[i]= row;
		
		for(unsigned int j=0; j<ncol; j++)
			mat[i][j]= i*10+j;
	}
	
	cout <<"mat:\n";
	for(unsigned int i=0; i<mat.size(); i++){
		for(unsigned int j=0; j<mat[0].size(); j++)
			printf("%2.0f ",mat[i][j]);
		printf("\n");
	}
	printf("\n");
	
	for(unsigned int k=0; k<mat.size(); k++){
		vector<vector<double> > mat_pref = get_prefix(mat,k+1);
		cout<< "get_prefix(mat,"<<(k+1)<<"):\n";
		for(unsigned int i=0; i<mat_pref.size(); i++){
		for(unsigned int j=0; j<mat_pref[0].size(); j++){
			printf("%2.0f ",mat_pref[i][j]);
		}
		printf("\n");}
	}
	
	vector<double> v= mat[0];
	cout<<"v:\n";
	for(unsigned int i=0; i<v.size(); i++)
		printf("%2f ",v[i]);
	printf("\n\n");
	
	for(unsigned int k=0; k<v.size(); k++){
		vector<double> v_pref= get_prefix(v,k+1);
		cout<< "get_prefix(v,"<<(k+1)<<"):\n";
		for(unsigned int i=0; i<v_pref.size(); i++)
			printf("%2.0f ",v_pref[i]);
		printf("\n");
	}
}

void test_get_values(unsigned int nkeys=10){
	cout<< "# test <utils.h>: get_keys(unordered_map), get_values(unordered_map):\n";
	
	unordered_map<string,unsigned int> map;
	
	char buffer[50];
	cout<<" map:\n";
	for(unsigned int i=0; i<nkeys; i++){
		sprintf(buffer,"gene_%u",i);
		string k= string(buffer);
		unsigned int go= 1000+i;
		
		map[k]= go;
		cout<<k<<" -> "<<go<<"\n";
	}
	
	cout<<"\nget_keys(map):\n";
	vector<string> keys= get_keys(map);
	for(unsigned int i=0; i<keys.size(); i++)
		cout<<keys[i]<<"\n";
		
	cout<<"\nget_values(map):\n";
	vector<unsigned int> values= get_values(map);
	for(unsigned int i=0; i<values.size(); i++)
		cout<<values[i]<<"\n";
}
	
	


void print_usage(const char* name){
	cerr << "USAGE: "<< name <<" -t num [-k num][-l num][-m str][-p goa_file][-g gop_file][-i ic_file][-b obo_file]\n"
		<<"t\ttest number, valid options:\n"
		<<" # tests for <stats.h>\n"
		<<" \t1\ttest_rand -k nbins\n"
		<<" \t2\ttest_rand2()\n"
		<<" \t3\ttest_permute()\n"
		<<" \t4\ttest_hash_key_encoding()\n"
		<<" \t5\ttest_get_auc()\n"
		<<" \t6\ttest_set_operations\n"
		<<" \t7\ttest_get_Jaccard\n"
		<<" \t8\ttest_basic_stats()\n"
		<<" \t9\ttest_matrix_stats()\n"
		<<" # tests for <golib.h>\n"
		<<" \t10\ttest_golib(), requires options\n"
		<<" \t\tp\tgo annotation file, format:<gene\\tgo\\tscore\\n>\n"
		<<" \t\tg\tgo parents file, format:<child_id\\tcomma_sep_parent_list\\n>\n"
		<<" \t\ti\tinformation content file, format:<go\\ticontent\\n>\n"
		<<" # tests for <gograph.h>\n"
		<<" \t11\ttest_create_go_graph(), requires -b obo_file\n"
		<<" \t12\ttest_get_parents(), requires -b obo_file -k id_query [-l max_num] -m isa[,partof]\n"
		<<" \t13\ttest_get_children(), requires -b obo_file -k id_query [-l max_num] -m isa[,partof]\n"
		<<" \t14\ttest_get_parent_map(), requires -b obo_file -m isa[,partof]\n"
		<<" # tests for <utils.h>\n"
		<<" \t15\ttest_get_prefix(), requires -k nrow -l ncol\n"
		<<" \t16\ttest_get_values(), -k nkeys\n";
}

int main(int argc, char** argv) {

	// paramenters
	const char* prog_name= argv[0];
	unsigned int test_num= 0;
	unsigned int k= 0;
	unsigned int l= 10000;
	const char* goa_file= "null";
	const char* gop_file= "null";
	const char* ic_file=  "null";
	const char* obo_file= "null";
	string mode= "null";
	
	int option= 0;
	while ((option = getopt(argc, argv,"t:k:l:m:p:g:i:b:")) != -1) {
        switch (option) {
		case 't' : 
	     		test_num = atoi(optarg);
             		break;
		case 'k':
			k= atoi(optarg);
			break;
		case 'l':
			l= atoi(optarg);
			break;
		case 'm':
			mode= string(optarg);
			break;	
		case 'p':
			goa_file= optarg; 
                	break;
		case 'g' :
	     		gop_file= optarg;
			break;
		case 'i' :
			ic_file= optarg;
			break;
		case 'b':
			obo_file= optarg;
			break;		
		case '?':
        		cerr << "option -"<<optopt<<" requires an argument\n\n";
        		exit(1);
             	default:
	     		print_usage(prog_name); 
                	exit(1);
        	}
    	}

	switch(test_num){
		case 1:
			if(k == 0){
				cerr << "test -t 1 requires -k num\n";
				exit(1);
			}
			test_rand(k);break;
		case 2:
			test_rand2();break;
		case 3:
			test_permute();break;
		case 4:
			test_hash_key_encoding(); break;
		case 5:
			test_get_auc();
			printf("\n");
			test_get_U();
			break;
		case 6:
			test_set_operations();break;
		case 7:
			test_get_Jaccard();break;
		case 8:
			test_basic_stats(); break;
		case 9:
			test_matrix_stats(); break;
		case 10:
			if(strcmp(goa_file,"null")==0 || strcmp(gop_file,"null")==0 || strcmp(ic_file,"null")==0){
				cerr << "test10: missing arguments: -p goa_file -g gop_file -i ic_file\n\n";
				exit(1);
			}
			test_golib(goa_file,gop_file,ic_file);
			break;
		case 11:
			if(strcmp(obo_file,"null")==0){
				cerr << "test11: missing arguments: -b obo_file\n\n";
				exit(1);
			}
			test_create_go_graph(obo_file);
			break;
		case 12:
			if(strcmp(obo_file,"null")==0 || k==0 || mode=="null"){
				cerr<< "test12: missing arguments: -b obo_file -k id_query -m isa[,partof]\n\n";
				exit(1);
			}
			test_get_relatives(obo_file,k,mode,"parents",l);
			break;
		case 13:
			if(strcmp(obo_file,"null")==0 || k==0 || mode=="null"){
				cerr<< "test13: missing arguments: -b obo_file -k id_query -m isa[,partof]\n\n";
				exit(1);
			}
			test_get_relatives(obo_file,k,mode,"children",l);
			break;
		case 14:
			if(strcmp(obo_file,"null")==0 || mode=="null"){
				cerr<< "test14: missing arguments: -b obo_file -m isa[,partof]\n\n";
				exit(1);
			}
			test_get_parent_map(obo_file,mode);
			break;
		case 15:
			test_get_prefix(k,l);
			break;
		case 16:
			test_get_values(k);
			break;		
		default:
			cerr<< "invalid -t num\n";
			print_usage(prog_name);
			exit(1);
	}


}





