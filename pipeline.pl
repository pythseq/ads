#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Long qw(GetOptions);
Getopt::Long::Configure qw(gnu_getopt);
#
#	ARTIFICIAL DILUTION SERIES PIPELINE
#
#	Credits:
#	Ilya Plyusnin (Ilja.Pljusnin@helsinki.fi)
#	Petri Toronen (Petri.Toronen@helsinki.fi)
# 
########################################################################

my $usage=	"USAGE: $0 --resdir dir -r|rmode int -s|srand int --max_ajac num --data str --perms int --obo file --goa file --pipeh str --pipev str[-h]\n".
		"--resdir [dir] : print results to dir (default \"res\")\n".
		"-r|rmode [int] : rmode for elab (default 2)\n".
		"-s|srand [int] : random seed for all random number generators (default 1)\n".
		"--max_ajac [num]: max ancestral jaccard similarity between swapped GO terms in permutation step (default 0.2)\n".
		"-data [str]: comma-separated list of input true-sets (default to Uniprot,CAFA,MouseFunc)\n".
		"--perms  [int]	: number of tested datasets (permutations) at each signal level (default 1000)\n".
		"-obo [file]    : GO obo file\n".
		"-goa [file]    : GOA file\n".
		"--datadir	: folder where generated input data files are stored (default data/) ".
		"-h|help        : print this user manual\n".
		"\n".
		"To run a part of pipeline specify --pipeh and --pipev strings.\n".
		"--pipeh [str]  : horisontal part of the pipeline: parallel elab runs (default All)\n".
		"                This can be (case insensitive):\n".
		"                AUC   : run all AUC metrics and Fmax\n".
		"                Jacc  : run all Jaccard metrics\n".
		"                Smin  : run all Smin-related metrics\n".
		"                Sem   : run all Semantic similarity metrics\n".
		"                All   : run everything\n".
		"                data  : Generate ADS datasets. Exclude the metric runs\n".
		"                e.g. --pipeh AUC,Jacc,Smin\n\n".
		"--pipev [str] : vertical part of the pipeline: non-parallel pipeline steps (default 1:2,4:10)\n".
		"                1     : extract GO parents\n".
		"                2     : extract IC\n".
		"                3     : extract IC2 (time consuming step)\n".
		"                4     : sample negatives + add scores\n".
		"                5     : generate and score AP sets (elab run)\n".
		"                6     : generate FP sets\n".
		"                7     : score FP sets\n".
		"                8     : calc Jacc index\n".
		"                9     : print summary tables\n".
		"                10    : print figures\n\n";

# DEFAULT DATA: EDIT THESE LINES TO POINT TO YOUR DATA OR SPECIFY AS ARGUMENTS
my $data_str	= "data/uniprot.1000.genego,data/cafa2012.genego,data/mouseFunc.genego";
my $obo_file	= "data/go.obo";
my $goa_file	= "data/goa_uniprot_flt.gaf";
my $data_dir	= "data/";

# THESE ARE GENERATED DURING THE RUN
my $gop_file		= "goparents.tab";
my $ic_file		= "ic.tab";
my $ic_file_ext		= "ic.ext.tab";
my $ic2_file		= "ic2.tab";
my $ic2_file_ext	= "ic2.ext.tab";


# PARAMETERS
my $srand	= 1;
my $rmode	= 2;
my $max_ajac	= 0.2;
my $res_root	= "ADS-results";
my $pipeh	= "All";
my $pipev_str	= "1:2,4:10";
my $perm_val	= 1000;
my $help	= !1;
my $call;


# COMMAND LINE OPTIONS
if(scalar(@ARGV) < 1){
	die $usage;
}
GetOptions('resdir=s'	=> \$res_root,
	   'pipeh=s'	=> \$pipeh,
	   'pipev=s'	=> \$pipev_str,
	   's|srand=i'	=> \$srand,
	   'r|rmode=i'	=> \$rmode,
	   'max_ajac=f'	=> \$max_ajac,
	   'data=s'	=> \$data_str,
	   'perms=i'	=> \$perm_val,
	   'obo=s'	=> \$obo_file,
	   'goa=s'	=> \$goa_file,
	   'datadir=s'	=> \$data_dir,
	   'h|help'	=> \$help) or die $usage;   
if($help){
	die $usage;
}

# Add / to folder name, if it is required
# solution from here https://www.perlmonks.org/?node_id=516285

$data_dir =~ s|/?$|/|;

# Add data_dir to names of generated data files

$gop_file		= $data_dir . $gop_file;
$ic_file		= $data_dir . $ic_file;
$ic_file_ext		= $data_dir . $ic_file_ext;
$ic2_file		= "$data_dir$ic2_file";
$ic2_file_ext		= "$data_dir$ic2_file_ext";

system_call("mkdir -p $data_dir");

my %pipev = parse_steps($pipev_str);
my @data_list= split(/,/,$data_str,-1);

# Define names for temporary data files
# These are now directed to datadir directory

my $goa_file_flt = $goa_file . ".flt";
$goa_file_flt =~ s/.*\///;
$goa_file_flt = $data_dir . $goa_file_flt;

my $goa_file_gocounts = $goa_file . ".gocounts";
$goa_file_gocounts =~ s{.*/}{};
$goa_file_gocounts = $data_dir . $goa_file_gocounts;

my $goa_file_ic2 = $goa_file . ".ic2";
$goa_file_ic2 =~ s{.*/}{};
$goa_file_ic2 = $data_dir . $goa_file_ic2;


if($pipev{1}){
	print STDERR "\n# PIPELINE: extract GO parents to $gop_file\n\n";
	system_call("bin/gorelatives -b $obo_file -q all -r isa,partof,consider,replacedby,altid -d parents -v > $gop_file");
}

if($pipev{2}){
	print STDERR "\n# PIPELINE: extract IC to $ic_file_ext\n\n";
	system_call("perl/filter_goa.pl $goa_file $goa_file_flt");
	system_call("perl/GOcounter.pl $goa_file_flt $gop_file > $goa_file_gocounts");
	system_call("cut -f1,4 $goa_file_gocounts > $ic_file");
	system_call("bin/propagate_ic -i $ic_file -o $ic_file_ext -b $obo_file -v");
}

if($pipev{3}){
	print STDERR "\n# PIPELINE: extract IC2 to $ic2_file_ext\n";
	print STDERR "#PIPELINE: This will take many hours!!\n\n";
	system_call("perl perl/get_Smin_InfCont3.pl $obo_file $gop_file $goa_file $goa_file_ic2 > SminIC.log 2>&1");
	system_call("cut -f1,5 $goa_file_ic2 > $ic2_file");
	system_call("bin/propagate_ic -i $ic2_file -o $ic2_file_ext -b $obo_file -v");
}	

system_call("mkdir -p $res_root");
my @ads_list;
my @fps_list;

foreach my $data(@data_list){

	my $label= $data;
	my @tmp= split(/\//,$label); # remove /*/* prefix
	$label = $tmp[$#tmp];
	$label =~ s/\.\w+$//g;	# remove .* suffix
	my $res_dir	= "$res_root/$label";	
	system_call("mkdir -p $res_dir");
	push(@ads_list,"$label");
	push(@fps_list,"$label.fps.scores.tab");	


	if($pipev{4}){
		print STDERR "\n# PIPELINE: sample negative + add scores\n\n";
		system_call("perl/sample_negatives.pl $data $data.neg $gop_file 4 $srand");
		system_call("perl/addscores.pl $data $data.sc 0.5 0.5 $srand");
		system_call("perl/addscores.pl $data.neg $data.neg.sc -0.5 0.5 $srand");
	}
	
	if($pipev{5}){
		print STDERR "\n# PIPELINE: generate + score AP sets (elab runs)\n\n";
		my $gocount_db = `cut -f5 $goa_file | sort -u | wc -l`;
		my $gocount_on = `cat $gop_file | wc -l`;
		chomp($gocount_db);
		chomp($gocount_on);
		print STDERR "GOC_db = $gocount_db\n";
		print STDERR "GOC_on = $gocount_on\n";		
		
		system_call("perl/run_elab_parallel.pl --perm $perm_val --emode 0,1,by=0.1 --rmode $rmode --max_ajac $max_ajac --goc_db $gocount_db --goc_on $gocount_on --srand $srand --pos $data.sc --neg $data.neg.sc --obo $obo_file --ic $ic_file_ext --ic2 $ic2_file_ext  --resdir $res_dir  --pipe $pipeh");
	}
	
	if($pipev{6}){
		print STDERR "\n# PIPELINE: generate FP sets\n\n";
		system_call("perl/get_naive_pred.pl $data $data.naive800 $goa_file.gocounts 1 800");
		system_call("perl/get_smallgo_pred.pl $data $data.smallgo800 $goa_file.gocounts 0 800");
		system_call("perl/get_randgo_pred.pl $data $data.randgo800 $goa_file.gocounts 800 $srand");
	}
	if($pipev{7}){
		print STDERR "\n# PIPELINE: score FP sets\n\n";
		system_call("perl/get_fps_score.pl bin/goscores $data.naive800,$data.smallgo800,$data.randgo800 $res_dir/* 1> $res_root/$label.fps.scores.tab");
	}
	
	if($pipev{8}){
		print STDERR "\n# PIPELINE: calc Jacc index\n\n";
		system_call("bin/elab -k 10 -r $rmode -s $srand -p $data.sc -n $data.neg.sc -b $obo_file -o $res_root/$label.jacc.elab -j $res_root/$label.jacc -e 0,1,by=0.2 -m LIST=go,TH=all,SUMF1=mean,SUMF2=mean,SF=AUC1");
	}
}

if($pipev{9}){
	print STDERR "\n# PIPELINE: print summary tables\n\n";
	system_call( sprintf("Rscript R/print_summary_table2.R $res_root %s %s summary.tab", join(',',@ads_list), join(',',@fps_list)) );
	# Check if perl XLS libraries are installed
	# Might be better to place this check inside the write_excel.pl?
	eval "use Excel::Writer::XLSX";
	if($@){
		print STDERR "\nSkipping excel table printing\nModule not found\n";
		$@ = ""; # Empty the error message from eval. No need to worry users...
	}
	else{
		system_call( "perl/write_excel.pl --headers $res_root/summary.tab  > $res_root/summary.xlsx");
	}
}

if($pipev{10}){
	print STDERR "\n# PIPELINE: print figures\n\n";
	system_call( "Rscript R/print_figures.R $res_root $ads_list[0] eps 1:5");
	system_call( "Rscript R/print_figures.R $res_root $ads_list[0] jpeg 1:5");
}

print STDERR "\n# PIPELINE: all done\n\n";

sub system_call{
	my $call= shift;
	print STDERR "$call\n";
	system($call) == 0 or die "";
}

# PARSES PIPELINE STEPS TO A HASH
sub parse_steps{
	my $pipe_str 	= shift();
	my %pipeh 	= ();

	for(my $i=1; $i<=10; $i++){  $pipeh{$i}= !1; }
	
	my @tmp= split(/,/,$pipe_str);
	
	foreach my $t(@tmp){
	  my @pair= split(/:/,$t);
	  if(scalar @pair > 1){
		if($pair[0] < 1  ||  $pair[1] > 10){
			die "invalid pipeline steps: $t: we only understand steps from 1 to 10\n";
		}
		for(my $i=$pair[0]; $i<=$pair[1]; $i++){
			$pipeh{$i} = 1;
		}
	  }
	  else{
		if($t < 1 || $t > 10){
			die "invalid pipeline steps: $t: we only understand steps from 1 to 10\n";
		}
		$pipeh{$t} = 1;
	  }
	}
	return %pipeh;
}
