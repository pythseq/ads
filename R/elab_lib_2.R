#
# FUNCTIONS FOR READING IN AND MANIPULATING ELAB OUTPUT

# DEV VERSION OF THE CODE

# Following code is for debugging

#source("lls_source_bckup.R")


#############################################
# Two versions of IsThis_Smin below
# 
# These are used to recognize data tables or
# row in data that come from Smin
##########################################

################################################################################
# This version requires that there is "Smin" but NOT "SimUI" and NOT "SimGIC"
################################################################################
IsThis_Smin <- function( strings_in ){

  out = ( grepl("Smin", strings_in) & !grepl("SimUI", strings_in) & !grepl("SimGIC", strings_in) )
  out

}
################################################################################
# This version requires that there is "Smin.Smin" in string
################################################################################
IsThis_Smin2 <- function( strings_in ){

  out = ( grepl("Smin\\.Smin", strings_in) & !grepl("SimUI", strings_in) & !grepl("SimGIC", strings_in) )
  out

}

##########################################################################
# Reads elab output. Mainly used to split erate and edata parts of data.
#
# Old version of read_elab. Does not allow flipping of the sign
###########################################################################

read_elab_old<- function(elab_file){
    
    data<- read.table(elab_file, sep="\t", header=F,as.is=F);
    
    data.erate<- as.vector(as.matrix(data[1,]));
    data.edata<- as.matrix(data[2:nrow(data),]);
    
    l<- list(2);
    l[[1]]<- data.erate;
    l[[2]]<- data.edata;
    l
}

###################################################
# Another version. 
# This flips the sign of Smin data by default. 
#	Behavior can be turned off with FlipSminSign = FALSE
# File name must test positive in IsThis_Smin
####################################################

read_elab<- function(elab_file, FlipSminSign = TRUE){
    
    data<- read.table(elab_file, sep="\t", header=F,as.is=F);
    
    data.erate<- as.vector(as.matrix(data[1,]));
    data.edata<- as.matrix(data[2:nrow(data),]);
    
    if(FlipSminSign && IsThis_Smin(elab_file) ){
       data.edata<- -data.edata
    }
    l<- list(2);
    l[[1]]<- data.erate;
    l[[2]]<- data.edata;
    l
}

#
# Returns matrix of cross correlation between different evaluation measures
#
# elab_files    list of elab files containing NxC evaluation matrices (first row for error rate, rows for perm, cols for erate, value ROC AUC or other evaluation measure)
# labels        labels used to referr different files in the stats matrix
get_crosscor<- function(elab_files, labels, col, prefix="",cor_method="spearman"){
  
  # read ELAB data files
  edata<- list();
  for(i in 1:length(elab_files)){
    temp<- read_elab(paste(prefix,elab_files[[i]],sep=""));
    edata[[i]]<- temp[[2]];
  }
  
  N<- length(edata);
  table<- matrix(0,nrow=N,ncol=N);
  rownames(table)<- labels;
  
  for(i in 1:N){
    for(j in 1:N){
      x<- as.vector(edata[[i]][,col]);
      y<- as.vector(edata[[j]][,col]);
      table[i,j]<- cor(x,y,method=cor_method);
    }
  }
  table
}

# Calculates Score ~ Signal correlation coefficient from a single score-file (ELAB file)
#
# elab_file         file containing results from ELAB run
# meth              correlation method, default is "spearman"
#
get_score2signal_corr<- function(elab_file,meth="spearman"){
    # read ELAB scores
    temp<- read_elab(elab_file);
    erate<- temp[[1]];
    edata<- temp[[2]];
    
    scores<- as.vector(t(edata)) # scores from evaluation metric
    signal<- 1-rep(erate,nrow(edata))
    c<- cor(signal,scores,method=meth);
    c
}

# Calculates Spearman's correlation coefficients between signal levels (1-error) and evaluation scores
# for a batch of ELAB files.
# 
# 
# prefix            path to the directory where subdirectories are
#                   
# subdir_list       list of subdirectories
#                   each subdirectory represents a set of score-files, f.e. score for different data sets
# [file_list]       this is set to all files in the first subdirectory on the list
#
# meth              correlations method, default is "spearman"
#                   
get_score2signal_corr_batch<- function(prefix,subdir_list, meth="spearman"){
    
    # PT modified. Added test for the existence of the files
    # If file not available return NA
    col_names<- subdir_list;
    file_list<- list.files(path=paste(prefix,subdir_list[1],sep="/"));
    row_names<- gsub("batch.","",as.list(file_list),perl=T);
    row_names<- gsub(".NULL","",row_names,perl=T);
    row_names<- gsub(".txt","",row_names,perl=T);
    row_names<- gsub(".elab","",row_names,perl=T);
    #row_names<- gsub("_"," ",row_names,perl=T);
    
    table<- matrix(0,nrow=length(file_list),ncol=length(subdir_list));
    rownames(table)<- as.vector(row_names);
    colnames(table)<- as.vector(col_names);
    
    for(di in 1:length(subdir_list)){
        for(fi in 1:length(file_list)){
	    file_string = paste(prefix,subdir_list[di],file_list[fi],sep="/")
	    if( file.exists(file_string)){	# PT added test
               # read ELAB scores
               temp<- read_elab(file_string);
               erate<- temp[[1]];
               edata<- temp[[2]];
            
               signal_levels<- 1-rep(erate,nrow(edata))
               scores<-  as.vector(t(edata))
            
               table[fi,di]<- cor(signal_levels,scores,method=meth);
            } else {
	       table[fi,di]<- NA
	    } 
        }
    }
    table
}




GetCorr <- function( ErrRate, DataIn, DoTranspose = TRUE, ...){
    
    # Ripped from Ilja's code
    #
    # I added DoTranspose parameter. 
    # It ensures that two generated vectors match to each other
    # Do NOT turn DoTranspose off, unless you know what you are doing
    
    signal_levels <- 1-rep(ErrRate,nrow(DataIn))
    if(DoTranspose){
        DataIn = t(DataIn)
    }
    scores        <- as.vector(DataIn)
    out           <- cor(signal_levels,scores, ...)
    out
}

ErrorWeigtedSum <- function( Values, Errors){
    
    # This function estimates the signal data point
    # based on signals for two (nearest) points
    # Error here is the distance to these points
    #
    # Sum of two values. Weighted by the error
    out = Values[1] + ( Errors[1]/(Errors[1]+Errors[2]))*(Values[2] - Values[1])
    out
}

GetFPD_signal <- function(ADS_Data, ADS_signals, FPD_scores, method = "median"){
    # Create score profile for each column in ADS data
    if(method == "median"){
        ADS_profile = apply(ADS_Data, 2, median)
    }
    else{ 
        if(method == "mean"){
            ADS_profile = apply(ADS_Data, 2, mean) 
        }
        else{
            print("Only methods median and mean currently available")
        }
    }
    output = rep(0, length(FPD_scores))
    FPD_scores = as.numeric(FPD_scores)
    for(k in 1:length( FPD_scores )){
        if( FPD_scores[k] > max( ADS_profile )){
            output[k] = max(ADS_signals)
        }
        else if( FPD_scores[k] < min( ADS_profile )){
            output[k] = min(ADS_signals)
            
        }  
        else{
            err_prof = abs(ADS_profile - FPD_scores[k]) 
            Two_inds = order(err_prof)[1:2]
            output[k] = ErrorWeigtedSum(ADS_signals[Two_inds], err_prof[Two_inds])
        }   
    }  
    output
    
}


  
