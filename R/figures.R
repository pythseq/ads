# 
# CODE FOR PLOTTING
#

##################################################
# REquired sub-function here
# This code defines the source code folder path
##################################################
thisFile <- function(removeFileName = TRUE) {
        # Code copied from Stack O-F
        # https://stackoverflow.com/.../rscript-determine-path-of-the-executing-script
        # solution by Steamer25
        cmdArgs <- commandArgs(trailingOnly = FALSE)
        needle <- "--file="
        match <- grep(needle, cmdArgs)
        if (length(match) > 0) {
                # Rscript
                out = normalizePath(sub(needle, "", cmdArgs[match]))
        } else {
                # 'source'd via R console
                out = normalizePath(sys.frames()[[1]]$ofile)
        }
        # remove file name from the end
        if(removeFileName){
                out = gsub("[^/]+$", "", out)
        }       
        return( out )
}

####################
# Main Code starts #
####################

# Following code allows sourcing other files 
code_folder = thisFile()        # This is where the code is
source(paste(code_folder,"ScatterWithLabels.R", sep = ""))
source(paste(code_folder,"elab_lib_2.R", sep = ""))
# 
# Generating a panel of boxplots from ADS and FPS scores
#
# input
# elab_list   : list of elab files as returned by read_elab
# fps_scores  : table of false positive scores in same order as the elab_list
# labels      : metric labels
# coln        : number of cols in the panel plot
# rown        : number of rows in the panel plot
# col         : list of colors used for FPS score lines
# lty         : list of line types for FPS score lines


plot_boxplot<- function(elab_list,fps_scores,labels,coln,rown,byrow=T,col=c("#FF0000","#00FF00","#0000FF"),lwd=1,lty=c(1,2,3)){
    
    par(mar=c(2.5,2.5,2.5,2.5));
    if(byrow){
        par(mfrow=c(rown,coln));
    }
    else{
        par(mfcol=c(rown,coln));}
    
    for(i in 1:length(elab_list)){
	    tmp_flat<- convert_to_flat(elab_list[[i]][[2]])
	    colnames(tmp_flat)<- c("rowname","noise","score");    
        plot(tmp_flat$noise,tmp_flat$score,main=labels[i],xlab="",lwd=lwd)
        for(j in 1:dim(fps_scores)[2]){
            abline(h=fps_scores[i,j], col=col[j], lty =lty[j]);
        }
    }
}
# Modified from plot_boxplot
# This code uses my DoBoxPlotAndLinesWithValues
# Code adds Rank Correlation and FP score to plot
#
plot_boxplot_new <- function(elab_list,fps_scores,labels,coln,rown,byrow=T,col=c("#FF0000","#00FF00","#0000FF"),lwd=1,lty=c(1,2,3)){
    
    #par(mar=c(2.5,2.5,2.5,2.5));
    #par(mar=c(2, 2, 2, 2))
    par(mar=c(2, 2, 1.5, 0.8))
    if(byrow){
        par(mfrow=c(rown,coln));
    }
    else{
        par(mfcol=c(rown,coln));
    }
    for(i in 1:length(elab_list)){
        if( grepl("Smin", labels[i])){
          DoBoxPlotAndLinesWithValues(elab_list[[i]][[2]], elab_list[[i]][[1]], as.numeric(fps_scores[i,]),
	                              main=labels[i], WriteLegend = FALSE, BordersOnlyFromADS = TRUE)
	}
	else{
	  DoBoxPlotAndLinesWithValues(elab_list[[i]][[2]], elab_list[[i]][[1]], as.numeric(fps_scores[i,]),
	                              main=labels[i], WriteLegend = FALSE)	
	}
    }
}

#
# Like plot_boxplot2 but no labels for individual plots and common labels for rows and cols
# PT Modified:
#	Uses DoBoxPlotAndLinesWithValues function
#	Added Smin_Ind to input. True/False vector. 
#	Smin_Ind is True when input is Smin style function

plot_boxplot2_new<- function(elab_list,fps_scores,rown,coln,row_labels,col_labels, Smin_Ind,
                         col=c("#FF0000","#00FF00","#0000FF"),lty=c(1,2,3), lwd=1){
    
    # :order of plotting
    m<- matrix( seq(rown+coln+1,rown+coln+rown*coln,1), ncol=coln,byrow=F);
    m<- rbind( seq(1,coln,1),m)
    m<- cbind( c(0,seq(coln+1,coln+rown)), m);
    # Re-adjusting text box sizes
    layout( mat=m, heights=c(0.28,rep(3,rown)),widths=c(0.4,3,3,3));
 
    # plot pars
    # Re-adjusting margins below
    #par(mar=c(0,2.5,1,1));    
    par(mar=c(0,0,0,0.2))
    for(i in 1:coln){ plot.new(); text(0.5,0.5,col_labels[i],font=2,cex=1.5);} # column titles
    for(i in 1:rown){ plot.new(); text(0.5,0.5,row_labels[i],font=2,cex=1.5, srt=90);} # row titles
    
    
    # Re-adjusting margins below
    #par(mar=c(2.5,2.5,1,1))
    par(mar=c(1.7,1.7,0.5,0.5))
    for(i in 1:length(elab_list)){
        #Borders are defined differently for Smin plots
	if( Smin_Ind[i]){
          DoBoxPlotAndLinesWithValues(elab_list[[i]][[2]], elab_list[[i]][[1]], as.numeric(fps_scores[i,]),
	                              WriteLegend = FALSE, BordersOnlyFromADS = TRUE)
	}
	else{
	  DoBoxPlotAndLinesWithValues(elab_list[[i]][[2]], elab_list[[i]][[1]], as.numeric(fps_scores[i,]),
	                              WriteLegend = FALSE)	
	}   
	 
    }
}


#
# Like plot_boxplot2 but no labels for individual plots and common labels for rows and cols
#
plot_boxplot2 <- function(elab_list,fps_scores,rown,coln,row_labels,col_labels,
                                col=c("#FF0000","#00FF00","#0000FF"),lty=c(1,2,3), lwd=1){
    
    # :order of plotting
    m<- matrix( seq(rown+coln+1,rown+coln+rown*coln,1), ncol=coln,byrow=F);
    m<- rbind( seq(1,coln,1),m)
    m<- cbind( c(0,seq(coln+1,coln+rown)), m);
    layout( mat=m, heights=c(1,rep(3,rown)),widths=c(1.5,3,3,3));
 
    # plot pars
    par(mar=c(0,2.5,1,1));    
    for(i in 1:coln){ plot.new(); text(0.5,0.5,col_labels[i],font=2,cex=1.5);} # column titles
    for(i in 1:rown){ plot.new(); text(0.5,0.5,row_labels[i],font=2,cex=1.5, srt=90);} # row titles
    
    
    par(mar=c(2.5,2.5,1,1))
    for(i in 1:length(elab_list)){
	    tmp_flat<- convert_to_flat(elab_list[[i]][[2]])
	    colnames(tmp_flat)<- c("rowname","noise","score");    
        plot(tmp_flat$noise,tmp_flat$score, lwd=lwd)
        for(j in 1:dim(fps_scores)[2]){
            abline(h=fps_scores[i,j], col=col[j], lty =lty[j]);
        }
    }
}


#
# Generates default figures for the article
#
# elab_list      : list of elab files as returned by read_elab (list of N elab files, where N is number of metrics)
# fps_scores     : table of false positive scores in same order as the elab_list (this is N x 3 matrix, where N is number of metrics)
# RC             : RC values for evaluated metrics (from summary.txt table)
# FPSsignal      : FPS signal values for evaluated metrics (from summary.txt table)
# format [eps]   : jpeg/tiff/eps
# figlist [1:5]  : indices of figures to generate:
#                1: boxplot1: commong metrics
#                2: boxplot2: AUC
#                3: boxplot3: semantic similarity
#                4: boxplot4: Smin and SimGIC
#                5: FPSsignal versus RC label plot
#
# width [5.2]     : width in inches (Plos values: 5.2: text column width, 7.5: full page width)
# res [300]       : ppi resolution
# pointsize [9]   : text size
# lwd [0.6]       : line width for boxplots
# tmp_name [""]   : Seed-name for output files (if ABC then outputs ABC_boxplot1.eps, ABC_boxplot2.eps...)

generate_all_figures<- function(elab_list, fps_scores, FPSsignal, RC, format="eps", tmp_name = "",
                                figlist=1:5, width=5.2, res=300, pointsize=9, lwd=0.6, cex=1.0){
    
    mlabels<- get_labels(metrics);
    
    if(is.element(1,figlist)){
    # BOXPLOT1: clear weaknesses in existing methods: US AUC3b, Fmax, Smin.ic2, Resnik A, Resnik D, Lin D
    
    ind<- c(grep("US AUCROC",mlabels), grep("Fmax",mlabels), grep("ic2 Smin2",mlabels), grep("Resnik A",mlabels),grep("Resnik D",mlabels),grep("Lin D",mlabels));
    
    if( nchar(tmp_name) > 0){
      tmp_name = paste(tmp_name, '_', sep = "")
    }
    use_name = paste(tmp_name, "boxplot1.", format, sep = "")
    if(format == "jpeg"){
        jpeg(filename= use_name, width=width*res, height=width*res*(2/3), units="px", res=300,pointsize=pointsize);    
        #jpeg(filename= "boxplot1.jpeg", width=width*res, height=width*res*(2/3), units="px", res=300,pointsize=pointsize);
    }
    else if(format == "tiff"){
        tiff(filename= use_name, width=width*res, height=width*res*(2/3), units="px", res=300,pointsize=pointsize);    
        #tiff(filename= "boxplot1.tiff", width=width*res, height=width*res*(2/3), units="px", res=300,pointsize=pointsize);
    }
    else if(format == "eps"){
        postscript(file =use_name, width=width, height=width*(2/3), paper="special",
                   family="Times",pointsize=pointsize,horizontal=F);

    }
    else if(format == "pdf"){
        pdf(file =use_name, width=width, height=width*(2/3), paper="special",
            family="Times",pointsize=pointsize);

    }
    plot_boxplot_new(elab_list =elab_list[ind], fps_scores =fps_scores[ind,],
                    labels=get_labels_short(metrics)[ind],coln=3,rown=2,byrow = F,lwd = lwd);
    dev.off()
    #embedFonts(file="boxplot1.eps",outfile="boxplot1b.eps"); # requires GhostScript
    }

    if(is.element(1,figlist)){
    # BOXPLOT1B: Striking image in case PLOS reviewers decide to accept this ... :-O
    
    ind<- c(grep("US AUCROC",mlabels), grep("Fmax",mlabels), grep("ic2 Smin2",mlabels), grep("Resnik A",mlabels) );
    
    use_name = paste(tmp_name, "boxplot1b.", format, sep = "")
    if(format == "jpeg"){
        jpeg(filename= use_name, width=width*res, height=width*res*(2/3), units="px", res=300,pointsize=pointsize);    
        #jpeg(filename= "boxplot1.jpeg", width=width*res, height=width*res*(2/3), units="px", res=300,pointsize=pointsize);
    }
    else if(format == "tiff"){
        tiff(filename= use_name, width=width*res, height=width*res*(2/3), units="px", res=300,pointsize=pointsize);    
        #tiff(filename= "boxplot1.tiff", width=width*res, height=width*res*(2/3), units="px", res=300,pointsize=pointsize);
    }
    else if(format == "eps"){
        postscript(file =use_name, width=width, height=width*(2/3), paper="special",
                   family="Times",pointsize=pointsize,horizontal=F);

    }
    else if(format == "pdf"){
        pdf(file =use_name, width=width, height=width*(2/3), paper="special",
            family="Times",pointsize=pointsize);

    }
    plot_boxplot_new(elab_list =elab_list[ind], fps_scores =fps_scores[ind,],
                    labels=get_labels_short(metrics)[ind],coln=2,rown=2,byrow = F,lwd = lwd);
    dev.off()
    #embedFonts(file="boxplot1.eps",outfile="boxplot1b.eps"); # requires GhostScript
    }
    
    if(is.element(2,figlist)){
    # BOXPLOT2: comparing AUC metrics: US AUC3b, GC AUC3b, TC AUC3, 
    ind<- c(grep("US AUCROC",mlabels),grep("GC AUCROC",mlabels),grep("TC AUCROC",mlabels),
            grep("US AUCPR$",mlabels),grep("GC AUCPR$",mlabels),grep("TC AUCPR$",mlabels));
    
    use_name = paste(tmp_name, "boxplot2.", format, sep = "")
    if(format == "jpeg"){
        jpeg(filename= use_name, width=width*res, height=width*res*(2/3), units="px", res=300,pointsize=pointsize);
        #jpeg(filename= "boxplot2.jpeg", width=width*res, height=width*res*(2/3), units="px", res=300,pointsize=pointsize);
    }
    else if(format == "tiff"){
        tiff(filename= use_name, width=width*res, height=width*res*(2/3), units="px", res=300,pointsize=pointsize);
        #tiff(filename= "boxplot2.tiff", width=width*res, height=width*res*(2/3), units="px", res=300,pointsize=pointsize);
    }
    else if(format == "eps"){
        postscript(file =use_name, width=width, height=width*(2/3), paper="special",
            family="Times",pointsize=pointsize,horizontal=F);
        #postscript(file ="boxplot2.eps", width=width, height=width*(2/3), paper="special",
        #    family="Times",pointsize=pointsize,horizontal=F);
    }    
    else if(format == "pdf"){
        pdf(file =use_name, width=width, height=width*(2/3), paper="special",
            family="Times",pointsize=pointsize);
    }    
    plot_boxplot_new(elab_list=elab_list[ind],fps_scores[ind,],
                     labels=get_labels_short(metrics)[ind],coln=3,rown=2,lwd = lwd);
    dev.off();
    }
    

    if(is.element(3,figlist)){
    # BOXPLOT3: comparing sematic metrics
    #           This contains all methods
    #           This is quite crowded plot
    ind<- c(grep("Lin",metrics),grep("AJacc",metrics),grep("Resnik",metrics));
    col_labels<- c("Lin","AJacc","Resnik");
    row_labels<- c("Method A","Method B","Method C","Method D","Method E","Method F");
    use_name = paste(tmp_name, "boxplot3.", format, sep = "")
    if(format == "jpeg"){
        jpeg(filename= use_name, width=width*res, height=min(width*(3/2)*res,2625), units="px",res=300,pointsize=pointsize);
    }
    else if(format == "tiff"){
        tiff(filename= use_name, width=width*res, height=min(width*(3/2)*res,2625), units="px",res=300,pointsize=pointsize);
    }
    else if(format == "eps"){
        postscript(file=use_name, width=width, height=min(width*(3/2),8.75), paper="special",
                   family="Times",pointsize=pointsize,horizontal=F);
    }   
    else if(format == "pdf"){
        pdf(file=use_name, width=width, height=min(width*(3/2),8.75), paper="special",
            family="Times",pointsize=pointsize);
    }
    # New version of boxplot2 requires Smin_Ind vector 
    # This is a True/False vector 
    plot_boxplot2_new(elab_list=elab_list[ind], fps_scores=fps_scores[ind,], rown=6,coln=3,
                      row_labels=row_labels,col_labels=col_labels, rep(FALSE, length(ind)), lwd=lwd);
    dev.off()
    }

    if(is.element(3,figlist)){
    # BOXPLOT3 B: comparing sematic metrics
    #             I exclude method F in this plot 
    #             F is very similar to our method E
    ind<- c(grep("Lin[^F]+$",metrics),grep("AJacc[^F]+$",metrics),
            grep("Resnik[^F]+$",metrics))
    col_labels<- c("Lin","AJacc","Resnik")
    row_labels<- c("Method A","Method B","Method C","Method D","Method E");
    use_name = paste(tmp_name, "boxplot3b.", format, sep = "")
    if(format == "jpeg"){
        jpeg(filename= use_name, width=width*res, height=min(width*(3/2)*res,2625), units="px",res=300,pointsize=pointsize);
    }
    else if(format == "tiff"){
        tiff(filename= use_name, width=width*res, height=min(width*(3/2)*res,2625), units="px",res=300,pointsize=pointsize);
    }
    else if(format == "eps"){
        postscript(file=use_name, width=width, height=min(width*(3/2),8.75), paper="special",
                   family="Times",pointsize=pointsize,horizontal=F);
    }   
    else if(format == "pdf"){
        pdf(file=use_name, width=width, height=min(width*(3/2),8.75), paper="special",
            family="Times",pointsize=pointsize);
    }
    # New version of boxplot2 requires Smin_Ind vector 
    # This is a True/False vector 
    plot_boxplot2_new(elab_list=elab_list[ind], fps_scores=fps_scores[ind,], rown=5,coln=3,
                      row_labels=row_labels,col_labels=col_labels, rep(FALSE, length(ind)), lwd=lwd);

    dev.off()
    }

    if(is.element(3,figlist)){
    # BOXPLOT3 C: comparing sematic metrics
    #             I exclude method B and C in this plot 
    #             B and C are our methods, intentionally made to fail
    #             
    ind<- c(grep("Lin[^BC]+$",metrics),grep("AJacc[^BC]+$",metrics),
            grep("Resnik[^BC]+$",metrics))
    col_labels<- c("Lin","AJacc","Resnik")
    row_labels<- c("Method A","Method D","Method E","Method F");
    use_name = paste(tmp_name, "boxplot3c.", format, sep = "")
    if(format == "jpeg"){
        jpeg(filename= use_name, width=width*res, height=min(width*(3/2)*res,2625), units="px",res=300,pointsize=pointsize);
    }
    else if(format == "tiff"){
        tiff(filename= use_name, width=width*res, height=min(width*(3/2)*res,2625), units="px",res=300,pointsize=pointsize);
    }
    else{
        postscript(file=use_name, width=width, height=min(width*(3/2),8.75), paper="special",
                   family="Times",pointsize=pointsize,horizontal=F);
    }   
    # New version of boxplot2 requires Smin_Ind vector 
    # This is a True/False vector 
    plot_boxplot2_new(elab_list=elab_list[ind], fps_scores=fps_scores[ind,], rown=4,coln=3,
                      row_labels=row_labels,col_labels=col_labels, rep(FALSE, length(ind)), lwd=lwd);

    dev.off()
    }

    if(is.element(4,figlist)){
    # BOXPLOT4: comparing Smin metrics
    ind<- grep("smin",metrics,ignore.case = T);
    use_name = paste(tmp_name, "boxplot4.", format, sep = "")
    if(format == "jpeg"){
        jpeg(filename= use_name, width=width*res, height=width*(3/4)*res, units="px",res=300,pointsize=pointsize);
    }
    else if(format == "tiff"){
        tiff(filename= use_name, width=width*res, height=width*(3/4)*res, units="px",res=300,pointsize=pointsize);
    }
    else if(format == "eps"){
        postscript(file=use_name, width=width, height=width*(3/4), paper="special",
                   family="Times",pointsize=pointsize,horizontal=F);
    }
    else if(format == "pdf"){
        pdf(file=use_name, width=width, height=width*(3/4), paper="special",
            family="Times",pointsize=pointsize);
    }
    plot_boxplot_new(elab_list= elab_list[ind], fps_scores = fps_scores[ind,],
                     labels=get_labels(metrics)[ind],coln=3,rown=3,byrow = F,lwd=lwd);
    dev.off()
    }
    
    if(is.element(4,figlist)){
    # BOXPLOT4.B: Comparing structured and unstructured Smin and SimGIC metrics
    ind<- c(grep("ic2 Smin1",mlabels),grep("ic2 Smin2",mlabels),
            grep("ic2 SimGIC$",mlabels), grep("ic2 SimGIC2$",mlabels),
            grep("US Jacc$",mlabels), grep("GC Jacc$",mlabels));
    use_name = paste(tmp_name, "boxplot4b.", format, sep = "");
    if(format == "jpeg"){
        jpeg(filename= use_name, width=width*res, height=width*(3/4)*res, units="px",res=300,pointsize=pointsize);
    }
    else if(format == "tiff"){
        tiff(filename= use_name, width=width*res, height=width*(3/4)*res, units="px",res=300,pointsize=pointsize);
    }
    else if(format == "eps"){
        postscript(file=use_name, width=width, height=width*(3/4), paper="special",
                   family="Times",pointsize=pointsize,horizontal=F);
    }
    else if(format == "pdf"){
        postscript(file=use_name, width=width, height=width*(3/4), paper="special",
                   family="Times",pointsize=pointsize);
    }
    plot_boxplot_new(elab_list= elab_list[ind], fps_scores = fps_scores[ind,],
                     labels=get_labels_short(metrics)[ind],coln=3,rown=2,byrow = F,lwd=lwd);
    dev.off()
    }
    
    
    if(is.element(5,figlist)){
    # Scatterplot RC x FPSsignal, two panels, with bottom panel zoomed on the top left corner

    use_name = paste(tmp_name, "scattered_labels.",  format, sep = "")
    #font_sz = (width/11)*0.8    
    if(format == "jpeg"){
        jpeg(filename= use_name, width=width*res, height=width*1.5*res, units="px",res=300);
    }
    else if(format == "tiff"){
        tiff(filename= use_name, width=width*res, height=width*1.5*res, units="px",res=300);
    }
    else if(format == "eps"){
        postscript(file=use_name, width=width, height= width*1.5, 
	           paper="special", family="Times",horizontal=F);
    }
    else if(format == "pdf"){
        pdf(file=use_name, width=width, height= width*1.5, 
	    paper="special", family="Times")
    }
    par(mfrow=c(2,1));
    par(mar=c(2.5, 4, 2, 2) + 0.1);
    Scatter_With_labels2( FPSsignal, RC, LabelTexts = get_labels(metrics), cex = 0.5);
    par(mar=c(4, 4, 0, 2) + 0.1);
    Scatter_With_labels2( FPSsignal, RC, LabelTexts = get_labels(metrics), cex = 0.6, ZoomIn=TRUE);
    
    par(mar=c(5,4,4,2)+0.1);
    dev.off();
    
    }
}

#
# This is a wrapper function that reads in ADS results from disk and calls generate_all_figures()
#
# input:
# res_root      : root directory for ADS output, e.g.  "res-2017-11-r2"
# res_dataset   : dataset in root directory for which figures will be generated, e.g. "uniprot.1000"
# other         : see generate_all_figures()
#
# e.g.: generate_all_figures2(res_root="res-2017-r2",res_dataset="uniprot.1000",figlist=1:4)
#
# PT Modified: 
generate_all_figures2<- function(res_root,res_dataset,
                        format="eps",figlist=1:8, width=5.2, res=300, pointsize=9, lwd=0.6, cex = 1){
    
        # READING ADS SCORES
    file_list<- sprintf("%s.elab",gsub(" ","_",metrics,perl=T));
    elab_list<- list();
    for(fi in 1:length(file_list)){
        elab_list[[fi]]<- read_elab(sprintf("%s/%s/%s",res_root,res_dataset,file_list[fi]) );
    }
    signal_levels<- 1-elab_list[[1]][[1]];
    signal_levels<- round(signal_levels,digits=3)
    for(i in 1:length(elab_list)){
          colnames(elab_list[[i]][[2]])<- signal_levels;
    }
    
        # READING FPS SCORES
    fps_scores<- read.table(file=sprintf("%s/%s.fps.scores.tab",res_root,res_dataset),header=T,row.names=1,sep="\t");
    fps_scores<- fps_scores[metrics,];
    tmp<- grep("smin[1-9]",metrics,ignore.case = T);
    fps_scores[tmp,]<- -fps_scores[tmp,];
    
        # FPSsignal & RC
    summary = read.table(file=sprintf("%s/summary.tab",res_root), sep="\t", header=TRUE, row.names=1)
    
    RC_col<- intersect( grep("RC",colnames(summary)), grep(res_dataset,colnames(summary)) );
    FPS_col<- intersect( grep("FPS",colnames(summary)), grep(res_dataset,colnames(summary)) );
    RC<-            summary[,RC_col];
    FPSsignal<-     summary[,FPS_col];
    tmp<- getwd();
    setwd(res_root);
    generate_all_figures(elab_list=elab_list, fps_scores=fps_scores, FPSsignal=FPSsignal, RC=RC, tmp_name=res_dataset,
                        format=format,figlist=figlist,width=width,res=res,pointsize=pointsize,lwd=lwd, cex=cex);
    setwd(tmp);
}

GetBorders <- function( DataIn, AddExtra = 0.05) {
    MinVal <- min(DataIn)
    MaxVal <- max(DataIn)
    RangeVal <- MaxVal - MinVal
    MinVal <- MinVal - AddExtra*RangeVal
    MaxVal <- MaxVal + AddExtra*RangeVal
    output <- c(MinVal, MaxVal)
    output
}

DoBoxPlotAndLinesWithValues <- function( DataIn, NoiseLevels, FPD_scores, decimalNumber = 3, LegendValues = c(),
                                         LineThickness=1.5, WriteLegend = TRUE, AddLegend=TRUE, BordersOnlyFromADS= FALSE, 
			                 AddFPD_to_plot = TRUE, Legend_cex=1.1, ...){

  # PT: I modified this code from other DoBoxPlotAndLines 
  # so that it adds selected two values to plot
  # These would be rank correlation and False Positive Data 
  # Added also the option that they would be calculated inside this function (if not given)
  #
  # TODO: legend plot wastes space outside text
  
  if(length(LegendValues) == 0){ 
     FPD_value = GetFPD_signal(DataIn, 1 - NoiseLevels, FPD_scores)
     signal<- 1-matrix( rep(NoiseLevels,nrow(DataIn)), nrow(DataIn), 
                        length(NoiseLevels), byrow = TRUE)
     CorValue  = cor(c(signal),c(DataIn),method="spearman")
  }
  else{
     CorValue  = LegendValues[1]
     FPD_value = LegendValues[2]
  }
  # round values for printing
  CorValue  = round(CorValue, 3)
  FPD_value = round(FPD_value, 3)
  row_names_in = as.character( round(1 - NoiseLevels, digits= decimalNumber), length = decimalNumber)
  
  if( BordersOnlyFromADS ){
    Y_limits = GetBorders(c(min(DataIn), max(DataIn)))
  }
  else{
    Y_limits = GetBorders(c(min(DataIn), max(DataIn), FPD_scores))
  }
  if(WriteLegend){
    boxplot(DataIn, names = row_names_in, xlab = "ADS signal level", ylim = Y_limits, ...)
  }
  else{
    boxplot(DataIn, names = row_names_in, ylim = Y_limits, ...)
  }
  X_coords = par("usr")
  #ls()
  if( AddFPD_to_plot){
    col_vector = c("red","green","blue", "black","magenta")
    for( k in 1:length(FPD_scores)){
       lines( X_coords[1:2], c(FPD_scores[k], FPD_scores[k]), lwd = LineThickness, col = col_vector[k], lty = k+1)
    }
  }
  if( AddLegend ){
    legend("bottomleft", paste(c("RC","FPS"),c(CorValue,max(FPD_value)),sep="="),
            bg = "white", x.intersp=0, y.intersp=0.85,cex= Legend_cex)
  } 
}


DoBoxPlotAndLines <- function( DataIn, NoiseLevels, FPD_scores, decimalNumber = 3,
                               LineThickness=1.5, WriteLegend = TRUE, BordersOnlyFromADS= FALSE, 
			       AddFPD_to_plot = TRUE, ...){

  row_names_in = as.character( round(1 - NoiseLevels, digits= decimalNumber), length = decimalNumber)
  
  if( BordersOnlyFromADS ){
    Y_limits = GetBorders(c(min(DataIn), max(DataIn)))
  }
  else{
    Y_limits = GetBorders(c(min(DataIn), max(DataIn), FPD_scores))
  }
  if(WriteLegend){
    boxplot(DataIn, names = row_names_in, xlab = "ADS signal level", ylim = Y_limits, ...)
  }
  else{
    boxplot(DataIn, names = row_names_in, ylim = Y_limits, ...)
  }
  X_coords = par("usr")
  #ls()
  if( AddFPD_to_plot){
    col_vector = c("red","green","blue", "black","magenta")
    for( k in 1:length(FPD_scores)){
       lines( X_coords[1:2], c(FPD_scores[k], FPD_scores[k]), lwd = LineThickness, col = col_vector[k], lty = k+1)
    }
  } 
}

