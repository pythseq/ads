#
# UTILITY FUNCTIONS
# 

# list of metrics in the right order
metrics<-   c("US_AUCROC",
              "GC_AUCROC",
              "TC_AUCROC",
            "US_AUCPR",
            "GC_AUCPR",
            "TC_AUCPR",
            "Fmax",
	      
            "Smin.ic.Smin1",
            "Smin.ic.Smin2",
            "Smin.ic.SimGIC",
            "Smin.ic.SimGIC2",           

            "Smin.ic2.Smin1",
            "Smin.ic2.Smin2",
            "Smin.ic2.SimGIC",
            "Smin.ic2.SimGIC2", 
                  
            "max_US_Jacc",
            "max_GC_Jacc",
            
            "max_Resnik_score_A",
            "max_Resnik_score_B",
            "max_Resnik_score_C",
            "max_Resnik_score_D",
            "max_Resnik_score_E",
            "max_Resnik_score_F",  
            
            "max_Lin_score_A",
            "max_Lin_score_B",
            "max_Lin_score_C",
            "max_Lin_score_D",
            "max_Lin_score_E",
            "max_Lin_score_F", 
            
            "max_AJacc_score_A",
            "max_AJacc_score_B",
            "max_AJacc_score_C",
            "max_AJacc_score_D",
            "max_AJacc_score_E",
            "max_AJacc_score_F");

metrics_f<- factor(metrics, levels = unique(metrics));

# Converts metric names to their labels for plotting
# input:   vector of metrics
# output:  vector of labels
get_labels<- function(str_in){
    labels_out<- gsub("^max_","",str_in,perl=T);
    labels_out<- gsub("_"," ",labels_out,perl=T);
    labels_out<- gsub("^Smin.","",labels_out,perl=T);
    labels_out<- gsub("\\.ic","\\.IC",labels_out,perl=T);
    labels_out<- gsub("\\."," ",labels_out,perl=T);
    labels_out<- gsub("score ","",labels_out,perl=T);
    gsub("  ", " ", labels_out);

    labels_out
}

get_labels_short<- function(str_in){
    labels_out <- get_labels(str_in);
    labels_out<- gsub("ic2 ","",labels_out,perl=T,ignore.case =T);
    labels_out
}

get_labels_oneletter<- function(str_in){
    labels_out<- get_labels(str_in);
    labels_out<- gsub("Resnik","R",labels_out,perl=T,ignore.case = T);
    labels_out<- gsub("Lin","L",labels_out,perl=T,ignore.case = T);
    labels_out<- gsub("AJacc","AJ",labels_out,perl=T,ignore.case = T);
    labels_out<- gsub("SimGIC","G",labels_out,perl=T,ignore.case = T);
    labels_out<- gsub("Smin","S",labels_out,perl=T,ignore.case = T);
    labels_out<- gsub("Fmax","F",labels_out,perl=T,ignore.case = T);
    labels_out<- gsub("Jacc","J",labels_out,perl=T,ignore.case = T);
    labels_out<- gsub("AUCROC","ROC",labels_out,perl=T,ignore.case = T);
    labels_out<- gsub("AUCPR","PR",labels_out,perl=T,ignore.case = T);
    labels_out<- gsub(" ","-",labels_out,perl=T);
    labels_out
}


# converts data frame to flat format 
# (columnwise catenation with addition of rowname and colname columns)
convert_to_flat<- function(data){
    m<- nrow(data);
    n<- ncol(data);
    data_flat<- as.data.frame(matrix(0, nrow= m*n, ncol=3));
    
    colnames(data_flat)<- c("rowname","colname","value");
    data_rownames<- rownames(data);
    data_colnames<- colnames(data);
    
    for(i in 1:m){
        for(j in 1:n){
            data_flat[(j-1)*m+i,1]<- data_rownames[i];
            data_flat[(j-1)*m+i,2]<- data_colnames[j];
            data_flat[(j-1)*m+i,3]<- data[i,j];
        }
    }
    data_flat$rowname<- factor(data_flat$rowname, levels = unique(data_flat$rowname));
    data_flat$colname<- factor(data_flat$colname, levels = unique(data_flat$colname))
    data_flat
}

