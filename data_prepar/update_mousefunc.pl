#!/usr/bin/perl
use strict;
use warnings;

# Extracts genes from MouseFunc data and assigned these genes GO terms extracted from UniProt
# Prints summary to STDERR: how many genes were re-annotated using UniProt, how many had missing links.
# Prints <gene\tgo> file to STDOUT.
#
# USAGE $0 1> mousefunk_updated.genego 2> update_summary.txt

my $root= "/data/ilja/repos/ads/";
my $f1	= "$root/data/mousefunc/go_novelAnnotations.txt";
my $f2	= "$root/data/mousefunc/Gene_identities.txt";
my $f3	= "$root/data/goa_uniprot_flt.gaf";
my $CELLULAR_COMPONENT= 5575;
my $BIOLOGICAL_PROCESS= 8150;
my $MOLECULAR_FUNCTION= 3674;

my %geneid_genename;
read_tohash_multiple($f2,5-1,1-1,\%geneid_genename);
my %genename_go;
read_tohash_multiple($f3,3-1,5-1,\%genename_go);

# removing "GO:" prefixes
foreach my $v (values %genename_go){
	my @gos= split(/,/,$v,-1);
	foreach my $go(@gos){ $go=~ s/GO://;}
	$v = join(",",@gos);
}
# removing root nodes:
foreach my $v(values %genename_go){
	my @gos= split(/,/,$v,-1);
	my @flt= ();
	foreach my $go(@gos){
		if(($go != $CELLULAR_COMPONENT) && ($go != $BIOLOGICAL_PROCESS) && ($go!= $MOLECULAR_FUNCTION)){
			push(@flt,$go);
		}
	}
	$v = join(",",@flt);
}
# process MouseFunc genes and print new annotation from UniProt (if available)
my $geneid_genename_nolink = 0;
my $noannot = 0;
my $genes_annot = 0;
open(IN,"<$f1") or die "Can\'t open $f1: $!\n";
my $l=<IN>;
while(my $l=<IN>){
	my @a= split('\t',$l,-1);
	my $geneid= $a[0];
	$geneid =~ s/"//g;
	
	if(!defined($geneid_genename{$geneid})){
		$geneid_genename_nolink++;
		next;
	}
	my $genename= $geneid_genename{$geneid};
	
	if(!defined($genename_go{$genename})){
		$noannot++;
		next;
	}
	
	my @gos= split(",",$genename_go{$genename});
	foreach my $go(@gos){
		print "$genename\t$go\n";
	}
	$genes_annot++;
}
printf STDERR ("# no geneid-genename-link: %u\n",$geneid_genename_nolink);
printf STDERR ("# no annotation in UniProt: %u\n",$noannot);
printf STDERR ("# gene annotated: %u\n",$genes_annot);



# read_hash(file,key_ind,val_ind,\%hash)
# if the same key points to multiple values, these are separated by comma
sub read_tohash_multiple{
	my $file= shift;
	my $keyi= shift;
	my $vali= shift;
	my $hashp= shift;
	print STDERR "# read_tohash from $file\n";
	open(IN,"<$file") or die "Can\'t open $file: $!\n";
	my $ln=0;
	while(my $l=<IN>){
		if($l =~ "^[!#]"){
			next;
		}
        	$ln++; if($ln%50000000 == 0){print STDERR "\t $ln lines read\n"};
		chomp($l);
		my @sp= split(/\t/,$l,-1);
		if(defined($hashp->{$sp[$keyi]})){
			$hashp->{$sp[$keyi]} = join(",",$hashp->{$sp[$keyi]},$sp[$vali]);
		}
		else{
        		$hashp->{$sp[$keyi]} = $sp[$vali];
		}
	}
	close(IN);
	
	# remove possible duplicates
	foreach my $v(values %{$hashp}){
		my %tmp; 
		my @sp= split(",",$v,-1);
		foreach my $x(@sp){$tmp{$x} = 1;}
		$v = join(",", sort keys %tmp);
	}
}

